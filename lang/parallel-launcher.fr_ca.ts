<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_CA" sourcelanguage="en_CA">
<context>
    <name>AbstractCoreFinderDialog</name>
    <message>
        <location filename="../src/ui/core-finder-dialog.cpp" line="10"/>
        <source>Searching for latest core version...</source>
        <translation>Recherche de la dernière version du core...</translation>
    </message>
</context>
<context>
    <name>BindInputDialog</name>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="14"/>
        <source>Bind Input</source>
        <translation>Saisir la touche</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="38"/>
        <source>To map a button or axis to this input, either press a button on your controller or hold a control stick or trigger in a different position for a half second.</source>
        <translation>Pour lier un bouton ou un axe à cette touche, appuyez soit sur un bouton de votre manette soit maintenez le stick anaglogique ou une gâchette dans une différente position pendant une demi-seconde.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="76"/>
        <source>Skip</source>
        <translation>Passer</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="87"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>ControllerConfigDialog</name>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="14"/>
        <source>Configure Controller</source>
        <translation>Configurer la manette</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="25"/>
        <source>Configuring Controller:</source>
        <translation>Configuration de la manette:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="62"/>
        <source>Quick Configure</source>
        <translation>Configuration rapide</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="86"/>
        <source>Enable Rumble</source>
        <translation>Activer la vibration</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="99"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="19"/>
        <source>Analog Up</source>
        <translation>Analogique Haut</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="122"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="20"/>
        <source>Analog Down</source>
        <translation>Analogique Bas</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="145"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="21"/>
        <source>Analog Left</source>
        <translation>Analogique Gauche</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="168"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="22"/>
        <source>Analog Right</source>
        <translation>Analogique Droite</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="191"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="31"/>
        <source>A Button</source>
        <translation>Bouton A</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="214"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="32"/>
        <source>B Button</source>
        <translation>Bouton B</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="248"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="23"/>
        <source>C Up</source>
        <translation>C Haut</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="271"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="24"/>
        <source>C Down</source>
        <translation>C Bas</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="294"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="25"/>
        <source>C Left</source>
        <translation>C Gauche</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="317"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="26"/>
        <source>C Right</source>
        <translation>C Droite</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="340"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="34"/>
        <source>Z Trigger</source>
        <translation>Gâchette Z</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="363"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="35"/>
        <source>R Trigger</source>
        <translation>Gâchette R</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="397"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="27"/>
        <source>D-Pad Up</source>
        <translation>D-Pad Haut</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="420"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="28"/>
        <source>D-Pad Down</source>
        <translation>D-Pas Bas</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="443"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="29"/>
        <source>D-Pad Left</source>
        <translation>D-Pad Gauche</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="466"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="30"/>
        <source>D-Pad Right</source>
        <translation>D-Pad Droite</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="489"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="36"/>
        <source>Start Button</source>
        <translation>Bouton Start</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="512"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="33"/>
        <source>L Trigger</source>
        <translation>Gâchette L</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="546"/>
        <source>The following inputs are only used by games that support Gamecube Controllers:</source>
        <translation>Les touches suivantes sont utilisées seulement par les jeux qui supporte les manettes Gamecube:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="566"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="37"/>
        <source>X Button</source>
        <translation>Bouton X</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="610"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="38"/>
        <source>Y Button</source>
        <translation>Bouton Y</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="660"/>
        <source>Sensitivity</source>
        <translation>Sensibilité</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="686"/>
        <source>Deadzone</source>
        <translation>Zone morte</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="745"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="39"/>
        <source>Save State</source>
        <translation>Sauver l&apos;état</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="774"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="40"/>
        <source>Load State</source>
        <translation>Charger l&apos;état</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="822"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="42"/>
        <source>Toggle Slow Motion</source>
        <translation>Basculer le ralenti</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="832"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="41"/>
        <source>Toggle Fast Forward</source>
        <translation>basculer l&apos;avance rapide</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="864"/>
        <source>How far you need to press down a trigger or tilt a control stick that is bound to an N64 button before it is considered a button press</source>
        <translation>Jusqu&apos;où vous devez pressez la gâchette ou déplacez le stick qui est lié à un bouton de la manette N64 pour qu&apos;il soit considéré comme une pression du bouton</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="867"/>
        <source>Analog to Digital Button Press Threshold</source>
        <translation>Seuil de pression pour le passage de la pression du bouton d&apos;analogique à numérique</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="949"/>
        <source>Save As</source>
        <translation>Enregistrer Sous</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="969"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="983"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="573"/>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="597"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="75"/>
        <source>Not Bound</source>
        <translation>Non lié</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="78"/>
        <source>Button %1</source>
        <translation>Bouton %1</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="81"/>
        <source>Axis -%1</source>
        <translation>Axe -%1</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="84"/>
        <source>Axis +%1</source>
        <translation>Axe +%1</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="87"/>
        <source>Hat %1 Up</source>
        <translation>Hat %1 Haut</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="90"/>
        <source>Hat %1 Down</source>
        <translation>Hat %1 Bas</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="93"/>
        <source>Hat %1 Left</source>
        <translation>Hat %1 Gauche</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="96"/>
        <source>Hat %1 Right</source>
        <translation>Hat %1 Droite</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="141"/>
        <source>Enter Profile Name</source>
        <translation>Entrer un nom de profil</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="141"/>
        <source>Enter a name for your new controller profile.</source>
        <translation>Entrer un nom pour votre nouveau profil manette.</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="147"/>
        <source>Failed to Save Profile</source>
        <translation>Échec de la sauvegarde du profil</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="147"/>
        <source>A default controller profile with that name already exists.</source>
        <translation>Un profil de manette par défaut existe déjà avec le même nom.</translation>
    </message>
</context>
<context>
    <name>ControllerSelectDialog</name>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="14"/>
        <source>Select Controller</source>
        <translation>Sélectionner la manette</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="28"/>
        <source>Input Driver</source>
        <translation>Driver des commandes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="74"/>
        <source>If your controller does not appear in this list, or if you are having problems setting up your controller bindings, try switching to a different input driver.</source>
        <translation>Si votre manette n&apos;apparaît pas dans cette liste ou si vous avez des problèmes à configurer vos touches, essayez un autre driver de commandes.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="106"/>
        <source>Connected Controllers</source>
        <translation>Manettes connectées</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="135"/>
        <source>Controller Profiles</source>
        <translation>Profils de la manette</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="150"/>
        <location filename="../src/ui/controller-select-dialog.cpp" line="141"/>
        <location filename="../src/ui/controller-select-dialog.cpp" line="183"/>
        <source>Edit Profile</source>
        <translation>Modifier le profil</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="161"/>
        <source>Delete Profile</source>
        <translation>Effacer le profil</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="137"/>
        <location filename="../src/ui/controller-select-dialog.cpp" line="179"/>
        <source>New Profile</source>
        <translation>Nouveau profil</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="232"/>
        <source>Confirm Delete</source>
        <translation>Confirmé l&apos;effacement</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="232"/>
        <source>Are you sure you want to delete controller profile &apos;%1&apos;?</source>
        <translation>Vous êtes sûr de vouloir effacer le profil de manette &apos;%1&apos;?</translation>
    </message>
</context>
<context>
    <name>CoreInstaller</name>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="26"/>
        <location filename="../src/ui/core-installer.cpp" line="77"/>
        <source>Download Failed</source>
        <translation>Échec du téléchargement</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="27"/>
        <location filename="../src/ui/core-installer.cpp" line="78"/>
        <source>Failed to download emulator core</source>
        <translation>Échec du téléchargement du core de l&apos;émulateur</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="35"/>
        <location filename="../src/ui/core-installer.cpp" line="110"/>
        <source>Installation Failed</source>
        <translation>Échec de l&apos;installation</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="36"/>
        <location filename="../src/ui/core-installer.cpp" line="111"/>
        <source>Failed to install emulator core</source>
        <translation>Échec de l&apos;installation du core de l&apos;émulateur</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="46"/>
        <location filename="../src/ui/core-installer.cpp" line="122"/>
        <source>Installation Successful</source>
        <translation>Installation réussie</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="47"/>
        <location filename="../src/ui/core-installer.cpp" line="123"/>
        <source>Core installed successfully.</source>
        <translation>Le core a été installé avec succès.</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="150"/>
        <source>Core Unavailable</source>
        <translation>Core indisponible</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="151"/>
        <source>Sorry! The Mupen64plus-Next emulator core is not available on MacOS. Try using ParallelN64 instead.</source>
        <translation>Désolé! Le core de l&apos;émulateur Mupen64plus-Next n&apos;est pas disponible sur MacOS. Veuillez essayer ParallelN64 à la place.</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="159"/>
        <source>Install Emulator Core?</source>
        <translation>Installer le core de l&apos;émulateur?</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="160"/>
        <source>This emulator core is not installed or is out of date. Would you like to install it now?</source>
        <translation>Ce coeur d&apos;émulateur n&apos;est pas installé ou est obsolète. Voulez-vous l&apos;installer maintenant?</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="214"/>
        <location filename="../src/ui/core-installer.cpp" line="245"/>
        <source>Core Update Available</source>
        <translation>Mise à jour de core disponible</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="215"/>
        <location filename="../src/ui/core-installer.cpp" line="246"/>
        <source>An update is available for %1. Would you like to install it now?</source>
        <translation>Une mise à jour pour %1 est disponible. Voulez-vous l&apos;installer maintenant?</translation>
    </message>
</context>
<context>
    <name>DeviceBusyDialog</name>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="17"/>
        <source>Device Busy</source>
        <translation>Appareil occupé</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="26"/>
        <source>One or more SD cards are busy. Waiting for all file operations to complete...</source>
        <translation>Une ou plusieurs cartes SD sont occupées. Attendez que les opérations des fichiers se terminent...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="58"/>
        <source>Force Unmount</source>
        <translation>Forcer le démontage</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="69"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>DirectPlay</name>
    <message>
        <location filename="../src/ui/play.cpp" line="638"/>
        <location filename="../src/ui/play.cpp" line="645"/>
        <location filename="../src/ui/play.cpp" line="652"/>
        <location filename="../src/ui/play.cpp" line="659"/>
        <source>Patch Failed</source>
        <translation>Correction échoué</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="639"/>
        <source>Failed to patch ROM. The .bps patch appears to be invalid.</source>
        <translation>Échec de la correction de la ROM. Le .bps correctif est invalide.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="646"/>
        <source>Failed to patch ROM. The base ROM does not match what the patch expects..</source>
        <translation>Échec de la correction de la ROM. La ROM de base ne correspond pas à ce que le correctif s&apos;attendait.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="653"/>
        <source>Failed to patch ROM. The base rom required to patch is not known to Parallel Launcher.</source>
        <translation>Échec de la correction de la ROM. La rom de base qui est requis pour appliquer le correctif n&apos;est pas connu par Parallel Launcher.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="660"/>
        <source>Failed to patch ROM. An error occurred while writing the patched ROM to disk.</source>
        <translation>Échec de la correction de la ROM. Une erreur s&apos;est produise pendant l&apos;écriture de la ROM corrigé sur le disque.</translation>
    </message>
</context>
<context>
    <name>DirectPlayWindow</name>
    <message>
        <location filename="../src/ui/designer/direct-play-window.ui" line="14"/>
        <source>ROM Settings</source>
        <translation>Réglages de la ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/direct-play-window.ui" line="27"/>
        <source>This is your first time running this ROM. Please select any ROM specific settings. You can always change these settings later.</source>
        <translation>Ceci est votre première fois que vous utiliser cette ROM. S&apos;il vous plaît veuillez sélectionner quelconque réglages spécifiques sur la ROM. Vous pouvez toujours changer ces réglages plus tard.</translation>
    </message>
</context>
<context>
    <name>DownloadDialog</name>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="22"/>
        <location filename="../src/ui/core-installer.cpp" line="73"/>
        <source>Downloading core...</source>
        <translation>Téléchargement du core...</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="202"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="112"/>
        <source>Downloading RetroArch</source>
        <translation>Téléchargement de RetroArch</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="53"/>
        <source>Downloading installer...</source>
        <translation>Téléchargement de l&apos;installeur...</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="332"/>
        <source>Downloading Discord plugin</source>
        <translation>Téléchargement du plugin Discord</translation>
    </message>
    <message>
        <location filename="../src/core/sdcard.mount.windows.cpp" line="409"/>
        <source>Downloading SD Card Manager Extension</source>
        <translation>Téléchargement de l&apos;utilitaire du contrôleur de Cartes SD</translation>
    </message>
</context>
<context>
    <name>EditableStarDisplayWidget</name>
    <message>
        <location filename="../src/rhdc/ui/star-display-widget.cpp" line="371"/>
        <source>Save slot is empty.</source>
        <translation>Le fichier de sauvegarde est vide.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/star-display-widget.cpp" line="375"/>
        <source>Create Save Slot</source>
        <translation>Créer un emplacement de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/star-display-widget.cpp" line="376"/>
        <source>Delete Save Slot</source>
        <translation>Effacer l&apos;emplacement de sauvegarde</translation>
    </message>
</context>
<context>
    <name>ErrorNotifier</name>
    <message>
        <location filename="../src/ui/error-notifier.cpp" line="5"/>
        <source>The application encountered an unexpected error:</source>
        <translation>L&apos;application a rencontré une erreur inattendue:</translation>
    </message>
    <message>
        <location filename="../src/ui/error-notifier.cpp" line="12"/>
        <source>Unhandled Error</source>
        <translation>Erreur non géré</translation>
    </message>
</context>
<context>
    <name>Game</name>
    <message>
        <location filename="../src/ui/play.cpp" line="40"/>
        <source>The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then this ROM may contain invalid or unsafe RSP microcode that cannot be run on console accurate graphics plugins. Alternatively, if you have a very old onboard graphics card, it is possible that Vulkan is not supported on your system. In either case, using another graphics plugin might resolve the issue.</source>
        <translation>L&apos;émulateur s&apos;est fermé peu après le démarrage. Si vous êtes capable de lancer d&apos;autres ROM sans problèmes, il se peut que cette ROM contienne un invalide ou non sûr RSP microcode qui ne peut pas être lancer avec le plugin graphique précis pour la console. Alternativement,si vous avez une très vieille carte graphique de base, il est possible que Vulkan ne soit pas supporter sur votre système. Dans les deux cas, utiliser un autre plugin graphique peut régler le problème.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="46"/>
        <source>The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then this ROM may contain invalid or unsafe RSP microcode that cannot be run on console accurate graphics plugins. If this is the case, try running the ROM with another graphics plugin instead.</source>
        <translation>L&apos;émulateur s&apos;est fermé peu après le démarrage. Si vous êtes capable de lancer d&apos;autres ROM sans problèmes, il se peut que cette ROM contienne un invalide ou non sûr RSP microcode qui ne peut pas être lancer avec les graphismes répresentant la console. Dans ce cas, essayer d&apos;ouvrir la ROM avec un autre plugin graphique à la place.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="51"/>
        <source>The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then this ROM is most likely corrupt.</source>
        <translation>L&apos;émulateur s&apos;est fermé peu après le démarrage. Si vous êtes capable de lancer d&apos;autres ROM sans problèmes, cela veut dire que cette ROM est sûrement corrompu.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="262"/>
        <source>Emulator Missing</source>
        <translation>Émulateur manquant</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="263"/>
        <source>Failed to launch emulator. It does not appear to be installed.</source>
        <translation>Échec du lancement de l&apos;émulateur. Il n&apos;est pas installé.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="451"/>
        <source>Possible ROM Error</source>
        <translation>Erreur possible venant de la ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="500"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="506"/>
        <source>Do not warn me again</source>
        <translation>Ne plus m&apos;avertir</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="571"/>
        <source>This game uses an emulated Gamecube controller, but you have not bound any inputs to the X and Y buttons.</source>
        <translation>Ce jeu utilise une manette Gamecube émulé, mais vous n&apos;êtes pas obligé de lier une touche aux boutons X et Y.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="581"/>
        <source>This game uses an emulated Gamecube controller, but you do not have an analog stick bound to the C buttons (C stick).</source>
        <translation>Ce jeu utilise une manette Gamecube émulé, mais vous n&apos;avez pas de stick analogique lié au bouttons C (C stick).</translation>
    </message>
</context>
<context>
    <name>GetControllerPortDialog</name>
    <message>
        <location filename="../src/ui/get-controller-port-dialog.cpp" line="8"/>
        <source>Choose Controller</source>
        <translation>Choisissez une manette</translation>
    </message>
    <message>
        <location filename="../src/ui/get-controller-port-dialog.cpp" line="9"/>
        <source>Press any button on the controller to bind to this port.</source>
        <translation>Appuyez sur n&apos;importe quel bouton sur la manette pour la lier à ce port.</translation>
    </message>
</context>
<context>
    <name>GroupName</name>
    <message>
        <location filename="../src/core/special-groups.cpp" line="14"/>
        <source>Favourites</source>
        <translation>Favori(s)</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="16"/>
        <source>Uncategorized</source>
        <translation>Non catégorisé(s)</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="18"/>
        <source>Want To Play</source>
        <translation>À jouer</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="20"/>
        <source>In Progress</source>
        <translation>En pleine progression</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="22"/>
        <source>Completed</source>
        <translation>Complété(s)</translation>
    </message>
</context>
<context>
    <name>HotkeyEditWidget</name>
    <message>
        <location filename="../src/ui/hotkey-edit-widget.cpp" line="8"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
</context>
<context>
    <name>ImportSaveDialog</name>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="10"/>
        <source>Select Save File</source>
        <translation>Sélectionner le fichier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="11"/>
        <source>Project64 Save Files</source>
        <translation>Les fichiers de sauvegardes de Project64</translation>
    </message>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="21"/>
        <source>Save data imported</source>
        <translation>Les données de sauvegardes ont étés importées</translation>
    </message>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="24"/>
        <source>Failed to import save data</source>
        <translation>Échec de l&apos;importage des donées de sauvegardes</translation>
    </message>
</context>
<context>
    <name>ImportSdCardDialog</name>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="17"/>
        <source>Import SD Card</source>
        <translation>Importer une carte SD</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="31"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="69"/>
        <source>Use this file directly</source>
        <translation>Utiliser directement le fichier</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="79"/>
        <source>Make a copy of this file</source>
        <translation>Créer une copie de ce fichier</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="69"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="132"/>
        <source>This name is already in use</source>
        <translation>Ce nom est déjà utilisé</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="149"/>
        <source>This name is reserved by Windows</source>
        <translation>Ce nom est réservé par le système</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="165"/>
        <source>Unexpected Error</source>
        <translation>Une erreur inattendue s&apos;est produite</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="165"/>
        <source>Failed to import SD card.</source>
        <translation>Échec de l&apos;import de la carte SD.</translation>
    </message>
</context>
<context>
    <name>InputModeSelect</name>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="506"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="507"/>
        <source>Maps your gamepad inputs to a single N64 controller using your controller profile</source>
        <translation>Met vos touches de manette sur une seule manette de N64 utilisant votre profil de manette</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="530"/>
        <source>Dual Analog</source>
        <translation>Analogique double</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="531"/>
        <source>Your gamepad inputs that normally bind to the C buttons instead bind to the analog stick on a second N64 controller</source>
        <translation>Vos touches qui sont normalement utilisés sur les boutons C sont à la place utilisés sur un stick analogique sur une deuxième manette de N64</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="554"/>
        <source>GoldenEye</source>
        <translation>GoldenEye</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="555"/>
        <source>Maps your gamepad inputs to two N64 controllers suitable for playing GoldenEye with the 2.4 Goodhead control style</source>
        <translation>Met vos touches sur 2 manettes de N64 utilisable pour jouer à GoldenEye avec le 2.4Goodhead style de contrôle</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="578"/>
        <source>Clone</source>
        <translation>Clone</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="579"/>
        <source>Your gamepad inputs are sent to two controller ports instead of just one</source>
        <translation>Les touches de votre manette est envoyer à deux manettes au lieu d&apos;une</translation>
    </message>
</context>
<context>
    <name>IsViewerWindow</name>
    <message>
        <location filename="../src/ui/is-viewer-window.cpp" line="58"/>
        <source>Error: IS Viewer was disconnected from the emulator.</source>
        <translation>Erreur : IS-Viewer a été déconnecté de l&apos;émulateur.</translation>
    </message>
    <message>
        <location filename="../src/ui/is-viewer-window.cpp" line="59"/>
        <source>Error: Failed to connect IS Viewer to the emulator.</source>
        <translation>Erreur : Impossible de connecter IS-Viewer à l&apos;émulateur.</translation>
    </message>
</context>
<context>
    <name>KeyboardConfigDialog</name>
    <message>
        <location filename="../src/ui/designer/keyboard-config-dialog.ui" line="14"/>
        <source>Configure Keyboard Controls</source>
        <translation>Configuartion des contrôles du clavier</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/keyboard-config-dialog.ui" line="28"/>
        <source>Hotkeys</source>
        <translation>Raccourcis clavier</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/keyboard-config-dialog.ui" line="71"/>
        <source>Keyboard Controls</source>
        <translation>Contrôle clavier</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="18"/>
        <source>Analog Up</source>
        <translation>Analogique haut</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="19"/>
        <source>Analog Down</source>
        <translation>Analogique bas</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="20"/>
        <source>Analog Left</source>
        <translation>Analogique gauche</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="21"/>
        <source>Analog Right</source>
        <translation>Analogique droite</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="22"/>
        <source>C Up</source>
        <translation>C haut</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="23"/>
        <source>C Down</source>
        <translation>C bas</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="24"/>
        <source>C Left</source>
        <translation>C gauche</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="25"/>
        <source>C Right</source>
        <translation>C droite</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="26"/>
        <source>D-Pad Up</source>
        <translation>D-Pad haut</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="27"/>
        <source>D-Pad Down</source>
        <translation>D-Pad bas</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="28"/>
        <source>D-Pad Left</source>
        <translation>D-Pad gauche</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="29"/>
        <source>D-Pad Right</source>
        <translation>D-Pad droite</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="30"/>
        <source>A Button</source>
        <translation>Bouton A</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="31"/>
        <source>B Button</source>
        <translation>Bouton B</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="32"/>
        <source>L Trigger</source>
        <translation>Gâchette L</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="33"/>
        <source>Z Trigger</source>
        <translation>Gâchette Z</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="34"/>
        <source>R Trigger</source>
        <translation>Gâchette R</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="35"/>
        <source>Start Button</source>
        <translation>Bouton Start</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="36"/>
        <source>X Button</source>
        <translation>Bouton X</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="37"/>
        <source>Y Button</source>
        <translation>Bouton Y</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="39"/>
        <source>Save State</source>
        <translation>Sauver l&apos;état</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="40"/>
        <source>Load State</source>
        <translation>Charger l&apos;état</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="41"/>
        <source>Previous State</source>
        <translation>État précédent</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="42"/>
        <source>Next State</source>
        <translation>État suivant</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="43"/>
        <source>Previous Cheat</source>
        <translation>Triche précédente</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="44"/>
        <source>Next Cheat</source>
        <translation>Triche suivante</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="45"/>
        <source>Toggle Cheat</source>
        <translation>Basculer la triche</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="46"/>
        <source>Toggle FPS Display</source>
        <translation>Basculer l&apos;affichage des IPS</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="47"/>
        <source>Pause/Unpause</source>
        <translation>Pause/enlever la pause</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="48"/>
        <source>Frame Advance</source>
        <translation>Prochaine frame</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="49"/>
        <source>Fast Forward (Hold)</source>
        <translation>Avance rapide (Maintenir)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="50"/>
        <source>Fast Forward (Toggle)</source>
        <translation>Avance rapide (Basculer)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="51"/>
        <source>Slow Motion (Hold)</source>
        <translation>Ralenti (Maintenir)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="52"/>
        <source>Slow Motion (Toggle)</source>
        <translation>Ralenti (Basculer)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="53"/>
        <source>Rewind*</source>
        <translation>Retour en arrière</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="54"/>
        <source>Quit Emulator</source>
        <translation>Quitter l&apos;émulateur</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="55"/>
        <source>Hard Reset</source>
        <translation>Redémarrage totale</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="56"/>
        <source>Toggle Fullscreen</source>
        <translation>Basculer l&apos;écran plein</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="57"/>
        <source>RetroArch Menu</source>
        <translation>Menu de RetroArch</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="58"/>
        <source>Record Video</source>
        <translation>Enregistrer une vidéo</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="59"/>
        <source>Record Inputs</source>
        <translation>Enregistrer les touches</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="60"/>
        <source>Take Screenshot</source>
        <translation>Prendre une capture d&apos;écran</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="61"/>
        <source>Raise Volume</source>
        <translation>Monter le volume</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="62"/>
        <source>Lower Volume</source>
        <translation>Descendre le volume</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="63"/>
        <source>Mute/Unmute</source>
        <translation>Couper le son/Remettre le son</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="64"/>
        <source>Grab Mouse</source>
        <translation>Capture la souris</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="98"/>
        <source>You must enable rewind functionality in the RetroArch menu to use this hotkey.</source>
        <translation>Vous devez activer la fonctionnalité de retour en arrière dans le menu de RetroArch pour utiliser cette touche raccourci.</translation>
    </message>
</context>
<context>
    <name>LogViewerDialog</name>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="14"/>
        <source>Log Viewer</source>
        <translation>Visionneuse de journaux</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="28"/>
        <source>Only show logs from this session</source>
        <translation>Seulement montrer les journaux de cette session</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="48"/>
        <source>Show timestamps</source>
        <translation>Montrer l&apos;horodatage</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="60"/>
        <source>Visible Logs</source>
        <translation>Journaux visibles</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="66"/>
        <source>Debug</source>
        <translation>Débug</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="76"/>
        <source>Info</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="86"/>
        <source>Warn</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="96"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="106"/>
        <source>Fatal</source>
        <translation>Fatale</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../src/main.cpp" line="225"/>
        <source>Installing RetroArch</source>
        <translation>Installation de RetroArch</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="226"/>
        <source>Parallel Launcher will now install RetroArch.</source>
        <translation>Parallel Launcher va maintenant installer RetroArch.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="235"/>
        <source>Fatal Error</source>
        <translation>Erreur Fatale</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="236"/>
        <source>Failed to install RetroArch.</source>
        <translation>Écher de l&apos;installation de RetroArch.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="64"/>
        <source>Refresh ROM List</source>
        <translation>Rafraîchir la liste de ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="81"/>
        <source>Configure Controller</source>
        <translation>Configuration de la manette</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="98"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="128"/>
        <source>Searching for ROMs...</source>
        <translation>Recherche de ROMs...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="185"/>
        <source>No ROMS have been found</source>
        <translation>Aucune ROMS n&apos;a été trouvé</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="192"/>
        <location filename="../src/ui/designer/main-window.ui" line="333"/>
        <source>Manage ROM Sources</source>
        <translation>Gérer la source des ROMs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="236"/>
        <source>Search...</source>
        <translation>Chercher...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="283"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="300"/>
        <source>Play Multiplayer</source>
        <translation>Jouer en multijoueur</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="311"/>
        <source>Play Singleplayer</source>
        <translation>Jouer en solo</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="328"/>
        <source>Settings</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="338"/>
        <source>Configure Controllers</source>
        <translation>Configurer les manettes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="343"/>
        <source>Keyboard Controls and Hotkeys</source>
        <translation>Les contrôles et les raccourcis clavier du clavier</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="346"/>
        <source>Configure keyboard controls and hotkeys</source>
        <translation>Configuer les contrôles et les raccourcis clavier du clavier</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="351"/>
        <location filename="../src/ui/main-window.cpp" line="785"/>
        <source>Login to romhacking.com</source>
        <translation>Connexion à romhacking.com</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="356"/>
        <source>Logout of romhacking.com</source>
        <translation>Déconnexion à romhacking.com</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="361"/>
        <source>Disable romhacking.com integration</source>
        <translation>Désactiver l&apos;intégration de romhacking.com</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="366"/>
        <source>Add Single ROM</source>
        <translation>Ajouter une seule ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="375"/>
        <source>Quit Parallel Launcher</source>
        <translation>Fermer Parallel Launcher</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="380"/>
        <source>Open save file directory</source>
        <translation>Ouvrir le répertoire des sauvegardes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="385"/>
        <source>Open savestate directory</source>
        <translation>Ouvrir le répertoire des sauvegardes rapides</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="390"/>
        <source>Open SD card directory</source>
        <translation>Ouvrir le répertoire des cartes SD</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="395"/>
        <source>Open app data directory</source>
        <translation>Ouvrir le répertoire des données de l&apos;application</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="400"/>
        <source>Open app config directory</source>
        <translation>Ouvrir le répertoire des paramètres de l&apos;application</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="405"/>
        <source>Open app cache directory</source>
        <translation>Ouvrir le répertoire du cache de l&apos;application</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="143"/>
        <source>Data Directories</source>
        <translation>Répertoires de données</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="630"/>
        <source>Select a ROM</source>
        <translation>Sélectionner une ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="631"/>
        <source>ROM or Patch File</source>
        <translation>ROM ou Corriger le fichier</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="656"/>
        <location filename="../src/ui/main-window.cpp" line="659"/>
        <location filename="../src/ui/main-window.cpp" line="662"/>
        <location filename="../src/ui/main-window.cpp" line="665"/>
        <source>Patch Failed</source>
        <translation>Correction échoué</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="656"/>
        <source>Failed to patch ROM. The .bps patch appears to be invalid.</source>
        <translation>Échec de la correction de la ROM. Le .bps correctif est invalide.</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="659"/>
        <source>Failed to patch ROM. The base ROM does not match what the patch expects.</source>
        <translation>Échec de la correction de la ROM. La ROM the base ne correspond pas à ce que la correction s&apos;attendait.</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="662"/>
        <source>Failed to patch ROM. The base rom required to patch is not known to Parallel Launcher.</source>
        <translation>Échec de la correction de la ROM. La rom de base qui est requis pour appliquer le correctif n&apos;est pas connu par Parallel Launcher.</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="665"/>
        <source>Failed to patch ROM. An error occurred while writing the patched ROM to disk.</source>
        <translation>Échec de la correction de la ROM. Une erreur s&apos;est produise pendant l&apos;écriture de la ROM corrigé sur le disque.</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="785"/>
        <source>Enable romhacking.com integration</source>
        <translation>Activer l&apos;intégration de romhacking.com</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="824"/>
        <source>Confirm Disable</source>
        <translation>Confirmer la désactivation</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="824"/>
        <source>Are you sure you want to disable romhacking.com integration?</source>
        <translation>Vous êtes sûr de vouloir désactiver l&apos;intégration de romhacking.com?</translation>
    </message>
</context>
<context>
    <name>ManageGroupsDialog</name>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="14"/>
        <source>Manage Groups</source>
        <translation>Gérer les groupes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="32"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="43"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="54"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="55"/>
        <source>Confirm Delete</source>
        <translation>Confirmer l&apos;effacement</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="55"/>
        <source>Are you sure you want to delete this group?</source>
        <translation>Vous êtes sûr de vouloir effacer ce groupe?</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="75"/>
        <source>Rename Group</source>
        <translation>Renommer le groupe</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="75"/>
        <source>Enter a new name for your group</source>
        <translation>Entrer un nouveau nom pour votre groupe</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="85"/>
        <source>Rename Failed</source>
        <translation>renommage échoué</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="85"/>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="104"/>
        <source>A group with this name already exists.</source>
        <translation>Un groupe avec ce nom existe déjà.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="99"/>
        <source>Create Group</source>
        <translation>Créer un groupe</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="99"/>
        <source>Enter a name for your new group</source>
        <translation>Entrer le nom de votre nouveau groupe</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="104"/>
        <source>Create Failed</source>
        <translation>Création échoué</translation>
    </message>
</context>
<context>
    <name>ManageSdCardsDialog</name>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="14"/>
        <source>Manage Virtual SD Cards</source>
        <translation>Gérer les cartes SD virtuelles</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="42"/>
        <source>Create New</source>
        <translation>Nouvelle</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="53"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="111"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="118"/>
        <source>Format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="147"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="168"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="211"/>
        <source>Reformat</source>
        <translation>Reformatter</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="222"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="251"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="415"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="437"/>
        <source>Clone</source>
        <translation>Cloner</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="262"/>
        <source>Browse Files</source>
        <translation>Naviguer dans les fichiers</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="295"/>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="306"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="117"/>
        <source>Confirm Reformat</source>
        <translation>Confirmer le reformatage</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="118"/>
        <source>Are you sure you want to reformat this virtual SD card? All data currently on it will be lost.</source>
        <translation>Êtes-vous sûr de vouloir reformater cette SD virtuelle? Toutes les données présentes actuellement seront effacés.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="213"/>
        <source>Confirm Deletion</source>
        <translation>Confirmer la suppression</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="214"/>
        <source>Are you sure you want to delete this virtual SD card? This action cannot be undone.</source>
        <translation>Êtes-vous sûr de vouloir effacer cette carte SD virtuelle? C&apos;est action ne peux être annulé.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="245"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="279"/>
        <source>Unexpected Error</source>
        <translation>Une erreur inattendue s&apos;est produite</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="245"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="279"/>
        <source>Failed to create the virtual SD card because of an unknown error.</source>
        <translation>Échec de la création de la carte SD virtuelle à cause d&apos;une erreur inconnu.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="297"/>
        <source>Import SD Card</source>
        <translation>Importer une carte SD</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="298"/>
        <source>Raw Disk Image</source>
        <translation>Image disque brute</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="306"/>
        <source>Invalid disk image</source>
        <translation>Image disque invalide</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="306"/>
        <source>The specified file is not a valid disk image.</source>
        <translation>Le fichier spécificé ne contient pas d&apos;image disque valide.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="309"/>
        <source>Disk image too large</source>
        <translation>Image disque trop grande</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="309"/>
        <source>The disk image could not be imported because it is larger than 2 TiB.</source>
        <translation>L&apos;image disque n&apos;a pas pu être importée car elle est supérieure à 2TiB.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="396"/>
        <source>Operation Failed</source>
        <translation>Opération raté</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="397"/>
        <source>Failed to mount the SD card image. For more detailed error information, go back to the main window and press F7 to view error logs.</source>
        <translation>Le montage de l&apos;image sur la carte SD à échoué. Pour plus de détails sur les erreurs, retourner à l&apos;écran principal et appuyez sur F7 pour voir les journaux d&apos;erreurs.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="411"/>
        <source>Clone SD Card</source>
        <translation>Cloner la carte SD</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="412"/>
        <source>Enter a name for the copied SD card:</source>
        <translation>Entrez un nom pour la carte SD copié:</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="514"/>
        <source>Confirm Close</source>
        <translation>Confirmer la fermeture</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="515"/>
        <source>All SD cards you have opened for browsing will be closed. Continue?</source>
        <translation>Toutes les cartes SD que avez ouvert pour naviguer dedans vont se fermer. Continuez?</translation>
    </message>
</context>
<context>
    <name>MultiplayerControllerSelectDialog</name>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="14"/>
        <source>Select Controllers</source>
        <translation>Sélectionner les manettes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="22"/>
        <source>Port 1</source>
        <translation>Port 1</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="33"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="59"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="85"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="111"/>
        <source>-- None --</source>
        <translation>-- Aucun --</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="41"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="67"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="93"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="119"/>
        <source>Detect Device</source>
        <translation>Détection de périphériques</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="48"/>
        <source>Port 2</source>
        <translation>Port 2</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="74"/>
        <source>Port 3</source>
        <translation>Port 3</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="100"/>
        <source>Port 4</source>
        <translation>Port 4</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="128"/>
        <source>Allow port 1 to save and load states</source>
        <translation>Autoriser le port 1 de sauvegarder et de charger des états</translation>
    </message>
</context>
<context>
    <name>NeutralInputDialog</name>
    <message>
        <location filename="../src/ui/designer/neutral-input-dialog.ui" line="14"/>
        <source>Return to Neutral</source>
        <translation>Retour en position neutre</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/neutral-input-dialog.ui" line="29"/>
        <source>Return all triggers and analog sticks to their neutral position, then press any button on the controller or click the OK button to continue.</source>
        <translation>Retourner toutes les gâchettes et les sticks analogiques à leur position neutral, puis appuyer sur n&apos;importe quel bouton sur la manette ou cliquer sur le bouton OK pour continuer.</translation>
    </message>
</context>
<context>
    <name>NowPlayingWidget</name>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="34"/>
        <source>Now Playing: </source>
        <translation>Joue maintenant: </translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="106"/>
        <source>Play Time</source>
        <translation>Temps de jeu</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="119"/>
        <source>Total</source>
        <translation>Total</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="150"/>
        <source>This Session</source>
        <translation>Cette session</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="221"/>
        <source>Hard Reset</source>
        <translation>Réinitialisation complète</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="232"/>
        <source>Kill Emulator</source>
        <translation>Tuer l&apos;émulateur</translation>
    </message>
    <message>
        <location filename="../src/ui/now-playing-widget.cpp" line="18"/>
        <source>The hack author has not provided a star layout for this hack. This layout will show unattainable stars and may be missing some others.</source>
        <translation>L&apos;auteur du hack n&apos;a pas prodigué de mise en page d&apos;étoiles pour ce hack. Cette mise en page va montrer des étoiles inatteignables et peut en manquer d&apos;autres.</translation>
    </message>
    <message>
        <location filename="../src/ui/now-playing-window.cpp" line="177"/>
        <source>Parallel Launcher - Now Playing</source>
        <translation>Parallel Launcher - Entrain de jouer</translation>
    </message>
</context>
<context>
    <name>RhdcDownloadDialog</name>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="297"/>
        <source>Downloading star layout</source>
        <translation>Téléchargement de star layout</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="322"/>
        <source>Failed to download star layout from RHDC</source>
        <translation>Échec du téléchargement de star layout depuis RHDC</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="338"/>
        <source>Downloading patch</source>
        <translation>Téléchargement du correctif</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="158"/>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="239"/>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="387"/>
        <source>Failed to download patch from RHDC</source>
        <translation>Échec tu téléchargement du correctif depuis RHDC</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="361"/>
        <source>Applying patch</source>
        <translation>Application du correctif</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="370"/>
        <source>The rom could not be installed because you do not have an unmodified copy of the US version of Super Mario 64 in your known roms list.</source>
        <translation>La rom n&apos;a pas pu être installé parce que vous n&apos;avez pas une copie NTSC/US non-modifié de Super Mario 64 dans votre liste de roms connues.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="427"/>
        <source>The rom could not be installed because no bps patch was found.</source>
        <translation>La rom n&apos;a pas pu être installé car aucun correcti bps n&apos;a pu être trouvé.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="441"/>
        <source>Could not install &apos;%1&apos; because of an unexpected error while applying the patch</source>
        <translation>Ne pouvait pas installer %1 à cause d&apos;une erreur inattendue pendant l&apos;application du correctif</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="451"/>
        <source>Computing checksum</source>
        <translation>Calculation du checksum</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="473"/>
        <source>Romhack installed successfully</source>
        <translation>La Romhack a été installée avec succès</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-download-dialog.ui" line="14"/>
        <source>Downloading Patch</source>
        <translation>Téléchargement du correctif</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-download-dialog.ui" line="25"/>
        <source>Fetching hack info</source>
        <translation>Recherche de l&apos;information de la romhack</translation>
    </message>
</context>
<context>
    <name>RhdcHackView</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="30"/>
        <source>Hack List:</source>
        <translation>Liste de hacks:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="63"/>
        <source>Sort By:</source>
        <translation>Trié par:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="80"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="85"/>
        <source>Popularity</source>
        <translation>Popularité</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="90"/>
        <source>Rating</source>
        <translation>Évaluation</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="95"/>
        <source>Difficulty</source>
        <translation>Difficulté</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="100"/>
        <source>Last Played</source>
        <translation>Dernière fois que vous avez joué</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="105"/>
        <source>Play Time</source>
        <translation>Temps de jeu</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="110"/>
        <source>Stars</source>
        <translation>Étoiles</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="131"/>
        <source>Search...</source>
        <translation>Chercher...</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="266"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="86"/>
        <source>All Hack Lists</source>
        <translation>La liste de tous les hacks</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="137"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="143"/>
        <source>Authors:</source>
        <translation>Auteurs:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="138"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="149"/>
        <source>Unknown</source>
        <extracomment>Shown when no authors are listed for a hack</extracomment>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="143"/>
        <source>Author:</source>
        <translation>Auteur:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="300"/>
        <source>Create List</source>
        <translation>Créer une liste</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="300"/>
        <source>Enter a name for your new hack list</source>
        <translation>Entrer un nom pour votre nouvelle liste</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="305"/>
        <source>List Exists</source>
        <translation>La liste existe</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="305"/>
        <source>A hack list with this name already exists</source>
        <translation>Une liste d&apos;hack existe déjà avec ce nom</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="331"/>
        <source>Rate Hack</source>
        <translation>Noter le hack</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="334"/>
        <source>Mark as Not Completed</source>
        <translation>Marquer comme Non Complété</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="336"/>
        <source>Mark as Completed</source>
        <translation>Marquer comme Complété</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="346"/>
        <source>Move to Hack List...</source>
        <translation>Déplacer vers cette liste de hack...</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="347"/>
        <source>Copy to Hack List...</source>
        <translation>Copier vers cette liste de hack...</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="354"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="355"/>
        <source>New List</source>
        <translation>Nouvelle liste</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="361"/>
        <source>Remove from &apos;%1&apos;</source>
        <translation>Enlever de &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="368"/>
        <source>Delete Save File</source>
        <translation>Effacer le fichier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="371"/>
        <source>Edit Save File</source>
        <translation>Modifier le fichier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="372"/>
        <source>Import Project64 Save File</source>
        <translation>Importer le fichier de sauvegarde de Project64</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="373"/>
        <source>Show Save File</source>
        <translation>Afficher le ficher de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="377"/>
        <source>Unfollow Hack</source>
        <translation>Ne plus suivre ce hack</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="379"/>
        <source>Uninstall Hack Version</source>
        <translation>Désinstaller la version de ce hack</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="482"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="518"/>
        <source>Confirm Delete</source>
        <translation>Confirmé l&apos;effacement</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="482"/>
        <source>Are you sure you want to delete your save file?</source>
        <translation>Vous êtes sûr de vouloir supprimer votre fichier de sauvegarde?</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="509"/>
        <source>Confirm Unfollow</source>
        <translation>Confirmer l&apos;arrêt de suivi</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="509"/>
        <source>Are you sure you want to unfollow this hack? The ROM will not be deleted from your computer, but it will be removed from all of your hack lists on romhacking.com and your progress will no longer be synced with romhacking.com.</source>
        <translation>Vous êtes sûr de vouloir ne plus suivre ce hack? La ROM ne va pas être effacer de votre ordinateur, mais va être enlevé de toutes vos liste sur romhacking.com et votre progression ne sera plus synchroniser avec romhacking.com.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="518"/>
        <source>Are you sure you want to delete this ROM file from your computer?</source>
        <translation>Vous êtes sûr de vouloir supprimer ce fichier ROM de votre ordinateur?</translation>
    </message>
</context>
<context>
    <name>RhdcHackWidget</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="302"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="341"/>
        <source>Popularity (Downloads)</source>
        <translation>Popularité (Téléchargements)</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="385"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="424"/>
        <source>Rating</source>
        <translation>Évaluation</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="465"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="504"/>
        <source>Difficulty</source>
        <translation>Difficulté</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="561"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="600"/>
        <source>Last Played</source>
        <extracomment>Date and time that a hack was last played</extracomment>
        <translation>Dernière fois que vous avez joué</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="641"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="680"/>
        <source>Total Play Time</source>
        <translation>Temps de jeu total</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-widget.cpp" line="145"/>
        <source>Never</source>
        <extracomment>Text shown in place of a date when a hack hasn&apos;t been played yet</extracomment>
        <translation>Jamais joué</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-widget.cpp" line="158"/>
        <source>Completed</source>
        <translation>Complété(s)</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-widget.cpp" line="158"/>
        <source>Incomplete</source>
        <translation>Incomplet</translation>
    </message>
</context>
<context>
    <name>RhdcLoginDialog</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="14"/>
        <source>RHDC Login</source>
        <translation>RHDC connexion</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="66"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To enable romhacking.com integration, you must have an unmodified copy of the US version of &lt;span style=&quot; font-style:italic;&quot;&gt;Super Mario 64&lt;/span&gt; in z64 format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pour activer l&apos;intégration de romhacking.com, vous devez avoir une copie de la version non modifiée US de &lt;span style=&quot; font-style:italic;&quot;&gt;Super Mario 64&lt;/span&gt; en format z64.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="128"/>
        <source>Browse for SM64 ROM</source>
        <translation>Parcourir pour la ROM de SM64</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="180"/>
        <source>Sign in to romhacking.com</source>
        <translation>Se connecter à romhacking.com</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="194"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="204"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="256"/>
        <source>Sign In</source>
        <translation>S&apos;inscrire</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="359"/>
        <source>Multi-factor authentication is enabled on this account</source>
        <translation>L&apos;authentification à deux facteurs est activée sur ce compte</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="366"/>
        <source>Please enter the 6-digit authentication code from your authenticator app.</source>
        <translation>Veuillez entrer le code d&apos;authentification à 6 caractères obtenu grâce à votre application d&apos;authentification.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="437"/>
        <source>Verify</source>
        <translation>Vérifier</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="501"/>
        <source>Incorrect authentication code</source>
        <translation>Code d&apos;authentification incorrect</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="546"/>
        <source>Please confirm your romhacking.com integration settings. You can always change these later in the settings dialog.</source>
        <translation>S&apos;il vous plaît confirmer les réglages d&apos;intégration de romhacking.com. Vous pourrez toujours les changer plus tard dedans les dialogues de paramètres.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="574"/>
        <source>Download Directory</source>
        <translation>Téléchargement du répertoire</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="596"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="620"/>
        <source>Sets the directory that hacks downloaded from romhacking.com will be saved to. If you change this setting later, your roms will automatically be moved to the new directory.</source>
        <translation>Met en place le répertoire où les hacks téléchargés de romhacking.com seront sauvés. Si vous changer ce paramètre plus tard, vos roms seront automatiquement déplacer dans le nouveau répertoire.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="652"/>
        <source>Enable Star Display</source>
        <translation>Activer Star Display</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="673"/>
        <source>If checked, when a rom is running that has a supported layout file from RHDC, a layout of all the stars in the hack will be shown. This layout updates automatically after the game is saved.</source>
        <translation>Si coché, quand une est active et qui a un layout supporté depuis RHDC, un layout de toutes les étoiles dans le hack vont être montrer. Ce layout ce met à jour automatiquement après que le jeu soit sauvé.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="710"/>
        <source>Show star display for hacks without a star layout</source>
        <translation>Montrer star display pour les hacks sans mise en page d&apos;étoiles</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="731"/>
        <source>If checked, Parallel Launcher will still show a star display with a default layout if the hack author did not provide one.</source>
        <translation>Si coché, Parallel Launcher va toujours montrer un star display avec une mise en page de base si l&apos;auteur du hack n&apos;en a pas prodigué une.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="768"/>
        <source>Check all save slots</source>
        <translation>Vérifie tous les fichiers de sauvegardes</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="786"/>
        <source>If checked, Parallel Launcher will submit your highest star count across all save slots to romhacking.com; otherwise, it will only submit your star count in slot A.</source>
        <translation>Si coché, Parallel Launcher va soumettre votre plus haut nombre d&apos;étoile parmis tous les fichers de sauvegardes sur romhacking.com; sinon, il va seulement soumettre votre nombre d&apos;étoile dans le fichier de sauvegarde A.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="818"/>
        <source>Prefer HLE plugins</source>
        <translation>Préférence pour les plugins HLE</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="836"/>
        <source>If checked, Parallel Launcher will use the GLideN64 plugin instead of the ParaLLEl plugin when it guesses that ParrLLEl is probably the best plugin to use. Only check this if your computer has an old GPU with poor Vulkan support. If you still experience lag even with GLideN64, you may need to disable &apos;Emulate Framebuffer&apos; and/or &apos;Emulate N64 Depth Compare&apos; in the GFX Plugins section of your Parallel Launcher settings.</source>
        <translation>Si coché, Parallel Launcher va utiliser le plugin GLideN64 au lieu du plugin ParaLLel s&apos;il pense que le ParrLLEl est probablement le meilleur plugin à utiliser. Cocher seulement ceci si votre ordinateur a une vieille carte graphique avec un mauvais support pour Vulkan. Si vous rencontrez encore des lags même avec GLideN64, vous devez peut-être désactiver &apos;Emulate Framebuffer&apos; et/ou &apos;EmulateN64 Depth Compare&apos; dans la section des plugins GFX depuis les paramètres de Parallel Launcher.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="868"/>
        <source>Ignore widescreen hint</source>
        <translation>Ignorer la suggestion de plein écran</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="886"/>
        <source>If checked, Parallel Launcher will always default to using 4:3 resolution, even if the recommended settings indicate that widescreen is supported</source>
        <translation>Si coché, Parallel Launcher va toujours utiliser par défaut la résolution 4:3, même si les options recommandés indique que le plein écran est supporté</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="79"/>
        <source>Select SM64 ROM</source>
        <translation>Sélectionner une ROM de SM64</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="85"/>
        <source>ROM Does not Match</source>
        <translation>Les ROMs ne correspondent pas</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="85"/>
        <source>The provided rom does not match the expected checksum.</source>
        <translation>La rom fournie ne correspond pas au checksum expecté.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="127"/>
        <source>Select a new download directory</source>
        <translation>Sélectionner un nouveau répetoire de téléchargement</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="150"/>
        <source>Enter your username</source>
        <translation>Entrer votre nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="155"/>
        <source>Enter your password</source>
        <translation>Entrer votre mot de passe</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="206"/>
        <source>Username or password is incorrect</source>
        <translation>Le nom d&apos;utilisateur ou le mot de passe est incorrect</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="209"/>
        <source>The romhacking.com servers appear to be down.</source>
        <translation>Les serveurs de romhacking.com sont indisponibles pour le moment.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="212"/>
        <source>An unknown error occurred.</source>
        <translation>Une erreur inattendue s&apos;est produite.</translation>
    </message>
</context>
<context>
    <name>RhdcRatingDialog</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="14"/>
        <source>Rate this Hack</source>
        <translation>Noter ce hack</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="25"/>
        <source>How would you rate this hack?</source>
        <translation>Comment noteriez-vous ce hack?</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="51"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Quality. &lt;/span&gt;How enjoyable, impressive, and/or polished was this hack?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Qualité. &lt;/span&gt;Comment agréale, impressionnant et/ou poli était ce hack?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="162"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Difficulty. &lt;/span&gt;In general, how would you rate this hack&apos;s difficulty level?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Difficulté. &lt;/span&gt;En général, comment noteriez-vous la difficulté de ce hack?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="10"/>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="24"/>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="38"/>
        <source>I can&apos;t decide or have no opinion</source>
        <extracomment>0</extracomment>
        <translation>Je ne peut pas noter ou je n&apos;ai pas d&apos;opinion</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="11"/>
        <source>Zero Effort</source>
        <extracomment>1</extracomment>
        <translation>Zéro effort</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="12"/>
        <source>Poor Quality</source>
        <extracomment>2</extracomment>
        <translation>Mauvaise qualité</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="13"/>
        <source>A Little Rough</source>
        <extracomment>3</extracomment>
        <translation>Un peu grossier</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="14"/>
        <source>Unremarkable</source>
        <extracomment>4</extracomment>
        <translation>Peu remarquable</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="15"/>
        <source>Decent</source>
        <extracomment>5</extracomment>
        <translation>Convenable</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="16"/>
        <source>Pretty Good</source>
        <extracomment>6</extracomment>
        <translation>Quand même bon</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="17"/>
        <source>Very Good</source>
        <extracomment>7</extracomment>
        <translation>Vraiment bon</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="18"/>
        <source>Excellent</source>
        <extracomment>8</extracomment>
        <translation>Excellent</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="19"/>
        <source>Exceptional</source>
        <extracomment>9</extracomment>
        <translation>Exceptionnel</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="20"/>
        <source>Legendary</source>
        <extracomment>10</extracomment>
        <translation>Légendaire</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="25"/>
        <source>No Challenge</source>
        <extracomment>1</extracomment>
        <translation>Aucune difficulté</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="26"/>
        <source>Very Easy</source>
        <extracomment>2</extracomment>
        <translation>Vraiment facile</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="27"/>
        <source>Casual</source>
        <extracomment>3</extracomment>
        <translation>Facile</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="28"/>
        <source>Classic SM64</source>
        <extracomment>4</extracomment>
        <translation>SM64 Classique</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="29"/>
        <source>Moderate</source>
        <extracomment>5</extracomment>
        <translation>Moyen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="30"/>
        <source>Challenging</source>
        <extracomment>6</extracomment>
        <translation>Demandant</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="31"/>
        <source>Difficult</source>
        <extracomment>7</extracomment>
        <translation>Difficile</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="32"/>
        <source>Very Difficult</source>
        <extracomment>8</extracomment>
        <translation>Vraiment difficile</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="33"/>
        <source>Extremely Difficult</source>
        <extracomment>9</extracomment>
        <translation>Extrêment difficile</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="34"/>
        <source>Borderline Kaizo</source>
        <extracomment>10</extracomment>
        <translation>Presque Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="39"/>
        <source>Beginner/Introductory Kaizo</source>
        <extracomment>1</extracomment>
        <translation>Kaizo pour débutant</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="40"/>
        <source>Easy Kaizo</source>
        <extracomment>2</extracomment>
        <translation>Kaizo facile</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="41"/>
        <source>Standard Kaizo</source>
        <extracomment>3</extracomment>
        <translation>Kaizo standard</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="42"/>
        <source>Moderate Kaizo</source>
        <extracomment>4</extracomment>
        <translation>Kaizo moyen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="43"/>
        <source>Challenging Kaizo</source>
        <extracomment>5</extracomment>
        <translation>Kaizo demandant</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="44"/>
        <source>Difficult Kaizo</source>
        <extracomment>6</extracomment>
        <translation>Kaizo difficile</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="45"/>
        <source>Very Difficult Kaizo</source>
        <extracomment>7</extracomment>
        <translation>Kaizo vraiment difficile</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="46"/>
        <source>Extremely Difficult Kaizo</source>
        <extracomment>8</extracomment>
        <translation>Kaizo extrêment difficile</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="47"/>
        <source>Hardest humanly possible Kaizo</source>
        <extracomment>9</extracomment>
        <translation>Kaizo presque inhumain</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="48"/>
        <source>TAS/Unbeatable</source>
        <extracomment>10</extracomment>
        <translation>Imbattable</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="59"/>
        <source>Submit</source>
        <translation>Soumettre</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="85"/>
        <source>Not Rated</source>
        <translation>Non évalué</translation>
    </message>
</context>
<context>
    <name>RhdcSaveEditor</name>
    <message>
        <location filename="../src/rhdc/ui/rhdc-save-editor.cpp" line="24"/>
        <source>Save Editor</source>
        <translation>Sauver l&apos;éditeur</translation>
    </message>
</context>
<context>
    <name>RhdcSaveSyncDialog</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="14"/>
        <source>Synchronize Save Files</source>
        <translation>Synchroniser les fichiers sauvegard</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="32"/>
        <source>Save file synchronization is enabled for this hack; however, your save file progress is not currently in sync. Please select which save file you would like to use for this hack.</source>
        <translation>La synchronisation des fichiers sauvegardés est activée pour cette romhack; toutefois, votre progression de votre fichier de sauvegarde n&apos;est pas actuellement synchronisé. S&apos;il-vous-plaît sélectionner quel fichier de sauvegarde vous aimeriez utiliser pour cette romhack.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="67"/>
        <source>Version</source>
        <extracomment>Which version of the hack the save file is for</extracomment>
        <translation>La version</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="72"/>
        <source>Last Saved</source>
        <extracomment>When the save file was last modified</extracomment>
        <translation>Modifié dernièrement</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="77"/>
        <source>Stars</source>
        <extracomment>Number of stars collected on the save file</extracomment>
        <translation>Nombre d&apos;étoiles</translation>
    </message>
</context>
<context>
    <name>RhdcSync</name>
    <message>
        <location filename="../src/rhdc/core/sync.cpp" line="235"/>
        <source>Failed to connect to romhacking.com</source>
        <translation>Échec de la connection à romhacking.com</translation>
    </message>
</context>
<context>
    <name>RhdcViewBubble</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-view-bubble.ui" line="171"/>
        <source>Click here to go to the romhacking.com view.</source>
        <translation>Cliquez ici pour aller à la vue romhacking.com.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-view-bubble.ui" line="214"/>
        <source>Okay</source>
        <translation>Okay</translation>
    </message>
</context>
<context>
    <name>RomListModel</name>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="243"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="244"/>
        <source>Internal Name</source>
        <translation>Nom interne</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="245"/>
        <source>File Path</source>
        <translation>Chemin du fichier</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="246"/>
        <source>Last Played</source>
        <translation>Dernière fois que vous avez joué</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="247"/>
        <source>Play Time</source>
        <translation>Temps de jeu</translation>
    </message>
</context>
<context>
    <name>RomListView</name>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="236"/>
        <source>Delete Save File</source>
        <translation>Effacer le fichier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="237"/>
        <source>[SM64] Edit Save File</source>
        <translation>[SM64] Modifier le fichier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="240"/>
        <source>Import Project64 Save File</source>
        <translation>Importer le fichier de sauvegarde de Project64</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="241"/>
        <source>Show Save File</source>
        <translation>Afficher le ficher de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="246"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="250"/>
        <source>Add to...</source>
        <translation>Ajouter à...</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="257"/>
        <source>New Group</source>
        <translation>Nouveau groupe</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="261"/>
        <source>Remove from %1</source>
        <translation>Enlever de %1</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="267"/>
        <source>Open Containing Folder</source>
        <translation>Ouvrir le fichier contenant les roms</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="268"/>
        <source>Delete ROM</source>
        <translation>Effacer la ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="270"/>
        <location filename="../src/ui/rom-list-view.cpp" line="373"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="272"/>
        <source>Revert ROM Name</source>
        <translation>Rétablir le nom de la ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="277"/>
        <source>Rate Hack</source>
        <translation>Noter le hack</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="280"/>
        <source>Mark as Not Completed</source>
        <translation>Marquer comme Non Complété</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="282"/>
        <source>Mark as Completed</source>
        <translation>Marquer comme Complété</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="305"/>
        <location filename="../src/ui/rom-list-view.cpp" line="364"/>
        <source>Confirm Delete</source>
        <translation>Confirmer l&apos;effacement</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="305"/>
        <source>Are you sure you want to delete your save file?</source>
        <translation>Vous êtes sûr de vouloir supprimer votre fichier de sauvegarde?</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="325"/>
        <source>Create Group</source>
        <translation>Créer un groupe</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="325"/>
        <source>Enter a name for your new group</source>
        <translation>Entrer un nom pour votre nouveau groupe</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="331"/>
        <source>Group Exists</source>
        <translation>Le groupe existe</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="331"/>
        <source>A group with this name already exists</source>
        <translation>Un groupe avec ce nom existe déjà</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="364"/>
        <source>This will completely remove the ROM file from your computer. Are you sure you want to delete this ROM?</source>
        <translation>Ceci va complètement effacer le ficher ROM de votre ordinateur. Vous êtes sûr de vouloir effacer cette ROM?</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="373"/>
        <source>Enter a new name for your rom</source>
        <translation>Entrer un nouveau nom pour votre rom</translation>
    </message>
</context>
<context>
    <name>RomSettingsWidget</name>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="29"/>
        <source>Input Mode</source>
        <translation>Mode input</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="76"/>
        <source>SD Card</source>
        <translation>Carte SD</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="156"/>
        <source>Unlocks the CPU to run at full speed, removing CPU-based lag you would encounter on console. This is almost always safe, but can rarely cause issues on certain ROMs.</source>
        <translation>Débloque le CPU pour qu&apos;il tourne à pleine puissance, effaçant la latence basé sur le CPU que vous renconteriez sur console. Ceci est pesque tout le temps sûr, mais, rarement peut causer des problèmes sur certaines ROMs.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="159"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="171"/>
        <source>Overclock CPU (Recommended)</source>
        <translation>CPU Overclock (Recommandé)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="175"/>
        <source>Alters the vertical interrupt timing. In most cases this either gives a slight performance boost or has no effect, but in some cases it can cause the game to run at the wrong framerate.</source>
        <translation>Altère le timing de l&apos;interruption verticale. Dans la plupart des cas ceci donne un petit gain de performance our n&apos;as pas d&apos;effet, mais, dans certains cas cela peut causer le jeu de ne pas jouer au bon nombre d&apos;images par seconde.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="178"/>
        <source>Overclock VI</source>
        <translation>Overclock VI</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="191"/>
        <source>Overrides save type autodetection, and forces the use of 16kB EEPROM for save data. This breaks roms that use 4kB EEPROM, so only enable this for romhacks that require it.</source>
        <translation>Ignore le type d&apos;auto détection sauvegardé et force l&apos;utilisation de 16kB EEPROM pour les fichiers de sauvegardes. Cela brise les roms qui utilise 4kB EEPROM, seulement à activer pour les romhacks qui le requiert.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="194"/>
        <source>Force 16kB EEPROM</source>
        <translation>Forcer 16kb EEPROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="207"/>
        <source>Use the interpreter core instead of the dynamic recompiler. Slower, but can be useful for debugging.</source>
        <translation>Utiliser l&apos;interprétateur de coeur au lieu du recompilateur dynamique. Plus lent, mais peut être utile pour le débug.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="210"/>
        <source>Use Interpreter Core</source>
        <translation>Utiliser l&apos;interprétateur de coeur</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="223"/>
        <source>Enable widescreen mode by stretching the video. Some ROMs have a widescreen mode that will allow the video to no longer be stretched when it is enabled.</source>
        <translation>Activer le mode plein écran en étirant le jeu. Certaines ROMs ont un mode plein écran qui va permet au jeu de ne plus être étiré quand il est activé.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="226"/>
        <source>Widescreen (Stretched)</source>
        <translation>Plein écran (Étiré)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="239"/>
        <source>Enable widescreen mode by forcing the game to use a widescreen viewport. You will experience visual artifacts on the sides of the screen unless you are playing a romhack specifically designed for this widescreen mode.</source>
        <translation>Activer le mode plein écran en forçant le jeu à utiliser une fenêtre d&apos;affichage plein écran. Vous allez voir certains artéfacts visuel sur les côtés de l&apos;écran à moins que vous jouez à une romhack spécialement conçu pour ce mode plein écran.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="242"/>
        <source>Widescreen (Viewport Hack)</source>
        <translation>Plein écran (Hack fenêtre d&apos;affichage)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="255"/>
        <source>Upscale textures drawn using the TEX_RECT command. This can cause visual artifacts in some games.</source>
        <translation>Monter le niveau des textures dessinées avec la commande TEX_RECT. Ceci peut causer des défauts visuels dans certains jeux.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="258"/>
        <source>Upscale TEXRECTs</source>
        <translation>Monter le TEXRECTs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="271"/>
        <source>Removes the black borders on the left and right sides of the video. Since these pixels are never rendered on real hardware, results will vary depending on the game.</source>
        <translation>Enlève les bandes noires sur la gauche et la droite du jeu. Vu que ces pixels ne sont jamais rendu sur la vraie console, les résultats vont varier dépendamment du jeu.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="274"/>
        <source>Remove Black Borders</source>
        <translation>Enlever les bandes noires</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="287"/>
        <source>Emulate the native framebuffer. This is required for some visual effects to work, but may cause lag on lower end GPUs</source>
        <translation>Emuler le framebuffer natif. Ceci est requit pour le fonctionnement de certains effets visuels, mais peut causer du ralentissement avec des cartes graphiques peu puissantes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="290"/>
        <source>Emulate Framebuffer</source>
        <translation>Émuler le Framebuffer</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="306"/>
        <source>Greatly increases accuracy by rendering decals correctly, but may cause a loss in performance</source>
        <translation>Augmente grandement la précision en affichants les objets correctement, mais peut causer une perte de performance</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="309"/>
        <source>Emulate N64 Depth Compare</source>
        <translation>Émuler N64 Depth Compare</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="373"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning:&lt;/span&gt; The &lt;span style=&quot; font-style:italic;&quot;&gt;Emulate N64 Depth Compare&lt;/span&gt; option does not work on MacOS. Some things may not render correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Attention:&lt;/span&gt; L&apos;option &lt;span style=&quot; font-style:italic;&quot;&gt;D&apos;émuler N64 Depth Compare&lt;/span&gt; ne fonctionne pas sur MacOS. Certaines choses peuvent ne pas être rendu correctement.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="398"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning:&lt;/span&gt; This ROM supports mouse input, but you do not have a hotkey assigned to &lt;span style=&quot; font-style:italic;&quot;&gt;Grab Mouse&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>&quot;Grab Mouse&quot; = &quot;Capture la souris&quot;</translatorcomment>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Attention:&lt;/span&gt; Cette ROM supporte la souris, mais vous n&apos;avez pas de touche de raccourci assigné à celle-ci &lt;span style=&quot; font-style:italic;&quot;&gt;Capture la souris&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="557"/>
        <source>gln64 (Obsolete)</source>
        <translation>gln64 (Obsolète)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="475"/>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="592"/>
        <source>Graphics Plugin</source>
        <translation>Plugins graphiques</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="20"/>
        <source>Sync save files between hack versions</source>
        <translation>Synchroniser les fichiers de sauvegarde entre les différentes versions de la romhack</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="325"/>
        <source>Allows roms with custom RSP microcode to be played, but causes visible seams in models due to plugin inaccuracies.</source>
        <translation>Pemettre les roms avec du code personnalisé RSP d&apos;être joué, mais cela peut causer des fissures sur certains modèles à cause du plugin.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="348"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Not enabling CPU overclocking is likely to result in a laggy experience. This option should only be used for speedruns or testing where you want to approximate console CPU lag.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Avertissement: &lt;/span&gt;Ne pas activer l&apos;overclock du CPU va très probablement résulter en une expérience avec plein de ralentissement. Cette option devrait seulement être utilisé pour les speedruns ou tester où vous voulez approximer les ralentissement de CPU de la console.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="484"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="539"/>
        <source>ParaLLEl (Recommended - very accurate to console)</source>
        <translation>ParaLLEl (Recommandé - se rapprochant beaucoup de la console)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="497"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="540"/>
        <source>GLideN64 (Recommended for lower end computers)</source>
        <translation>GLideN64 (Recommandé pour les ordinateurs plus faible)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="507"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="541"/>
        <source>OGRE (Needed by some older romhacks)</source>
        <translation>OGRE (Demandé par certaines romhacks plus anciennes)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="601"/>
        <source>ParaLLEl (Recommended)</source>
        <translation>ParaLLEl (Recommandé)</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="171"/>
        <source>Overclock CPU</source>
        <translation>CPU Overclock</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="331"/>
        <source>Show Fewer Plugins</source>
        <translation>Montrer moins de plugins</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="331"/>
        <source>Show More Plugins</source>
        <translation>Montrer plus de plugins</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="342"/>
        <source>This ROM supports mouse input. After launching the emulator, your mouse will be captured by the emulator. Press %1 if you need to free the mouse cursor.</source>
        <translation>Cette ROM supporte les actions de la souris. Après avoir démarré l&apos;émulateur, votre souris sera utilisé par l&apos;émulateur. Appuyez sur %1 si vous avez besoin de libérer le curseur de la souris.</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="521"/>
        <source>(recommended by hack author)</source>
        <translation>(recommandé by l&apos;auteur du hack)</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="613"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
</context>
<context>
    <name>RomSourceDialog</name>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="14"/>
        <source>Manage ROM Sources</source>
        <translation>Gérer la source des ROMs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="31"/>
        <source>ROM Search Folders</source>
        <translation>Rechercher le dossiers des ROMs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="68"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="82"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="77"/>
        <source>New Source</source>
        <translation>Nouvelle source</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="103"/>
        <source>ROM Source</source>
        <translation>Source des ROMs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="114"/>
        <source>Browse for a folder</source>
        <translation>Parcourir pour un fichier</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="121"/>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="431"/>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="502"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="134"/>
        <source>Also scan all subfolders</source>
        <translation>Scanner aussi les sous-fichiers</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="137"/>
        <source>Recursive</source>
        <translation>Récurrent</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="168"/>
        <source>Ignore directories that begin with a period character</source>
        <translation>Ignorer les chemins qui commencent par un point</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="171"/>
        <source>Ignore hidden directories</source>
        <translation>Ignorer les chemins cachés</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="183"/>
        <source>Max Depth</source>
        <translation>Profondeur maximale</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="236"/>
        <source>Follow symbolic links to folders and ROMs</source>
        <translation>Suivre les liens symboliques jusqu&apos;aux dossiers et ROMs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="239"/>
        <source>Follow Symlinks</source>
        <translation>Suivre les liens symboliques</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="256"/>
        <source>Automatically add to groups</source>
        <translation>Ajouter automatique aux groupes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="289"/>
        <source>Manage Groups</source>
        <translation>gérer les groupes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="317"/>
        <source>Individual ROMs</source>
        <translation>ROMs individuelle</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="348"/>
        <source>Forget Selected ROM</source>
        <translation>Oublier la ROM sélectionner</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="359"/>
        <source>Add New ROM(s)</source>
        <translation>Ajouter de nouvelle(s) ROM(s)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="388"/>
        <source>BPS Patches</source>
        <translation>Correction BPS</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="417"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="298"/>
        <source>Patch File</source>
        <translation>Corriger le fichier</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="444"/>
        <source>Base ROM</source>
        <translation>La ROM de base</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="450"/>
        <source>Automatic (Search Existing ROMs)</source>
        <translation>Automatique (Rechercher des ROMs existantes)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="463"/>
        <source>Provided ROM:</source>
        <translation>ROM fourni:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="532"/>
        <source>Apply Patch</source>
        <translation>Appliquer le corriger</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="275"/>
        <source>Select one or more ROMs</source>
        <translation>Sélectionner une ou plusieurs ROMs</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="297"/>
        <source>Select a .bps patch file</source>
        <translation>Sélectionner un fichier .bps de correction</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="308"/>
        <source>Select a ROM</source>
        <translation>Sélectionner une ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="337"/>
        <source>Patch Applied</source>
        <translation>Correction appliquer</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="337"/>
        <source>Saved patched rom to %1</source>
        <translation>Sauvegarder la rom corrigé à %1</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="352"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="355"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="359"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="361"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="365"/>
        <source>Patch Failed</source>
        <translation>Correction échoué</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="352"/>
        <source>Failed to patch ROM. The .bps patch appears to be invalid.</source>
        <translation>Échec de la correction de la ROM. La correction .bps doit est invalide.</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="355"/>
        <source>Failed to patch ROM. The base ROM does not match what the patch expects.</source>
        <translation>Échec de la correction de la ROM. La ROM the base ne correspond pas à ce que la correction s&apos;attendait.</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="359"/>
        <source>Failed to patch ROM. The base rom required to patch is not known to Parallel Launcher.</source>
        <translation>Échec de la correction de la ROM. La rom de base qui est requis pour la correction est inconnu à Parallel Launcher.</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="361"/>
        <source>Failed to patch ROM. The base ROM is missing or invalid.</source>
        <translation>Échec de la correction de la ROM. La ROM de base est soit manquante ou invalide.</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="365"/>
        <source>Failed to patch ROM. An error occurred while writing the patched ROM to disk.</source>
        <translation>Échec de la correction de la ROM. Une erreur s&apos;est produite pendant l&apos;écriture de la ROM corrigé sur le disque.</translation>
    </message>
</context>
<context>
    <name>SaveFileEditorDialog</name>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="14"/>
        <source>Edit SM64 Save File</source>
        <translation>Modifier le fichier de sauvegarde de SM64</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;This is a save file editor for Super Mario 64 and SM64 romhacks. Attempting to use this save file editor with other ROMs will corrupt your save file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Attention: &lt;/span&gt;Ceci est un modificateur de fichier pour Super Mario 64 et les romhacks de SM64. Essayer d&apos;utiliser ce modificateur de fichiers de sauvegarde avec d&apos;autres ROMs va corrompre  votre fichier de sauvegarde.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="41"/>
        <source>Save Slot:</source>
        <translation>Sauvegarder dans l&apos;emplacement:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="55"/>
        <source>Slot A (Empty)</source>
        <translation>Emplacement A (Vide)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="60"/>
        <source>Slot B (Empty)</source>
        <translation>Emplacement B (Vide)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="65"/>
        <source>Slot C (Empty)</source>
        <translation>Emplacement C (Vide)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="70"/>
        <source>Slot D (Empty)</source>
        <translation>Emplacement D (Vide)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="95"/>
        <source>Delete Save Slot</source>
        <translation>Effacer l&apos;emplacement de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="106"/>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="62"/>
        <source>Edit Save Slot</source>
        <translation>Modifier l&apos;emplacement de sauvegarde</translation>
    </message>
    <message>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="15"/>
        <source>Slot %1 (Empty)</source>
        <translation>Emplacement %1 (Vide)</translation>
    </message>
    <message>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="18"/>
        <source>Slot %1 (%2 Stars)</source>
        <translation>Emplacement %1 (%2 Étoiles)</translation>
    </message>
    <message>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="66"/>
        <source>Create Save Slot</source>
        <translation>Créer un emplacement de sauvegarde</translation>
    </message>
</context>
<context>
    <name>SaveSlotEditorDialog</name>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="14"/>
        <source>Edit Save Slot (SM64)</source>
        <translation>Modifier l&apos;emplacement de sauvegarde (SM64)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="20"/>
        <source>Show flags and stars unused in the vanilla game</source>
        <translation>Montrer les drapeaux et les étoiles inutiliser dans le jeu de base</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="32"/>
        <source>Flags</source>
        <translatorcomment>Flags as in the technical term for a bit having some meaning; not an actual physical flag</translatorcomment>
        <translation>Drapeaux</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="74"/>
        <source>Caps Unlocked</source>
        <translation>Casquettes débloqué</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="80"/>
        <source>Wing Cap</source>
        <translation>Casquette-Ailée</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="93"/>
        <source>Metal Cap</source>
        <translation>Caquette-Metal</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="106"/>
        <source>Vanish Cap</source>
        <translation>Casquette-Invisible</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="122"/>
        <source>Keys Collected</source>
        <translation>Clés collectés</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="128"/>
        <source>Basement Key</source>
        <translation>Clé du sous-sol</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="141"/>
        <source>Upstairs Key</source>
        <translation>Clé de l&apos;étage supérieur</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="157"/>
        <source>Unlocked Doors</source>
        <translation>Portes débloqués</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="163"/>
        <source>Basement Key Door</source>
        <translation>Clé de la porte du sous-sol</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="176"/>
        <source>Upstairs Key Door</source>
        <translation>Clé de la porte de l&apos;étage supérieur</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="189"/>
        <source>Peach&apos;s Secret Slide Door</source>
        <translation>porte à &quot;glissade de la princesse&quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="202"/>
        <source>Whomp&apos;s Fortress Door</source>
        <translation>porte à &quot;forteresse de whomp&quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="215"/>
        <source>Cool Cool Mountain Door</source>
        <translation>porte à &quot;montagne gla-gla&quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="228"/>
        <source>Jolly Rodger Bay Door</source>
        <translation>porte à &quot;baie des pirates&quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="241"/>
        <source>Dark World Door</source>
        <translation>porte à &quot;Bowser des tenebres&quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="254"/>
        <source>Fire Sea Door</source>
        <translation>porte à &quot;Bowser des laves&quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="267"/>
        <source>50 Star Door</source>
        <translation>La porte des 50 étoiles</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="283"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="289"/>
        <source>Moat Drained</source>
        <translation>Douves drainé</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="302"/>
        <source>Bowser&apos;s Sub Gone</source>
        <translation>Le sous-marin de Bowser est parti</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="318"/>
        <source>Unused</source>
        <translation>Inutiliser</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="366"/>
        <source>Lost/Stolen Cap</source>
        <translation>Casquette perdu/volé</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="374"/>
        <source>Level</source>
        <translation>Niveau</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="395"/>
        <source>Area</source>
        <translation>Zone</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="427"/>
        <source>Cap Stolen By:</source>
        <translation>Casquette volé par:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="434"/>
        <source>The flag indicates that Mario&apos;s cap is on the ground, however, when loading a save file, a cap that is on the ground is either given back to Mario or moved to the appropriate NPC for the area, so it will never actually be on the ground.</source>
        <translation>Le drapeau indique que la casquette de Mario est sur le sol, par contre, quand vous charger le fichier de sauvegarde, une casquette qui est sur le sol est soit re-donné à Mario ou est retourné au PNJ la zone approprié, ducoup elle ne va jamais être vraiment sur le sol.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="437"/>
        <source>Automatic*</source>
        <translation>Automatique*</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="450"/>
        <source>Klepto (Bird)</source>
        <translation>Klepto (Oiseau)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="463"/>
        <source>Ukiki (Monkey)</source>
        <translation>Ukiki (Singe)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="476"/>
        <source>Mr. Blizzard (Snowman)</source>
        <translation>Mr. Blizzard (Bonhomme de neige)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="499"/>
        <source>Stars and Coins</source>
        <translation>Étoiles et pièces</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="44"/>
        <source>Big Boo&apos;s Haunt</source>
        <translation>Manoir de Big Boo</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="45"/>
        <source>Cool Cool Mountain</source>
        <translation>Montagne gla-gla</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="46"/>
        <source>Castle Interior</source>
        <translation>L&apos;intérieur du château</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="47"/>
        <source>Hazy Maze Cave</source>
        <translation>Caverne brumeuse</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="48"/>
        <source>Shifting Sand Land</source>
        <translation>Sables trop mouvants</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="49"/>
        <source>Bob-Omb Battlefield</source>
        <translation>Bataille de bob-omb</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="50"/>
        <source>Snowman&apos;s Land</source>
        <translation>Chez le roi des neiges</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="51"/>
        <source>Wet Dry World</source>
        <translation>monde trempe-sèche</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="52"/>
        <source>Jolly Rodger Bay</source>
        <translation>Baie des pirates</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="53"/>
        <source>Tiny-Huge Island</source>
        <translation>Ile grands-petits</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="54"/>
        <source>Tick Tock Clock</source>
        <translation>Horloge tic-tac</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="55"/>
        <source>Rainbow Ride</source>
        <translation>Course arc-en-ciel</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="56"/>
        <source>Castle Grounds</source>
        <translation>Le sol du château</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="57"/>
        <source>Bowser in the Dark World</source>
        <translation>Bowser des tenebres</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="58"/>
        <source>Vanish Cap Under the Moat</source>
        <translation>Invisible sous les douves</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="59"/>
        <source>Bowser in the Fire Sea</source>
        <translation>Bowser des laves</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="60"/>
        <source>Secret Aquarium</source>
        <translation>Aquarium secret</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="61"/>
        <source>Bowser in the Sky</source>
        <translation>Bowser des cieux</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="62"/>
        <source>Lethal Lava Land</source>
        <translation>Laves fatales</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="63"/>
        <source>Dire Dire Docks</source>
        <translation>Affreux bassin</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="64"/>
        <source>Whomp&apos;s Fortress</source>
        <translation>Forteresse de whomp</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="65"/>
        <source>End Screen</source>
        <translation>Écran de fin</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="66"/>
        <source>Castle Courtyard</source>
        <translation>La cour du château</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="67"/>
        <source>Peach&apos;s Secret Slide</source>
        <translation>Glissade de la princesse</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="68"/>
        <source>Cavern of the Metal Cap</source>
        <translation>Mine des casquettes-metal</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="69"/>
        <source>Tower of the Wing Cap</source>
        <translation>Interrupteur de la tour ailee</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="70"/>
        <source>Bowser 1</source>
        <translation>Bowser 1</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="71"/>
        <source>Winged Mario over the Rainbow</source>
        <translation>Au-dela de l&apos;arc-en-ciel</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="73"/>
        <source>Bowser 2</source>
        <translation>Bowser 2</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="74"/>
        <source>Bowser 3</source>
        <translation>Bowser 3</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="76"/>
        <source>Tall, Tall Mountain</source>
        <translation>Trop haute montagne</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="11"/>
        <source>8 Red Coins</source>
        <translation>8 pièces rouges</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="12"/>
        <source>100 Coins</source>
        <translation>100 pièces</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="17"/>
        <source>Toad 1</source>
        <translation>Toad 1</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="18"/>
        <source>Toad 2</source>
        <translation>Toad 2</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="19"/>
        <source>Toad 3</source>
        <translation>Toad 3</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="20"/>
        <source>MIPS 1</source>
        <translation>MIPS 1</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="21"/>
        <source>MIPS 2</source>
        <translation>MIPS 2</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="27"/>
        <source>Big Bob-Omb on the Summit</source>
        <translation>Roi bob-omb du sommet</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="28"/>
        <source>Footface with Koopa the Quick</source>
        <translation>Course contre koopa-rapido</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="29"/>
        <source>Shoot to the Island in the Sky</source>
        <translation>Canon vers l&apos;ile perchee</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="30"/>
        <source>Find the 8 Red Coins</source>
        <translation>Les 8 pieces rouges</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="31"/>
        <source>Mario Wings to the Sky</source>
        <translation>Vol du mario aile</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="32"/>
        <source>Behind Chain Chomp&apos;s Gate</source>
        <translation>Derriere la cage de chomp</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="37"/>
        <source>Chip Off Whomp&apos;s Block</source>
        <translation>Destruction du gros whomp</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="38"/>
        <source>To the Top of the Fortress</source>
        <translation>En haut de la forteresse</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="39"/>
        <source>Shoot into the Wild Blue</source>
        <translation>Canon vers les cieux</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="40"/>
        <source>Red Coins on the Floating Isle</source>
        <translation>Pieces rouges dans les airs</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="41"/>
        <source>Fall onto the Caged Island</source>
        <translation>Tombez dans la cage perchee</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="42"/>
        <source>Blast Away the Wall</source>
        <translation>Canon vers un mur...</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="47"/>
        <source>Plunder in the Sunken Ship</source>
        <translation>Pillez le bateau coule</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="48"/>
        <source>Can the Eel Come Out to Play?</source>
        <translation>L&apos;anguille veut faire joujou?</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="49"/>
        <source>Treasure of the Ocean Cave</source>
        <translation>Tresor dans la caverne</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="50"/>
        <source>Red Coins on the Ship Afloat</source>
        <translation>Pieces rouges a bord!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="51"/>
        <source>Blast to the Stone Pillar</source>
        <translation>Canon vers les piliers</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="52"/>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="110"/>
        <source>Through the Jet Stream</source>
        <translation>Dans le geyser</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="57"/>
        <source>Slip Slidin&apos; Away</source>
        <translation>Glissade sur glace</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="58"/>
        <source>Li&apos;l Penguin Lost</source>
        <translation>Petit pingouin perdu</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="59"/>
        <source>Big Penguin Race</source>
        <translation>Course du pingouin</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="60"/>
        <source>Frosty Slide for 8 Red Coins</source>
        <translation>8 pieces rouges givrees</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="61"/>
        <source>Snowman&apos;s Lost his Head</source>
        <translation>Bonhomme de neige sans tete</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="62"/>
        <source>Wall Kicks Will Work</source>
        <translation>Murs pour rebondir</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="67"/>
        <source>Go on a Ghost Hunt</source>
        <translation>Cherchez le big boo</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="68"/>
        <source>Ride Big Boo&apos;s Merry-Go-Round</source>
        <translation>Manege des boos pas beaux</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="69"/>
        <source>Secret of the Haunted Books</source>
        <translation>Secret des livres hantes</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="70"/>
        <source>Seek the 8 Red Coins</source>
        <translation>Trouvez les 8 pieces rouges</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="71"/>
        <source>Big Boo&apos;s Balcony</source>
        <translation>Balcon de Big Boo</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="72"/>
        <source>Eye to Eye in the Secret Room</source>
        <translation>Un oeil sur les salles secretes</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="77"/>
        <source>Swimming Beast in the Cavern</source>
        <translation>Gros bibi de la caverne</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="78"/>
        <source>Elevate for 8 Red Coins</source>
        <translation>Chariot pour pieces rouges</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="79"/>
        <source>Metal-Head Mario Can Move!</source>
        <translation>Course de metal mario</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="80"/>
        <source>Navigating the Toxic Maze</source>
        <translation>Dans le labyrinthe toxique</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="81"/>
        <source>A-maze-ing Emergency Exit</source>
        <translation>Au-dessus des gaz...</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="82"/>
        <source>Watch for Rolling Rocks</source>
        <translation>Attention aux boulets</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="87"/>
        <source>Boil the Big Bully</source>
        <translation>Cramez le gros moche</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="88"/>
        <source>Bully the Bullies</source>
        <translation>Poussez les petits moches</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="89"/>
        <source>8-Coin Puzzle with 15 Pieces</source>
        <translation>Puzzle de 8 pieces rouges</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="90"/>
        <source>Red-Hot Log Rolling</source>
        <translation>Balade sur la buche</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="91"/>
        <source>Hot-Foot-it into the Volcano</source>
        <translation>Chaud dans le volcan</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="92"/>
        <source>Elevator Tour in the Volcano</source>
        <translation>Ascenceur dans le volcan</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="97"/>
        <source>In the Talons of the Big Bird</source>
        <translation>Dans les griffes du vautour</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="98"/>
        <source>Shining Atop the Pyramid</source>
        <translation>Au sommet de la pyramide</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="99"/>
        <source>Inside the Ancient Pyramid</source>
        <translation>Dans l&apos;ancienne pyramide</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="100"/>
        <source>Stand Tall on the Four Pillars</source>
        <translation>Les quatre piliers de la foi</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="101"/>
        <source>Free Flying for 8 Red Coins</source>
        <translation>Vol vers les pieces rouges</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="102"/>
        <source>Pyramid Puzzle</source>
        <translation>Enigme de la pyramide</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="107"/>
        <source>Board Bowser&apos;s Sub</source>
        <translation>Sous-marin de Bowser</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="108"/>
        <source>Chests in the Current</source>
        <translation>Coffres dans le courant</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="109"/>
        <source>Pole-Jumping for Red Coins</source>
        <translation>Perches pour pieces rouges</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="111"/>
        <source>The Manta Ray&apos;s Reward</source>
        <translation>Recompense de la raie</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="112"/>
        <source>Collect the Caps...</source>
        <translation>Association de casquettes...</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="117"/>
        <source>Snowman&apos;s Big Head</source>
        <translation>Grosse tete du bonhomme</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="118"/>
        <source>Chill with the Bully</source>
        <translation>Combattez le gros moche givre</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="119"/>
        <source>In the Deep Freeze</source>
        <translation>Dans le glaçon</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="120"/>
        <source>Whirl from the Freezing Pond</source>
        <translation>Vrille depuis le lac</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="121"/>
        <source>Shell Shreddin&apos; for Red Coins</source>
        <translation>Carapace pour pieces rouges</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="122"/>
        <source>Into the Igloo</source>
        <translation>Dans l&apos;igloo</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="127"/>
        <source>Shocking Arrow Lifts!</source>
        <translation>Mini plates-formes flechees!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="128"/>
        <source>Top o&apos; the Town</source>
        <translation>Sur les toits</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="129"/>
        <source>Secrets in the Shallows &amp; Sky</source>
        <translation>Enigme des chiffres caches</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="130"/>
        <source>Express Evelator--Hurry Up!</source>
        <translation>Ascenseur express!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="131"/>
        <source>Go to Town for Red Coins</source>
        <translation>Pieces rouges: ville engloutie</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="132"/>
        <source>Quick Race through Downtown!</source>
        <translation>Course en ville</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="137"/>
        <source>Scale the Mountain</source>
        <translation>Escaladez la montagne</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="138"/>
        <source>Mystery of the Monkey&apos;s Cage</source>
        <translation>Mystere de la cage du singe</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="139"/>
        <source>Scary &apos;Shrooms, Red Coins</source>
        <translation>Pieces rouges sur champignon</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="140"/>
        <source>Mysterious Mountainside</source>
        <translation>Paroi mysterieuse</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="141"/>
        <source>Breathtaking View from Bridge</source>
        <translation>Panorama du pont</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="142"/>
        <source>Blast to the Lonely Mushroom</source>
        <translation>Canon vers le champignon</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="147"/>
        <source>Pluck the Piranha Flower</source>
        <translation>Detruisez les maxi-carnivores</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="148"/>
        <source>The Tip Top of the Huge Island</source>
        <translation>Au sommet de l&apos;ile des geants</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="149"/>
        <source>Rematch with Koopa the Quick</source>
        <translation>Koopa-rapido, la vengeance</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="150"/>
        <source>Five Itty Bitty Secrets</source>
        <translation>Cinq tout petits secrets</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="151"/>
        <source>Wiggler&apos;s Red Coins</source>
        <translation>Les pieces rouges du Gigoteur</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="152"/>
        <source>Make Wiggler Squirm</source>
        <translation>Enervez le Gigoteur</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="157"/>
        <source>Roll into the Cage</source>
        <translation>Roulements dans la cage</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="158"/>
        <source>The Pit and the Pendulums</source>
        <translation>Les trous et le pendule</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="159"/>
        <source>Get a Hand</source>
        <translation>Chevauchez l&apos;aiguille</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="160"/>
        <source>Stomp on the Thwomp</source>
        <translation>L&apos;ecrabouilleur</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="161"/>
        <source>Timed Jumps on Moving Bars</source>
        <translation>Sauts synchronises</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="162"/>
        <source>Stop Time for Red Coins</source>
        <translation>L&apos;heure pile pour les pieces</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="167"/>
        <source>Cruiser Crossing the Rainbow</source>
        <translation>Croisiere sur l&apos;arc-en-ciel</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="168"/>
        <source>The Big House in the Sky</source>
        <translation>Manoir dans les airs</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="169"/>
        <source>Coins Amassed in a Maze</source>
        <translation>Labyrinthe des pieces rouges</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="170"/>
        <source>Swingin&apos; in the Breeze</source>
        <translation>Balançoire celeste</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="171"/>
        <source>Tricky Triangles!</source>
        <translation>Vicieux triangles!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="172"/>
        <source>Somewhere Over the Rainbow</source>
        <translation>L&apos;arc-en-ciel et au-dela</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="227"/>
        <source>Act 1 Star</source>
        <translation>Acte 1</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="228"/>
        <source>Act 2 Star</source>
        <translation>Acte 2</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="229"/>
        <source>Act 3 Star</source>
        <translation>Acte 3</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="230"/>
        <source>Act 4 Star</source>
        <translation>Acte 4</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="231"/>
        <source>Act 5 Star</source>
        <translation>Acte 5</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="232"/>
        <source>Act 6 Star</source>
        <translation>Acte 6</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="233"/>
        <source>100 Coin Star</source>
        <translation>100 pièces</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="234"/>
        <source>Cannon Status</source>
        <translation>Statut des canons</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="235"/>
        <source>Coin High Score</source>
        <translation>Record des pièces</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="264"/>
        <source>Peach&apos;s Castle</source>
        <translation>Château de Peach</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="317"/>
        <source>-- None --</source>
        <translation>-- Aucun --</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="44"/>
        <source>User Interface</source>
        <translation>L&apos;interface utilisateur</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="49"/>
        <source>Emulation</source>
        <translation>Émulation</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="54"/>
        <source>BPS Patches</source>
        <translation>Corrections BPS</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="59"/>
        <source>GFX Plugins</source>
        <translation>Plugins GFX</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="64"/>
        <source>Updaters</source>
        <translation>Updaters</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="69"/>
        <source>Romhacking.com</source>
        <translation>Romhacking.com</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="118"/>
        <source>Theme (Requires Restart)</source>
        <translation>Thème (Redémarrage requis)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="134"/>
        <source>Dark Mode (Requires Restart)</source>
        <translation>Mode sombre (Redémarrage requis)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="143"/>
        <source>Language (Requires Restart)</source>
        <translation>Langue (Redémarrage requis)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="158"/>
        <source>Hide launcher while playing ROM</source>
        <translation>Cacher le lanceur quand vous jouez à la ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="165"/>
        <source>Check for updates on startup</source>
        <translation>Vérifier les mises à jour au démarrage</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="175"/>
        <source>Discord &quot;Now Playing&quot; Integration</source>
        <translation>L&apos;intégration Discord &quot; En train de jouer &quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="182"/>
        <source>Show additional rom settings meant for advanced users</source>
        <translation>Afficher des réglages supplémentaires de la rom fait pour les utilisateurs expérimenté</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="185"/>
        <source>Show advanced ROM options</source>
        <translation>Afficher les options avancés de la ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="192"/>
        <source>Visible Columns</source>
        <translation>Colonnes visibles</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="198"/>
        <source>The full path to the file</source>
        <translation>Le chemin complet vers le fichier</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="201"/>
        <source>File Path</source>
        <translation>Chemin du fichier</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="211"/>
        <source>The filename without the extension</source>
        <translation>Le nom du fichier sans l&apos;extension</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="214"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="227"/>
        <source>The name stored in the ROM itself</source>
        <translation>Le nom stocké dans la ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="230"/>
        <source>Internal Name</source>
        <translation>Nom interne</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="237"/>
        <source>The date you last played the ROM</source>
        <translation>La date de la dernière fois vous avez joué à cette ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="240"/>
        <source>Last Played</source>
        <translation>Dernière fois que vous avez joué</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="250"/>
        <source>The total time spent playing the ROM</source>
        <translation>Le temps total que vous avez jouer à cette ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="253"/>
        <source>Total Play Time</source>
        <translation>Temps de jeu total</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="433"/>
        <source>Window Scale</source>
        <translation>Taille de la fenêtre</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="404"/>
        <source>Default ParallelN64 Plugin</source>
        <translation>Plugin par défaut ParallelN64</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="443"/>
        <source>Default Mupen Plugin</source>
        <translation>Plugin par défaut Mupen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="380"/>
        <source>Default Emulator Core</source>
        <translation>Le core de l&apos;émulateur par défaut</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="481"/>
        <source>Fullscreen</source>
        <translation>Écran plein</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="495"/>
        <source>Pause when emulator loses focus</source>
        <translation>Met en pause l&apos;émulateur quand il n&apos;est plus le centre d&apos;intérêt</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="502"/>
        <source>Do not show RetroArch notifications such as &quot;Saved state to slot #0&quot; or &quot;Device disconnected from port #1&quot;</source>
        <translation>Ne pas montrer les notifications de RetroArch comme &quot;Sauvegarde instantanée enregistrée vers l&apos;emplacement #0&quot; ou &quot;Périphérique déconnecté du port #1&quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="505"/>
        <source>Hide notifications</source>
        <translation>Cacher les notifications</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="537"/>
        <source>Reset RetroArch Config</source>
        <translation>Réinitialiser la configuration de RetroArch</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="589"/>
        <source>When loading a BPS patch, save the patched ROM...</source>
        <translation>Quand vous charger une correction BPS, sauvegarder la ROM corrigé...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="596"/>
        <source>To the same folder as the patch</source>
        <translation>Au même dossier que le correctif</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="609"/>
        <source>To this folder:</source>
        <translation>À ce dossier:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="651"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1350"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="686"/>
        <source>Global Settings</source>
        <translation>Réglages globaux</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="710"/>
        <source>Upscaling</source>
        <translation>Monter le niveau</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="726"/>
        <source>Native (x1 - 320x240)</source>
        <translation>Natif (x1 - 320x240)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="764"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="893"/>
        <source>Anti-Aliasing</source>
        <translation>Anti-Aliasing</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="799"/>
        <source>Filtering</source>
        <translation>Filtrage</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="810"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="815"/>
        <source>Anti-Alias</source>
        <translation>Anti-Alias</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="820"/>
        <source>Anti-Alias + Dedither</source>
        <translation>Anti-Alias + Dedither</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="825"/>
        <source>Anti-Alias + Blur</source>
        <translation>Anti-Alias + Flou</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="830"/>
        <source>Filtered</source>
        <translation>Filtré</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="992"/>
        <source>Removes the black borders on the left and right sides of the video. Since these pixels are never rendered on real hardware, results will vary depending on the game.</source>
        <translation>Enlève les bandes noires sur la gauche et la droite du jeu. Vu que ces pixels ne sont jamais rendu sur la vraie console, les résultats vont varier dépendamment du jeu.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="995"/>
        <source>Remove Black Borders</source>
        <translation>Enlever les bandes noires</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="903"/>
        <source>Use N64 3-Point Filtering</source>
        <translation>Utiliser N64 3-point filtrage</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="74"/>
        <source>Developer</source>
        <translation>Développeur</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="79"/>
        <source>System Clock</source>
        <translation>Heure système</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="453"/>
        <source>Audio Driver</source>
        <translation>Pilote audio</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="488"/>
        <source>Vsync</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="522"/>
        <source>Emulate SummerCart64 SD card interface</source>
        <translation>Émuler l&apos;interface de communication SummerCart64 pour la carte SD</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="927"/>
        <source>Default ROM Settings</source>
        <translation>Réglages par défaut de la ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="951"/>
        <source>Upscale textures drawn using the TEX_RECT command. This can cause visual artifacts in some games.</source>
        <translation>Monter le niveau des textures dessinées avec la commande TEX_RECT. Ceci peut causer des défauts visuels dans certains jeux.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="954"/>
        <source>Upscale TEXRECTs</source>
        <translation>Monter le TEXRECTs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="974"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1096"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1140"/>
        <source>Apply to all existing ROMs</source>
        <translation>Appliquer à toutes les ROMs existantes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1070"/>
        <source>Emulate the native framebuffer. This is required for some visual effects to work, but may cause lag on lower end GPUs</source>
        <translation>Emuler le framebuffer natif. Ceci est requit pour le fonctionnement de certains effets visuels, mais peut causer du ralentissement avec des cartes graphiques peu puissantes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1073"/>
        <source>Emulate Framebuffer</source>
        <translation>Émuler le Framebuffer</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1114"/>
        <source>Greatly increases accuracy by rendering decals correctly, but may cause a loss in performance</source>
        <translation>Augmente grandement la précision en affichants les objets correctement, mais peut causer une perte de performance</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1117"/>
        <source>Emulate N64 Depth Compare</source>
        <translation>Émuler N64 Depth Compare</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1177"/>
        <source>Enable RetroArch Automatic Updates</source>
        <translation>Activer les mises à jour automatique de RetroArch</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1187"/>
        <source>Enable Mupen64Plus-Next Automatic Updates</source>
        <translation>Activer les mises à jour automatique de Mupen64Plus-Next</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1215"/>
        <source>Use development branch</source>
        <translation>Utiliser la branche de développement</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1229"/>
        <source>Update Interval</source>
        <translation>Mettre à jour Interval</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1249"/>
        <source>Every Launch</source>
        <translation>Tous les lancements</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1254"/>
        <source>Daily</source>
        <translation>Quotidien</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1259"/>
        <source>Weekly</source>
        <translation>Hebdomadaire</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1264"/>
        <source>Monthly</source>
        <translation>Mensuel</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1289"/>
        <source>Check for Updates</source>
        <translation>Vérifier les mises à jour</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1328"/>
        <source>Download Directory</source>
        <translation>Téléchargement du répertoire</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1340"/>
        <source>Sets the directory that hacks downloaded from romhacking.com will be saved to. If you change this setting later, your roms will automatically be moved to the new directory.</source>
        <translation>Met en place le répertoire où les hacks téléchargés de romhacking.com seront sauvés. Si vous changer ce paramètre plus tard, vos roms seront automatiquement déplacer dans le nouveau répertoire.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1379"/>
        <source>Enable Star Display</source>
        <translation>Activer Star Display</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1408"/>
        <source>If checked, Parallel Launcher will still show a star display with a default layout if the hack author did not provide one.</source>
        <translation>Si coché, Parallel Launcher va toujours montrer un star display avec une mise en page de base si l&apos;auteur du hack n&apos;en a pas prodigué une.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1411"/>
        <source>Show star display for hacks without a
star layout</source>
        <translation>Montrer star display pour les hacks
sans mise en page d&apos;étoiles</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1425"/>
        <source>If checked, Parallel Launcher will submit your highest star count across all save slots to romhacking.com; otherwise, it will only submit your star count in slot A.</source>
        <translation>Si coché, Parallel Launcher va soumettre votre plus haut nombre d&apos;étoile parmis tous les fichers de sauvegardes sur romhacking.com; sinon, il va seulement soumettre votre nombre d&apos;étoile dans le fichier de sauvegarde A.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1428"/>
        <source>Check all save slots</source>
        <translation>Vérifie tous les fichiers de sauvegardes</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1435"/>
        <source>If checked, Parallel Launcher will use the GLideN64 plugin instead of the ParaLLEl plugin when it guesses that ParrLLEl is probably the best plugin to use. Only check this if your computer has an old GPU with poor Vulkan support. If you still experience lag even with GlideN64, you may need to disable &apos;Emulate Framebuffer&apos; and/or &apos;Emulate N64 Depth Compare&apos; in the GFX Plugins section of your Parallel Launcher settings.</source>
        <translation>Si coché, Parallel Launcher va utiliser le plugin GLideN64 au lieu du plugin ParaLLel s&apos;il pense que le ParrLLEl est probablement le meilleur plugin à utiliser. Cocher seulement ceci si votre ordinateur a une vieille carte graphique avec un mauvais support pour Vulkan. Si vous rencontrez encore des lags même avec GLideN64, vous devez peut-être désactiver &apos;Emulate Framebuffer&apos; et/ou &apos;EmulateN64 Depth Compare&apos; dans la section des plugins GFX depuis les paramètres de Parallel Launcher.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1438"/>
        <source>Prefer HLE plugins</source>
        <translation>Préférence pour les plugins HLE</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1445"/>
        <source>If checked, Parallel Launcher will always default to using 4:3 resolution, even if the recommended settings indicate that widescreen is supported</source>
        <translation>Si coché, Parallel Launcher va toujours utiliser par défaut la résolution 4:3, même si les options recommandés indique que le plein écran est supporté</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1448"/>
        <source>Ignore widescreen hint</source>
        <translation>Ignorer la suggestion de plein écran</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1472"/>
        <source>Emulate IS Viewer hardware and display any messages sent from the ROM in a separate window</source>
        <translation>Émule le composant IS-Viewer, et affiche tout les messages envoyés depuis la ROM dans une fenêtre séparée</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1475"/>
        <source>Enable IS Viewer (ParallelN64 only)</source>
        <translation>Activer IS-Viewer (ParallelN64)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1503"/>
        <source>History Size</source>
        <translation>Taille de l&apos;historique</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1572"/>
        <source>The following options only apply to the ParallelN64 emulator core. The mupen64plus core will always sync to the system clock.</source>
        <translation>Les options suivantes ne s’appliquent qu&apos;au core de l&apos;émulateur ParallelN64. Le core de l&apos;émulateur mupen64plus se synchronisera toujours avec l&apos;heure système.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1588"/>
        <source>N64 Real Time Clock</source>
        <translation>Heure en temps réel de la N64</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1595"/>
        <source>Sync to system clock</source>
        <translation>Synchroniser avec l&apos;heure système</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1608"/>
        <source>Use this time:</source>
        <translation>Utiliser l&apos;heure suivante :</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1648"/>
        <source>Rollback clock when loading a savestate</source>
        <translation>Restauration de l&apos;heure au chargement d&apos;une sauvegarde rapide</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="80"/>
        <source>Automatic (%1)</source>
        <translation>Automatique (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="328"/>
        <source>Install Discord Plugin?</source>
        <translation>Installer le plugin Discord?</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="328"/>
        <source>You have enabled Discord integration, but the Discord plugin is not currently installed. Would you like to install it now?</source>
        <translation>Vous avez activer l&apos;intégration Discord, mais le plugin Discord n&apos;est pas installé actuellement. Voulez-vous l&apos;installer maintenant?</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="338"/>
        <source>Download Failed</source>
        <translation>Échec du téléchargement</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="338"/>
        <source>Failed to download Discord plugin</source>
        <translation>Échec du téléchargement du plugin Discord</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="371"/>
        <source>Auto</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="375"/>
        <location filename="../src/ui/settings-dialog.cpp" line="382"/>
        <source>Select a Folder</source>
        <translation>Sélectionner un fichier</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="391"/>
        <source>Confirm Reset</source>
        <translation>Confirmer la réinitialisation</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="392"/>
        <source>This will reset your RetroArch config file, undoing any changes you have made within RetroArch. Your Parallel Launcher settings will not be affected. Do you want to continue?</source>
        <translation>Ceci va réinitialiser votre configuration du fichier de RetroArch, enlèvera tous les changements vous avez fait depuis RetroArch. Vos réglages de Parallel Launcher ne seront pas affectés. Voulez-vous continuer?</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="395"/>
        <source>Config Reset</source>
        <translation>Réinitialiser la configuration</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="395"/>
        <source>Your RetroArch config has been reset.</source>
        <translation>Votre configuration de RetroArch a été réinitialiser.</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="397"/>
        <source>Oops</source>
        <translation>Oups</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="397"/>
        <source>An unknown error occurred. Your RetroArch config has not been reset.</source>
        <translation>Une erreur inconnue s&apos;est produite. Votre configuration RetroArch ne s&apos;est pas réinitialiser.</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="416"/>
        <location filename="../src/ui/settings-dialog.cpp" line="422"/>
        <location filename="../src/ui/settings-dialog.cpp" line="428"/>
        <location filename="../src/ui/settings-dialog.cpp" line="434"/>
        <source>Confirm Apply All</source>
        <translation>Confirmer l&apos;application à tout</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="416"/>
        <location filename="../src/ui/settings-dialog.cpp" line="422"/>
        <location filename="../src/ui/settings-dialog.cpp" line="428"/>
        <location filename="../src/ui/settings-dialog.cpp" line="434"/>
        <source>Apply this setting to all current roms?</source>
        <translation>Appliquer ce réglage à toutes les roms actuelles?</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="446"/>
        <source>Unlimited</source>
        <translation>Illimité</translation>
    </message>
</context>
<context>
    <name>SingleplayerControllerSelectDialog</name>
    <message>
        <location filename="../src/ui/designer/singleplayer-controller-select-dialog.ui" line="20"/>
        <source>Controller Select</source>
        <translation>Sélectionner la manette</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/singleplayer-controller-select-dialog.ui" line="32"/>
        <source>Multiple controllers are connected. Please press any button on the controller you wish to use, or press Esc on the keyboard to use keyboard controls.</source>
        <translation>Plusieurs manettes sont connectées. S&apos;il vous plaît appuyez sur n&apos;importe quel bouton de la manette que vous désirez utiliser, ou appuyez sur Échap sur votre clavier pour utiliser le clavier comme manette.</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="14"/>
        <source>Update Available</source>
        <translation>Mise à jour disponible</translation>
    </message>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="26"/>
        <source>A new update is available for Parallel Launcher. Would you like to update now?</source>
        <translation>Une mise à jour de Parallel Launcher est disponible. Voulez-vous l&apos;installer maintenant?</translation>
    </message>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="62"/>
        <source>Changelog:</source>
        <translation>Changelog:</translation>
    </message>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="79"/>
        <source>Don&apos;t remind me again</source>
        <translation>Ne plus m&apos;avertir</translation>
    </message>
</context>
<context>
    <name>WindowsRetroArchUpdater</name>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="111"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="33"/>
        <source>Checking for RetroArch Updates</source>
        <translation>Recherche de mises à jour de RetroArch</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="210"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="120"/>
        <source>Download Failed</source>
        <translation>Échec du téléchargement</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="211"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="121"/>
        <source>Failed to download RetroArch.</source>
        <translation>Échec de l&apos;installation de RetroArch.</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="218"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="128"/>
        <source>Installing RetroArch</source>
        <translation>Installation de RetroArch</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="238"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="144"/>
        <source>Installation Error</source>
        <translation>Échec de l&apos;installation</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="239"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="145"/>
        <source>An error occurred attempting to uncompress the portable RetroArch bundle</source>
        <translation>Une erreur s&apos;est produite pendant l&apos;essai de la décompression du paquet RetroArch portable</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="280"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="174"/>
        <source>Install Update?</source>
        <translation>Installer la mise à jour?</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="281"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="175"/>
        <source>An update for RetroArch is available. Would you like to install it now?</source>
        <translation>Une mise à jour de RetroArch est disponible. Voulez-vous l&apos;installer maintenant?</translation>
    </message>
</context>
<context>
    <name>WindowsUpdater</name>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="45"/>
        <source>Unexpected Error</source>
        <translation>Une erreur inattendue s&apos;est produite</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="46"/>
        <source>Failed to launch installer.</source>
        <translation>Échec du lancement de l&apos;installation.</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="57"/>
        <source>Download Failed</source>
        <translation>Échec du téléchargement</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="58"/>
        <source>Failed to download the latest installer. Try again later.</source>
        <translation>Échec du téléchargement de l&apos;installateur le plus récent. Veuillez essayer ultérieurement.</translation>
    </message>
</context>
</TS>
