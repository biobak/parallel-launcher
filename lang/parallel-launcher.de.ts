<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de" sourcelanguage="en_CA">
<context>
    <name>AbstractCoreFinderDialog</name>
    <message>
        <location filename="../src/ui/core-finder-dialog.cpp" line="10"/>
        <source>Searching for latest core version...</source>
        <translation>Suche nach neuster Core-Version...</translation>
    </message>
</context>
<context>
    <name>BindInputDialog</name>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="14"/>
        <source>Bind Input</source>
        <translation>Tastenbelegung zuweisen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="38"/>
        <source>To map a button or axis to this input, either press a button on your controller or hold a control stick or trigger in a different position for a half second.</source>
        <translation>Um einen Knopf oder eine Achse zu diesem Input zuzuweisen, drücke entweder einen Knopf auf dem Controller oder bewege den Control-Stick/Trigger in eine andere Position für eine halbe Sekunde.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="76"/>
        <source>Skip</source>
        <translation>Überspringen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/bind-input-dialog.ui" line="87"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>ControllerConfigDialog</name>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="14"/>
        <source>Configure Controller</source>
        <translation>Controller konfigurieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="25"/>
        <source>Configuring Controller:</source>
        <translation>Momentaner Controller:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="62"/>
        <source>Quick Configure</source>
        <translation>Schnellkonfiguration</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="86"/>
        <source>Enable Rumble</source>
        <translation>Vibration aktivieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="99"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="19"/>
        <source>Analog Up</source>
        <translation>Analog Hoch</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="122"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="20"/>
        <source>Analog Down</source>
        <translation>Analog Runter</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="145"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="21"/>
        <source>Analog Left</source>
        <translation>Analog Links</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="168"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="22"/>
        <source>Analog Right</source>
        <translation>Analog Rechts</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="191"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="31"/>
        <source>A Button</source>
        <translation>A-Knopf</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="214"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="32"/>
        <source>B Button</source>
        <translation>B-Knopf</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="248"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="23"/>
        <source>C Up</source>
        <translation>C-Stick Hoch</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="271"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="24"/>
        <source>C Down</source>
        <translation>C-Stick Runter</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="294"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="25"/>
        <source>C Left</source>
        <translation>C-Stick Links</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="317"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="26"/>
        <source>C Right</source>
        <translation>C-Stick Rechts</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="340"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="34"/>
        <source>Z Trigger</source>
        <translation>Z-Trigger</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="363"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="35"/>
        <source>R Trigger</source>
        <translation>R-Trigger</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="397"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="27"/>
        <source>D-Pad Up</source>
        <translation>Steuertaste Hoch</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="420"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="28"/>
        <source>D-Pad Down</source>
        <translation>Steuertaste Runter</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="443"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="29"/>
        <source>D-Pad Left</source>
        <translation>Steuertaste Links</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="466"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="30"/>
        <source>D-Pad Right</source>
        <translation>Steuertaste Rechts</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="489"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="36"/>
        <source>Start Button</source>
        <translation>Start-Knopf</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="512"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="33"/>
        <source>L Trigger</source>
        <translation>L-Trigger</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="546"/>
        <source>The following inputs are only used by games that support Gamecube Controllers:</source>
        <translation>Die Folgenden Tasten werden nur von Spielen benutzt, die Gamecube-Controller unterstützen:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="566"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="37"/>
        <source>X Button</source>
        <translation>X-Knopf</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="610"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="38"/>
        <source>Y Button</source>
        <translation>Y-Knopf</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="660"/>
        <source>Sensitivity</source>
        <translation>Sensitivität</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="686"/>
        <source>Deadzone</source>
        <translation>Tote Zone</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="745"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="39"/>
        <source>Save State</source>
        <translation>Spielstand speichern</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="774"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="40"/>
        <source>Load State</source>
        <translation>Spielstand laden</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="822"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="42"/>
        <source>Toggle Slow Motion</source>
        <translation>Zeitlupe ein-/ausschalten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="832"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="41"/>
        <source>Toggle Fast Forward</source>
        <translation>Vorspulen ein-/ausschalten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="864"/>
        <source>How far you need to press down a trigger or tilt a control stick that is bound to an N64 button before it is considered a button press</source>
        <translation>Für Trigger/Control-Sticks, die Knöpfen zugewiesen sind: Wie weit der Trigger/Stick bewegt werden muss, damit der Knopf als &quot;gedrückt&quot; zählt</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="867"/>
        <source>Analog to Digital Button Press Threshold</source>
        <translation>Analog-zu-Digital Druckschwelle</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="949"/>
        <source>Save As</source>
        <translation>Speichern als</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="969"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="983"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="573"/>
        <location filename="../src/ui/designer/controller-config-dialog.ui" line="597"/>
        <location filename="../src/ui/controller-config-dialog.cpp" line="75"/>
        <source>Not Bound</source>
        <translation>Nicht zugewiesen</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="78"/>
        <source>Button %1</source>
        <translation>Knopf %1</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="81"/>
        <source>Axis -%1</source>
        <translation>Achse -%1</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="84"/>
        <source>Axis +%1</source>
        <translation>Achse +%1</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="87"/>
        <source>Hat %1 Up</source>
        <translation>Kappe %1 Hoch</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="90"/>
        <source>Hat %1 Down</source>
        <translation>Kappe %1 Runter</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="93"/>
        <source>Hat %1 Left</source>
        <translation>Kappe %1 Links</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="96"/>
        <source>Hat %1 Right</source>
        <translation>Kappe %1 Rechts</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="141"/>
        <source>Enter Profile Name</source>
        <translation>Profilnamen eingeben</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="141"/>
        <source>Enter a name for your new controller profile.</source>
        <translation>Bitte einen namen für das neue Controllerprofil eingeben.</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="147"/>
        <source>Failed to Save Profile</source>
        <translation>Fehler beim Speichern des Profils</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-config-dialog.cpp" line="147"/>
        <source>A default controller profile with that name already exists.</source>
        <translation>Es existiert schon ein vorgegebenes Controllerprofil mit diesem Namen.</translation>
    </message>
</context>
<context>
    <name>ControllerSelectDialog</name>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="14"/>
        <source>Select Controller</source>
        <translation>Controller auswählen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="28"/>
        <source>Input Driver</source>
        <translation>Input-Treiber</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="74"/>
        <source>If your controller does not appear in this list, or if you are having problems setting up your controller bindings, try switching to a different input driver.</source>
        <translation>Wenn dein Controller nicht in der Liste auftaucht oder wenn du Probleme mit Tastenbelegungen hast, versuche einen anderen Input-Treiber zu benutzen.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="106"/>
        <source>Connected Controllers</source>
        <translation>Verbundene Controller</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="135"/>
        <source>Controller Profiles</source>
        <translation>Controllerprofile</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="150"/>
        <location filename="../src/ui/controller-select-dialog.cpp" line="141"/>
        <location filename="../src/ui/controller-select-dialog.cpp" line="183"/>
        <source>Edit Profile</source>
        <translation>Profil bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/controller-select-dialog.ui" line="161"/>
        <source>Delete Profile</source>
        <translation>Profil löschen</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="137"/>
        <location filename="../src/ui/controller-select-dialog.cpp" line="179"/>
        <source>New Profile</source>
        <translation>Neues Profil</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="232"/>
        <source>Confirm Delete</source>
        <translation>Löschen bestätigen</translation>
    </message>
    <message>
        <location filename="../src/ui/controller-select-dialog.cpp" line="232"/>
        <source>Are you sure you want to delete controller profile &apos;%1&apos;?</source>
        <translation>Bist du dir sicher, dass du das Controllerprofil &apos;%1&apos; löschen möchtest?</translation>
    </message>
</context>
<context>
    <name>CoreInstaller</name>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="26"/>
        <location filename="../src/ui/core-installer.cpp" line="77"/>
        <source>Download Failed</source>
        <translation>Download fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="27"/>
        <location filename="../src/ui/core-installer.cpp" line="78"/>
        <source>Failed to download emulator core</source>
        <translation>Beim Herunterladen des Emulator-Cores ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="35"/>
        <location filename="../src/ui/core-installer.cpp" line="110"/>
        <source>Installation Failed</source>
        <translation>Installation felhgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="36"/>
        <location filename="../src/ui/core-installer.cpp" line="111"/>
        <source>Failed to install emulator core</source>
        <translation>Beim Installieren des Emulator-Cores ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="46"/>
        <location filename="../src/ui/core-installer.cpp" line="122"/>
        <source>Installation Successful</source>
        <translation>Installation erfolgreich</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="47"/>
        <location filename="../src/ui/core-installer.cpp" line="123"/>
        <source>Core installed successfully.</source>
        <translation>Core erfolgreich installiert.</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="150"/>
        <source>Core Unavailable</source>
        <translation>Core nicht verfügbar</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="151"/>
        <source>Sorry! The Mupen64plus-Next emulator core is not available on MacOS. Try using ParallelN64 instead.</source>
        <translation>Sorry! Der Mupen64plus-Next Emulator-Core ist auf MacOS nicht verfügbar. Versuche stattdessen ParallelN64 zu verwenden.</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="159"/>
        <source>Install Emulator Core?</source>
        <translation>Emulator-Core installieren?</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="160"/>
        <source>This emulator core is not installed or is out of date. Would you like to install it now?</source>
        <translation>Dieser Emulator-Core ist nicht installiert oder veraltet. Möchtest du ihn jetzt installieren?</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="214"/>
        <location filename="../src/ui/core-installer.cpp" line="245"/>
        <source>Core Update Available</source>
        <translation>Core-Aktualisierung verfügbar</translation>
    </message>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="215"/>
        <location filename="../src/ui/core-installer.cpp" line="246"/>
        <source>An update is available for %1. Would you like to install it now?</source>
        <translation>Ein Update für den Core %1 ist verfügbar. Möchtest du es jetzt installieren?</translation>
    </message>
</context>
<context>
    <name>DeviceBusyDialog</name>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="17"/>
        <source>Device Busy</source>
        <translation>Gerät wird benutzt</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="26"/>
        <source>One or more SD cards are busy. Waiting for all file operations to complete...</source>
        <translation>Eine SD-Karte wird noch benutzt. Bitte warten, während Schreiboperationen abgeschlossen werden...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="58"/>
        <source>Force Unmount</source>
        <translation>Auswerfen erzwingen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/device-busy-dialog.ui" line="69"/>
        <source>Cancel</source>
        <translation>Auswerfen abbrechen</translation>
    </message>
</context>
<context>
    <name>DirectPlay</name>
    <message>
        <location filename="../src/ui/play.cpp" line="638"/>
        <location filename="../src/ui/play.cpp" line="645"/>
        <location filename="../src/ui/play.cpp" line="652"/>
        <location filename="../src/ui/play.cpp" line="659"/>
        <source>Patch Failed</source>
        <translation>Patch Fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="639"/>
        <source>Failed to patch ROM. The .bps patch appears to be invalid.</source>
        <translation>Fehler beim Patchen der ROM: Der .bps-Patch scheint ungültig zu sein.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="646"/>
        <source>Failed to patch ROM. The base ROM does not match what the patch expects..</source>
        <translation>Fehler beim Patchen der ROM: Die Basis-ROM hat nicht die vom Patch erwartete Prüfsumme.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="653"/>
        <source>Failed to patch ROM. The base rom required to patch is not known to Parallel Launcher.</source>
        <translation>Fehler beim Patchen der ROM: Die Basis-ROM ist Parallel-Launcher nicht bekannt.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="660"/>
        <source>Failed to patch ROM. An error occurred while writing the patched ROM to disk.</source>
        <translation>Fehler beim Patchen der ROM: Ein Fehler beim Speichern der ROM auf die Festplatte ist aufgetreten.</translation>
    </message>
</context>
<context>
    <name>DirectPlayWindow</name>
    <message>
        <location filename="../src/ui/designer/direct-play-window.ui" line="14"/>
        <source>ROM Settings</source>
        <translation>ROM-Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/direct-play-window.ui" line="27"/>
        <source>This is your first time running this ROM. Please select any ROM specific settings. You can always change these settings later.</source>
        <translation>Dies ist das erste mal, dass du diese ROM startest. Bitte ändere ROM-spezifische Einstellungen hier. Du kannst diese Einstellungen später jederzeit ändern.</translation>
    </message>
</context>
<context>
    <name>DownloadDialog</name>
    <message>
        <location filename="../src/ui/core-installer.cpp" line="22"/>
        <location filename="../src/ui/core-installer.cpp" line="73"/>
        <source>Downloading core...</source>
        <translation>Core wird heruntergeladen...</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="202"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="112"/>
        <source>Downloading RetroArch</source>
        <translation>RetroArch wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="53"/>
        <source>Downloading installer...</source>
        <translation>Installationsprogramm wird heruntergeladen...</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="332"/>
        <source>Downloading Discord plugin</source>
        <translation>Discord-Plugin wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../src/core/sdcard.mount.windows.cpp" line="409"/>
        <source>Downloading SD Card Manager Extension</source>
        <translation>Erweiterung zum Verwalten von SD-Karten wird heruntergeladen</translation>
    </message>
</context>
<context>
    <name>EditableStarDisplayWidget</name>
    <message>
        <location filename="../src/rhdc/ui/star-display-widget.cpp" line="371"/>
        <source>Save slot is empty.</source>
        <translation>Speicherstand ist leer.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/star-display-widget.cpp" line="375"/>
        <source>Create Save Slot</source>
        <translation>Speicherstand erstellen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/star-display-widget.cpp" line="376"/>
        <source>Delete Save Slot</source>
        <translation>Speicherstand löschen</translation>
    </message>
</context>
<context>
    <name>ErrorNotifier</name>
    <message>
        <location filename="../src/ui/error-notifier.cpp" line="5"/>
        <source>The application encountered an unexpected error:</source>
        <translation>Ein unerwarteter Fehler ist aufgetreten:</translation>
    </message>
    <message>
        <location filename="../src/ui/error-notifier.cpp" line="12"/>
        <source>Unhandled Error</source>
        <translation>Unbehandelter Fehler</translation>
    </message>
</context>
<context>
    <name>Game</name>
    <message>
        <location filename="../src/ui/play.cpp" line="40"/>
        <source>The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then this ROM may contain invalid or unsafe RSP microcode that cannot be run on console accurate graphics plugins. Alternatively, if you have a very old onboard graphics card, it is possible that Vulkan is not supported on your system. In either case, using another graphics plugin might resolve the issue.</source>
        <translation>Der Emulator wurde kurz nach dem Start beendet. Wenn du andere ROMs speieln kannst, dann enthält diese ROM möglicherweise ungültigen oder unsicheren RSP-Mikrocode, welcher nicht auf konsolengetreuen Grafikplugins ausgeführt werden kann. Alternativ, wenn du eine sehr alte integrierte Grafikkarte besitzt, ist es möglich, dass Vulkan auf deinem System nicht unterstützt wird. In beiden Fällen könnte ein anderes Grafikplugin den Fehler beheben.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="46"/>
        <source>The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then this ROM may contain invalid or unsafe RSP microcode that cannot be run on console accurate graphics plugins. If this is the case, try running the ROM with another graphics plugin instead.</source>
        <translation>Der Emulator wurde kurz nach dem Start beendet. Wenn du andere ROMs speieln kannst, dann enthält diese ROM möglicherweise ungültigen oder unsicheren RSP-Mikrocode, welcher nicht auf konsolengetreuen Grafikplugins ausgeführt werden kann. Wenn dies der Fall ist, versuche, die ROM mit einem anderen Grafikplugin zu starten.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="51"/>
        <source>The emulator exited shortly after launching. If you are able to launch other ROMs without issues, then this ROM is most likely corrupt.</source>
        <translation>Der Emulator wurde kurz nach dem Start beendet. Wenn du andere ROMs speieln kannst, dann ist diese ROM wahrscheinlich ungültig/beschädigt.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="262"/>
        <source>Emulator Missing</source>
        <translation>Emulator nicht gefunden</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="263"/>
        <source>Failed to launch emulator. It does not appear to be installed.</source>
        <translation>Fehler beim Starten des Emulators. Er scheint nicht installiert zu sein.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="451"/>
        <source>Possible ROM Error</source>
        <translation>Möglicher ROM-Fehler</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="500"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="506"/>
        <source>Do not warn me again</source>
        <translation>Warnungen deaktivieren</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="571"/>
        <source>This game uses an emulated Gamecube controller, but you have not bound any inputs to the X and Y buttons.</source>
        <translation>Dieses Spiel benutzt einen emuliteren Gamecube-Controller, aber du hast noch keine X- und Y-Tasten zugewiesen.</translation>
    </message>
    <message>
        <location filename="../src/ui/play.cpp" line="581"/>
        <source>This game uses an emulated Gamecube controller, but you do not have an analog stick bound to the C buttons (C stick).</source>
        <translation>Dieses Spiel benutzt einen emuliteren Gamecube-Controller, aber du hast noch keinen Analog-Stick zu den C-Knöpfen (C-Stick) zugewiesen.</translation>
    </message>
</context>
<context>
    <name>GetControllerPortDialog</name>
    <message>
        <location filename="../src/ui/get-controller-port-dialog.cpp" line="8"/>
        <source>Choose Controller</source>
        <translation>Controller auswählen</translation>
    </message>
    <message>
        <location filename="../src/ui/get-controller-port-dialog.cpp" line="9"/>
        <source>Press any button on the controller to bind to this port.</source>
        <translation>Drücke einen beliebigen Knopf auf dem Controller, um diesen Port hinzuzufügen.</translation>
    </message>
</context>
<context>
    <name>GroupName</name>
    <message>
        <location filename="../src/core/special-groups.cpp" line="14"/>
        <source>Favourites</source>
        <translation>Favoriten</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="16"/>
        <source>Uncategorized</source>
        <translation>Unkategorisiert</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="18"/>
        <source>Want To Play</source>
        <translation>Will ich spielen</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="20"/>
        <source>In Progress</source>
        <translation>Noch nicht fertig</translation>
    </message>
    <message>
        <location filename="../src/core/special-groups.cpp" line="22"/>
        <source>Completed</source>
        <translation>Fertig</translation>
    </message>
</context>
<context>
    <name>HotkeyEditWidget</name>
    <message>
        <location filename="../src/ui/hotkey-edit-widget.cpp" line="8"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
</context>
<context>
    <name>ImportSaveDialog</name>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="10"/>
        <source>Select Save File</source>
        <translation>Speicherstand auswählen</translation>
    </message>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="11"/>
        <source>Project64 Save Files</source>
        <translation>Project64-Speicherstände</translation>
    </message>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="21"/>
        <source>Save data imported</source>
        <translation>Speicherstand importiert</translation>
    </message>
    <message>
        <location filename="../src/ui/import-save-dialog.cpp" line="24"/>
        <source>Failed to import save data</source>
        <translation>Fehler beim Importieren des Speicherstandes</translation>
    </message>
</context>
<context>
    <name>ImportSdCardDialog</name>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="17"/>
        <source>Import SD Card</source>
        <translation>SD-Karte importieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="31"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="69"/>
        <source>Use this file directly</source>
        <translation>Datei direkt importieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/import-sd-card-dialog.ui" line="79"/>
        <source>Make a copy of this file</source>
        <translation>Datei kopieren und Kopie importieren</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="69"/>
        <source>Import</source>
        <translation>Importieren</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="132"/>
        <source>This name is already in use</source>
        <translation>Dieser Name wird bereits verwendet</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="149"/>
        <source>This name is reserved by Windows</source>
        <translation>Dieser Name ist von Windows reserviert</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="165"/>
        <source>Unexpected Error</source>
        <translation>Unerwarteter Fehler</translation>
    </message>
    <message>
        <location filename="../src/ui/import-sd-card-dialog.cpp" line="165"/>
        <source>Failed to import SD card.</source>
        <translation>Fehler beim Importieren der SD-Karte.</translation>
    </message>
</context>
<context>
    <name>InputModeSelect</name>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="506"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="507"/>
        <source>Maps your gamepad inputs to a single N64 controller using your controller profile</source>
        <translation>Übersetzt die Controllereingaben zu einem N64-Controller mit deinem Controllerprofil</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="530"/>
        <source>Dual Analog</source>
        <translation>Dual-Analog</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="531"/>
        <source>Your gamepad inputs that normally bind to the C buttons instead bind to the analog stick on a second N64 controller</source>
        <translation>Die Controllereingaben die normalerweise den C-Knöpfen zugewiesen sind gehen jetzt zum Analog-Stick eines 2. N64-Controllers</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="554"/>
        <source>GoldenEye</source>
        <translation>GoldenEye</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="555"/>
        <source>Maps your gamepad inputs to two N64 controllers suitable for playing GoldenEye with the 2.4 Goodhead control style</source>
        <translation>Die Controllereingaben werden auf 2 N64-Controller so aufgeteilt, dass GoldenEye mit dem &quot;2.4 Goodhead&quot;-Controlstil gespielt werden kann</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="578"/>
        <source>Clone</source>
        <translation>Klonen</translation>
    </message>
    <message>
        <location filename="../src/core/preset-controllers.cpp" line="579"/>
        <source>Your gamepad inputs are sent to two controller ports instead of just one</source>
        <translation>Die Controllereingaben werden zu 2 Controllerports anstelle von einem gesendet</translation>
    </message>
</context>
<context>
    <name>IsViewerWindow</name>
    <message>
        <location filename="../src/ui/is-viewer-window.cpp" line="58"/>
        <source>Error: IS Viewer was disconnected from the emulator.</source>
        <translation>Fehler: IS-Viewer wurde vom Emulator getrennt.</translation>
    </message>
    <message>
        <location filename="../src/ui/is-viewer-window.cpp" line="59"/>
        <source>Error: Failed to connect IS Viewer to the emulator.</source>
        <translation>Fehler: IS-Viewer konnte nicht mit dem Emulator verbunden werden.</translation>
    </message>
</context>
<context>
    <name>KeyboardConfigDialog</name>
    <message>
        <location filename="../src/ui/designer/keyboard-config-dialog.ui" line="14"/>
        <source>Configure Keyboard Controls</source>
        <translation>Tastaturbelegungen ändern</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/keyboard-config-dialog.ui" line="28"/>
        <source>Hotkeys</source>
        <translation>Hotkeys</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/keyboard-config-dialog.ui" line="71"/>
        <source>Keyboard Controls</source>
        <translation>Tastatursteuerung</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="18"/>
        <source>Analog Up</source>
        <translation>Analog Hoch</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="19"/>
        <source>Analog Down</source>
        <translation>Analog Runter</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="20"/>
        <source>Analog Left</source>
        <translation>Analog Links</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="21"/>
        <source>Analog Right</source>
        <translation>Analog Rechts</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="22"/>
        <source>C Up</source>
        <translation>C-Stick Hoch</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="23"/>
        <source>C Down</source>
        <translation>C-Stick Runter</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="24"/>
        <source>C Left</source>
        <translation>C-Stick Links</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="25"/>
        <source>C Right</source>
        <translation>C-Stick Rechts</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="26"/>
        <source>D-Pad Up</source>
        <translation>Steuertaste Hoch</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="27"/>
        <source>D-Pad Down</source>
        <translation>Steuertaste Runter</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="28"/>
        <source>D-Pad Left</source>
        <translation>Steuertaste Links</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="29"/>
        <source>D-Pad Right</source>
        <translation>Steuertaste Rechts</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="30"/>
        <source>A Button</source>
        <translation>A-Knopf</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="31"/>
        <source>B Button</source>
        <translation>B-Knopf</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="32"/>
        <source>L Trigger</source>
        <translation>L-Trigger</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="33"/>
        <source>Z Trigger</source>
        <translation>Z-Trigger</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="34"/>
        <source>R Trigger</source>
        <translation>R-Trigger</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="35"/>
        <source>Start Button</source>
        <translation>Start-Knopf</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="36"/>
        <source>X Button</source>
        <translation>X-Knopf</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="37"/>
        <source>Y Button</source>
        <translation>Y-Knopf</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="39"/>
        <source>Save State</source>
        <translation>Spielstand speichern</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="40"/>
        <source>Load State</source>
        <translation>Spielstand laden</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="41"/>
        <source>Previous State</source>
        <translation>Letzter Speicherstand</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="42"/>
        <source>Next State</source>
        <translation>Nächster Speicherstand</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="43"/>
        <source>Previous Cheat</source>
        <translation>Letzter Cheat</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="44"/>
        <source>Next Cheat</source>
        <translation>Nächster Cheat</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="45"/>
        <source>Toggle Cheat</source>
        <translation>Cheat an-/ausschalten</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="46"/>
        <source>Toggle FPS Display</source>
        <translation>FPS-Anzeige ein-/ausschalten</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="47"/>
        <source>Pause/Unpause</source>
        <translation>Pausieren/Weiter</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="48"/>
        <source>Frame Advance</source>
        <translation>Frame Advance</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="49"/>
        <source>Fast Forward (Hold)</source>
        <translation>Vorspulen (halten)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="50"/>
        <source>Fast Forward (Toggle)</source>
        <translation>Vorspulen (ein/aus)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="51"/>
        <source>Slow Motion (Hold)</source>
        <translation>Zeitlupe (halten)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="52"/>
        <source>Slow Motion (Toggle)</source>
        <translation>Zeitlupe (ein/aus)</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="53"/>
        <source>Rewind*</source>
        <translation>Zurückspulen*</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="54"/>
        <source>Quit Emulator</source>
        <translation>Emulator stoppen</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="55"/>
        <source>Hard Reset</source>
        <translation>&quot;Hard Reset&quot;</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="56"/>
        <source>Toggle Fullscreen</source>
        <translation>Vollbild ein-/ausschalten</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="57"/>
        <source>RetroArch Menu</source>
        <translation>RetroArch-Menü</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="58"/>
        <source>Record Video</source>
        <translation>Video aufnehmen</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="59"/>
        <source>Record Inputs</source>
        <translation>Eingaben aufnehmen</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="60"/>
        <source>Take Screenshot</source>
        <translation>Bildschirmfoto machen</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="61"/>
        <source>Raise Volume</source>
        <translation>Lautstärke erhöhen</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="62"/>
        <source>Lower Volume</source>
        <translation>Lautstärke verringern</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="63"/>
        <source>Mute/Unmute</source>
        <translation>Stummschalten</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="64"/>
        <source>Grab Mouse</source>
        <translation>Maus einfangen</translation>
    </message>
    <message>
        <location filename="../src/ui/keyboard-config-dialog.cpp" line="98"/>
        <source>You must enable rewind functionality in the RetroArch menu to use this hotkey.</source>
        <translation>Um den Zurückspulen-Hotkey zu verwenden, muss die Funktionalität in RetroArch eingeschaltet werden.</translation>
    </message>
</context>
<context>
    <name>LogViewerDialog</name>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="14"/>
        <source>Log Viewer</source>
        <translation>Log-Betrachter</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="28"/>
        <source>Only show logs from this session</source>
        <translation>Nur Logs von dieser Sitzung anzeigen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="48"/>
        <source>Show timestamps</source>
        <translation>Zeitstempel</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="60"/>
        <source>Visible Logs</source>
        <translation>Sichtbare Logs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="66"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="76"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="86"/>
        <source>Warn</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="96"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/log-viewer-dialog.ui" line="106"/>
        <source>Fatal</source>
        <translation>Fatal</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../src/main.cpp" line="225"/>
        <source>Installing RetroArch</source>
        <translation>RetroArch wird installert</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="226"/>
        <source>Parallel Launcher will now install RetroArch.</source>
        <translation>Parallel Launcher wird jetzt RetroArch installieren.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="235"/>
        <source>Fatal Error</source>
        <translation>Fataler Fehler</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="236"/>
        <source>Failed to install RetroArch.</source>
        <translation>Fehler beim Installieren von RetroArch.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="64"/>
        <source>Refresh ROM List</source>
        <translation>ROM-Liste aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="81"/>
        <source>Configure Controller</source>
        <translation>Controller konfigurieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="98"/>
        <source>Options</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="128"/>
        <source>Searching for ROMs...</source>
        <translation>Suche nach ROMs...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="185"/>
        <source>No ROMS have been found</source>
        <translation>Es wurden keine ROMs gefunden</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="192"/>
        <location filename="../src/ui/designer/main-window.ui" line="333"/>
        <source>Manage ROM Sources</source>
        <translation>ROM-Quellen verwalten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="236"/>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="283"/>
        <source>Download</source>
        <translation>Herunterladen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="300"/>
        <source>Play Multiplayer</source>
        <translation>Mehrspieler spielen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="311"/>
        <source>Play Singleplayer</source>
        <translation>Einzelspieler spielen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="328"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="338"/>
        <source>Configure Controllers</source>
        <translation>Controller konfigurieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="343"/>
        <source>Keyboard Controls and Hotkeys</source>
        <translation>Tastaturbelegungen und Hotkeys</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="346"/>
        <source>Configure keyboard controls and hotkeys</source>
        <translation>Tastaturbelegungen und Hotkeys einstellen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="351"/>
        <location filename="../src/ui/main-window.cpp" line="785"/>
        <source>Login to romhacking.com</source>
        <translation>In romhacking.com einloggen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="356"/>
        <source>Logout of romhacking.com</source>
        <translation>Von romhacking.com abmelden</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="361"/>
        <source>Disable romhacking.com integration</source>
        <translation>Romhacking.com-Integration deaktivieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="366"/>
        <source>Add Single ROM</source>
        <translation>Einzelne ROM hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="375"/>
        <source>Quit Parallel Launcher</source>
        <translation>Parallel Launcher beenden</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="380"/>
        <source>Open save file directory</source>
        <translation>Speicherstand-Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="385"/>
        <source>Open savestate directory</source>
        <translation>SaveState-Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="390"/>
        <source>Open SD card directory</source>
        <translation>SD-Karten-Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="395"/>
        <source>Open app data directory</source>
        <translation>Daten-Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="400"/>
        <source>Open app config directory</source>
        <translation>Konfigurationsordner öffnen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/main-window.ui" line="405"/>
        <source>Open app cache directory</source>
        <translation>Cache-Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="143"/>
        <source>Data Directories</source>
        <translation>PL-Ordner</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="630"/>
        <source>Select a ROM</source>
        <translation>Bitte eine ROM auswählen</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="631"/>
        <source>ROM or Patch File</source>
        <translation>ROM oder Patch-Datei</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="656"/>
        <location filename="../src/ui/main-window.cpp" line="659"/>
        <location filename="../src/ui/main-window.cpp" line="662"/>
        <location filename="../src/ui/main-window.cpp" line="665"/>
        <source>Patch Failed</source>
        <translation>Patch Fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="656"/>
        <source>Failed to patch ROM. The .bps patch appears to be invalid.</source>
        <translation>Fehler beim Patchen der ROM: Der .bps-Patch scheint ungültig zu sein.</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="659"/>
        <source>Failed to patch ROM. The base ROM does not match what the patch expects.</source>
        <translation>Fehler beim Patchen der ROM: Die Basis-ROM hat nicht die vom Patch erwartete Prüfsumme.</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="662"/>
        <source>Failed to patch ROM. The base rom required to patch is not known to Parallel Launcher.</source>
        <translation>Fehler beim Patchen der ROM: Die Basis-ROM ist Parallel-Launcher nicht bekannt.</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="665"/>
        <source>Failed to patch ROM. An error occurred while writing the patched ROM to disk.</source>
        <translation>Fehler beim Patchen der ROM: Ein Fehler beim Speichern der ROM auf die Festplatte ist aufgetreten.</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="785"/>
        <source>Enable romhacking.com integration</source>
        <translation>Romhacking.com-Integration aktivieren</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="824"/>
        <source>Confirm Disable</source>
        <translation>Deaktivierung bestätigen</translation>
    </message>
    <message>
        <location filename="../src/ui/main-window.cpp" line="824"/>
        <source>Are you sure you want to disable romhacking.com integration?</source>
        <translation>Möchtest du wirklich romhacking.com-Integration deaktivieren?</translation>
    </message>
</context>
<context>
    <name>ManageGroupsDialog</name>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="14"/>
        <source>Manage Groups</source>
        <translation>Gruppen verwalten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="32"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="43"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-groups-dialog.ui" line="54"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="55"/>
        <source>Confirm Delete</source>
        <translation>Löschen bestätigen</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="55"/>
        <source>Are you sure you want to delete this group?</source>
        <translation>Möchtest du diese Gruppe wirklich löschen?</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="75"/>
        <source>Rename Group</source>
        <translation>Gruppe umbenennen</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="75"/>
        <source>Enter a new name for your group</source>
        <translation>Bitte einen neuen Namen für diese Gruppe eingeben</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="85"/>
        <source>Rename Failed</source>
        <translation>Umbenennen fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="85"/>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="104"/>
        <source>A group with this name already exists.</source>
        <translation>Eine Gruppe mit diesem Namen existiert bereits.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="99"/>
        <source>Create Group</source>
        <translation>Gruppe erstellen</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="99"/>
        <source>Enter a name for your new group</source>
        <translation>Bitte einen Namen für die neue Gruppe eingeben</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-groups-dialog.cpp" line="104"/>
        <source>Create Failed</source>
        <translation>Erstellen fehlgeschlagen</translation>
    </message>
</context>
<context>
    <name>ManageSdCardsDialog</name>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="14"/>
        <source>Manage Virtual SD Cards</source>
        <translation>Virtuelle SD-Karten verwalten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="42"/>
        <source>Create New</source>
        <translation>Neue SD-Karte erstellen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="53"/>
        <source>Import</source>
        <translation>Importieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="111"/>
        <source>Size</source>
        <translation>Größe</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="118"/>
        <source>Format</source>
        <translatorcomment>Translating this from &quot;file system&quot; as &quot;Format&quot; isn&apos;t really established as meaning &quot;file system format&quot;</translatorcomment>
        <translation>Dateisystem</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="147"/>
        <source>None</source>
        <translation>Keins</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="168"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="211"/>
        <source>Reformat</source>
        <translation>Neu formatieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="222"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="251"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="415"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="437"/>
        <source>Clone</source>
        <translation>Duplizieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="262"/>
        <source>Browse Files</source>
        <translation>Dateien durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="295"/>
        <source>Create</source>
        <translation>Erstellen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/manage-sd-cards-dialog.ui" line="306"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="117"/>
        <source>Confirm Reformat</source>
        <translation>Neuformatieren bestätigen</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="118"/>
        <source>Are you sure you want to reformat this virtual SD card? All data currently on it will be lost.</source>
        <translation>Bist du dir sicher, dass du diese virtuelle SD-Karte neu formatieren willst? Alle momentan darauf gespeicherten Daten werden gelöscht.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="213"/>
        <source>Confirm Deletion</source>
        <translation>Löschen bestätigen</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="214"/>
        <source>Are you sure you want to delete this virtual SD card? This action cannot be undone.</source>
        <translation>Bist du dir sicher, dass du diese virtuelle SD-Karte löschen willst? Das Löschen kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="245"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="279"/>
        <source>Unexpected Error</source>
        <translation>Unerwarteter Fehler</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="245"/>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="279"/>
        <source>Failed to create the virtual SD card because of an unknown error.</source>
        <translation>Die virtuelle SD-Karte konnte nicht erstellt werden, da ein unbekannter Fehler aufgetreten ist.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="297"/>
        <source>Import SD Card</source>
        <translation>SD-Karte importieren</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="298"/>
        <source>Raw Disk Image</source>
        <translation>Datenträgerabbild</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="306"/>
        <source>Invalid disk image</source>
        <translation>Ungültiges Datenträgerabbild</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="306"/>
        <source>The specified file is not a valid disk image.</source>
        <translation>Diese Datei ist kein gültiges Datenträgerabbild.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="309"/>
        <source>Disk image too large</source>
        <translation>Datenträgerabbild zu groß</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="309"/>
        <source>The disk image could not be imported because it is larger than 2 TiB.</source>
        <translation>Dieses Datenträgerabbild konnte nicht importiert werden, da es größer als 2TB ist.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="396"/>
        <source>Operation Failed</source>
        <translation>Vorgang fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="397"/>
        <source>Failed to mount the SD card image. For more detailed error information, go back to the main window and press F7 to view error logs.</source>
        <translation>Fehler beim Einbinden der SD-Karten-Datei. Für eine detailierte Fehlerbeschreibung, gehe zurück ins Hauptfenster und drücke &quot;F7&quot;.</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="411"/>
        <source>Clone SD Card</source>
        <translation>SD-Karte duplizieren</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="412"/>
        <source>Enter a name for the copied SD card:</source>
        <translation>Bitte einen Namen für die duplizierte SD-Karte eingeben:</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="514"/>
        <source>Confirm Close</source>
        <translation>Schließen bestätigen</translation>
    </message>
    <message>
        <location filename="../src/ui/manage-sd-cards-dialog.cpp" line="515"/>
        <source>All SD cards you have opened for browsing will be closed. Continue?</source>
        <translation>Alle momentan geöffneten SD-Karten werden geschlossen. Fortsetzen?</translation>
    </message>
</context>
<context>
    <name>MultiplayerControllerSelectDialog</name>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="14"/>
        <source>Select Controllers</source>
        <translation>Controller auswählen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="22"/>
        <source>Port 1</source>
        <translation>Port 1</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="33"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="59"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="85"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="111"/>
        <source>-- None --</source>
        <translation>-- Keine --</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="41"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="67"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="93"/>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="119"/>
        <source>Detect Device</source>
        <translation>Controller erkennen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="48"/>
        <source>Port 2</source>
        <translation>Port 2</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="74"/>
        <source>Port 3</source>
        <translation>Port 3</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="100"/>
        <source>Port 4</source>
        <translation>Port 4</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/multiplayer-controller-select-dialog.ui" line="128"/>
        <source>Allow port 1 to save and load states</source>
        <translation>Port 1 kann Savestates speichern/laden</translation>
    </message>
</context>
<context>
    <name>NeutralInputDialog</name>
    <message>
        <location filename="../src/ui/designer/neutral-input-dialog.ui" line="14"/>
        <source>Return to Neutral</source>
        <translation>Controller loslassen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/neutral-input-dialog.ui" line="29"/>
        <source>Return all triggers and analog sticks to their neutral position, then press any button on the controller or click the OK button to continue.</source>
        <translation>Bitte alle Trigger und Analog-Sticks zurück in die Ausgangsposition bringen und einen Knopf auf dem Controller oder den Ok-Button drücken, um fortzufahren.</translation>
    </message>
</context>
<context>
    <name>NowPlayingWidget</name>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="34"/>
        <source>Now Playing: </source>
        <translation>Momentane ROM: </translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="106"/>
        <source>Play Time</source>
        <translation>Spielzeit</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="119"/>
        <source>Total</source>
        <translation>Gesamt</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="150"/>
        <source>This Session</source>
        <translation>Diese Sitzung</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="221"/>
        <source>Hard Reset</source>
        <translation>Spiel neustarten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/now-playing-widget.ui" line="232"/>
        <source>Kill Emulator</source>
        <translation>Emulator beenden</translation>
    </message>
    <message>
        <location filename="../src/ui/now-playing-widget.cpp" line="18"/>
        <source>The hack author has not provided a star layout for this hack. This layout will show unattainable stars and may be missing some others.</source>
        <translation>Der Hack-Autor hat kein Sternen-Layout für diesen Hack bereitgestellt. Das Standard-Layout für SM64 wird benutzt. Es könnte nicht erreichbare oder zu viele Sterne enthalten.</translation>
    </message>
    <message>
        <location filename="../src/ui/now-playing-window.cpp" line="177"/>
        <source>Parallel Launcher - Now Playing</source>
        <translation>Parallel Launcher - Momentane ROM</translation>
    </message>
</context>
<context>
    <name>RhdcDownloadDialog</name>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="297"/>
        <source>Downloading star layout</source>
        <translation>Sternen-Layout wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="322"/>
        <source>Failed to download star layout from RHDC</source>
        <translation>Beim Herunterladen des Sternen-Layouts von RHDC ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="338"/>
        <source>Downloading patch</source>
        <translation>Patch wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="158"/>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="239"/>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="387"/>
        <source>Failed to download patch from RHDC</source>
        <translation>Beim Herunterladen des Patches von RHDC ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="361"/>
        <source>Applying patch</source>
        <translation>Patch wird angewendet</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="370"/>
        <source>The rom could not be installed because you do not have an unmodified copy of the US version of Super Mario 64 in your known roms list.</source>
        <translation>Diese ROM konnte nicht installiert werden, da du keine unmodifizierte Kopie von der US-Version von Super Mario 64 in der ROM-Liste hast.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="427"/>
        <source>The rom could not be installed because no bps patch was found.</source>
        <translation>Diese ROM konnte nicht installiert werden, da keine BPS-Patchdatei gefunden wurde.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="441"/>
        <source>Could not install &apos;%1&apos; because of an unexpected error while applying the patch</source>
        <translation>&apos;%1&apos; konnte nicht installiert werden: Unbekannter Fehler beim Anwenden des Patches</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="451"/>
        <source>Computing checksum</source>
        <translation>Prüfsumme wird berechnet</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-download-dialog.cpp" line="473"/>
        <source>Romhack installed successfully</source>
        <translation>Romhack erfolgreich installiert</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-download-dialog.ui" line="14"/>
        <source>Downloading Patch</source>
        <translation>Patch wird heruntergeladen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-download-dialog.ui" line="25"/>
        <source>Fetching hack info</source>
        <translation>Hack-Info wird geladen</translation>
    </message>
</context>
<context>
    <name>RhdcHackView</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="30"/>
        <source>Hack List:</source>
        <translation>Hack-Liste:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="63"/>
        <source>Sort By:</source>
        <translation>Sortieren nach:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="80"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="85"/>
        <source>Popularity</source>
        <translation>Popularität</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="90"/>
        <source>Rating</source>
        <translation>Bewertung</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="95"/>
        <source>Difficulty</source>
        <translation>Schwierigkeit</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="100"/>
        <source>Last Played</source>
        <translation>Zuletzt gespielt</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="105"/>
        <source>Play Time</source>
        <translation>Spielzeit</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="110"/>
        <source>Stars</source>
        <translation>Sterne</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="131"/>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack-view.ui" line="266"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="86"/>
        <source>All Hack Lists</source>
        <translation>Alle Hack-Listen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="137"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="143"/>
        <source>Authors:</source>
        <translation>Urheber:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="138"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="149"/>
        <source>Unknown</source>
        <extracomment>Shown when no authors are listed for a hack</extracomment>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="143"/>
        <source>Author:</source>
        <translation>Urheber:</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="300"/>
        <source>Create List</source>
        <translation>Liste erstellen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="300"/>
        <source>Enter a name for your new hack list</source>
        <translation>Bitte einen Namen für die neue Hack-Liste eingeben</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="305"/>
        <source>List Exists</source>
        <translation>Liste existiert bereits</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="305"/>
        <source>A hack list with this name already exists</source>
        <translation>Eine Hack-Liste mit diesem Namen existiert bereits</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="331"/>
        <source>Rate Hack</source>
        <translation>Hack bewerten</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="334"/>
        <source>Mark as Not Completed</source>
        <translation>Als &quot;Noch nicht fertig&quot; markieren</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="336"/>
        <source>Mark as Completed</source>
        <translation>Als &quot;Fertig&quot; markieren</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="346"/>
        <source>Move to Hack List...</source>
        <translation>In andere Hack-Liste verschieben...</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="347"/>
        <source>Copy to Hack List...</source>
        <translation>In andere Hack-Liste kopieren...</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="354"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="355"/>
        <source>New List</source>
        <translation>Neue Liste</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="361"/>
        <source>Remove from &apos;%1&apos;</source>
        <translation>Von &apos;%1&apos; entfernen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="368"/>
        <source>Delete Save File</source>
        <translation>Speicherstand löschen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="371"/>
        <source>Edit Save File</source>
        <translation>Speicherstand bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="372"/>
        <source>Import Project64 Save File</source>
        <translation>Project64-Speicherstand importieren</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="373"/>
        <source>Show Save File</source>
        <translation>Speicherstand anzeigen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="377"/>
        <source>Unfollow Hack</source>
        <translation>Hack entfolgen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="379"/>
        <source>Uninstall Hack Version</source>
        <translation>Hack-Version deinstallieren</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="482"/>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="518"/>
        <source>Confirm Delete</source>
        <translation>Löschen bestätigen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="482"/>
        <source>Are you sure you want to delete your save file?</source>
        <translation>Möchtest du deinen Speicherstand wirklich löschen?</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="509"/>
        <source>Confirm Unfollow</source>
        <translation>Entfolgen bestätigen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="509"/>
        <source>Are you sure you want to unfollow this hack? The ROM will not be deleted from your computer, but it will be removed from all of your hack lists on romhacking.com and your progress will no longer be synced with romhacking.com.</source>
        <translation>Bist du dir sicher, dass du diesem Hack entfolgen willst? Die ROM-Datei wird nicht von deinem PC gelöscht, aber der Hack wird aus all deinen Hack-Listen auf romhacking.com entfernt und dein Fortschritt wird nicht mehr synchronisiert.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-view.cpp" line="518"/>
        <source>Are you sure you want to delete this ROM file from your computer?</source>
        <translation>Bist du dir sicher, dass du diese ROM-Datei von deinem PC löschen willst?</translation>
    </message>
</context>
<context>
    <name>RhdcHackWidget</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="302"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="341"/>
        <source>Popularity (Downloads)</source>
        <translation>Popularität (Downloads)</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="385"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="424"/>
        <source>Rating</source>
        <translation>Bewertung</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="465"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="504"/>
        <source>Difficulty</source>
        <translation>Schwierigkeit</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="561"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="600"/>
        <source>Last Played</source>
        <extracomment>Date and time that a hack was last played</extracomment>
        <translation>Zuletzt gespielt</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="641"/>
        <location filename="../src/rhdc/ui/designer/rhdc-hack.ui" line="680"/>
        <source>Total Play Time</source>
        <translation>Gesamtspielzeit</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-widget.cpp" line="145"/>
        <source>Never</source>
        <extracomment>Text shown in place of a date when a hack hasn&apos;t been played yet</extracomment>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-widget.cpp" line="158"/>
        <source>Completed</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-hack-widget.cpp" line="158"/>
        <source>Incomplete</source>
        <translation>Unvollständig</translation>
    </message>
</context>
<context>
    <name>RhdcLoginDialog</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="14"/>
        <source>RHDC Login</source>
        <translation>RHDC-Login</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="66"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To enable romhacking.com integration, you must have an unmodified copy of the US version of &lt;span style=&quot; font-style:italic;&quot;&gt;Super Mario 64&lt;/span&gt; in z64 format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Um romhacking.com-Integration zu aktivieren musst du eine unmodifizierte Kopie der US-Version von &lt;span style=&quot; font-style:italic;&quot;&gt;Super Mario 64&lt;/span&gt; in z64-Format besitzen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="128"/>
        <source>Browse for SM64 ROM</source>
        <translation>SM64-Rom auswählen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="180"/>
        <source>Sign in to romhacking.com</source>
        <translation>Auf romhacking.com anmelden</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="194"/>
        <source>Username</source>
        <translation>Nutzername</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="204"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="256"/>
        <source>Sign In</source>
        <translation>Anmelden</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="359"/>
        <source>Multi-factor authentication is enabled on this account</source>
        <translation>Dieses Benutzerprofil benutzt Multi-Faktor-Authentifizierung</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="366"/>
        <source>Please enter the 6-digit authentication code from your authenticator app.</source>
        <translation>Bitte gebe den 6-stelligen Authentifizierungscode von deiner Authentifizierungs-App ein.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="437"/>
        <source>Verify</source>
        <translation>Bestätigen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="501"/>
        <source>Incorrect authentication code</source>
        <translation>Falscher Authentifizierungscode</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="546"/>
        <source>Please confirm your romhacking.com integration settings. You can always change these later in the settings dialog.</source>
        <translation>Bitte überprüfe deine romhacking.com-Integrationseinstellungen. Du kannst sie später jederzeit im Einstellungsmenü ändern.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="574"/>
        <source>Download Directory</source>
        <translation>Ordner für Downloads</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="596"/>
        <source>Browse</source>
        <translation>Durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="620"/>
        <source>Sets the directory that hacks downloaded from romhacking.com will be saved to. If you change this setting later, your roms will automatically be moved to the new directory.</source>
        <translation>Dies kontrolliert das Verzeichnis, in dem Hacks von romhacking.com gespeichert werden. Wenn diese Einstellung verändert wird, werden alle ROMS automatisch in das neue Verzeichnis verschoben.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="652"/>
        <source>Enable Star Display</source>
        <translation>Sternen-Anzeige einschalten</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="673"/>
        <source>If checked, when a rom is running that has a supported layout file from RHDC, a layout of all the stars in the hack will be shown. This layout updates automatically after the game is saved.</source>
        <translation>Wenn eine ROM mit einem verfügbaren Layout von RHDC gestartet wird, wird eine Übersicht aller Sterne im Hack angezeigt. Dieses Layout wird automatisch beim Speichern aktualisiert.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="710"/>
        <source>Show star display for hacks without a star layout</source>
        <translation>Sternen-Anzeige für Hacks ohne ein Sternen-Layout anzeigen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="731"/>
        <source>If checked, Parallel Launcher will still show a star display with a default layout if the hack author did not provide one.</source>
        <translation>Wenn ein Hack kein Sternen-Layout hat, wird Parallel Launcher das Standard-Layout von SM64 anzeigen.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="768"/>
        <source>Check all save slots</source>
        <translation>Alle Spielstände überprüfen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="786"/>
        <source>If checked, Parallel Launcher will submit your highest star count across all save slots to romhacking.com; otherwise, it will only submit your star count in slot A.</source>
        <translation>Wenn diese Einstellung aktiviert ist, wird Parallel Launcher die höchste Sternenanzahl aller Speicherstände an romhacking.com senden; wenn deaktiviert wird nur Speicherstand A gesendet.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="818"/>
        <source>Prefer HLE plugins</source>
        <translation>HLE-Plugins bevorzugen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="836"/>
        <source>If checked, Parallel Launcher will use the GLideN64 plugin instead of the ParaLLEl plugin when it guesses that ParrLLEl is probably the best plugin to use. Only check this if your computer has an old GPU with poor Vulkan support. If you still experience lag even with GLideN64, you may need to disable &apos;Emulate Framebuffer&apos; and/or &apos;Emulate N64 Depth Compare&apos; in the GFX Plugins section of your Parallel Launcher settings.</source>
        <translation>Wenn diese Einstellung aktiviert ist, wird Parallel Launcher bei der automatischen Erkennung des Grafikplugins das GLideN64-Plugin anstelle des ParaLLEl-Plugins verwenden. Bitte aktiviere diese Einstellung nur, wenn du eine ältere GPU mit schlechtem Vulkan-Support benutzt. Wenn du immer noch schlechte Performance mit GLideN64 bekommst, versuche die &quot;Framebuffer emulieren&quot;- und/oder &quot;N64 Depth Compare emulieren&quot;-Einstellung in den Grafikplugin-Einstellungen zu deaktivieren.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="868"/>
        <source>Ignore widescreen hint</source>
        <translation>Breitbild-Empfehlung ignorieren</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-login-dialog.ui" line="886"/>
        <source>If checked, Parallel Launcher will always default to using 4:3 resolution, even if the recommended settings indicate that widescreen is supported</source>
        <translation>Wenn diese Einstellung aktiviert ist, wird Parallel Launcher immer das 4:3-Bildformat verwenden, auch wenn Breitbild laut den empfohlenen Einstellungen unterstützt wird</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="79"/>
        <source>Select SM64 ROM</source>
        <translation>SM64-ROM auswählen</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="85"/>
        <source>ROM Does not Match</source>
        <translation>ROM stimmt nicht überein</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="85"/>
        <source>The provided rom does not match the expected checksum.</source>
        <translation>Die angegebene ROM stimmt nicht mit der erwarteten Prüfsumme überein.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="127"/>
        <source>Select a new download directory</source>
        <translation>Wähle ein neues Download-Verzeichnis</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="150"/>
        <source>Enter your username</source>
        <translation>Bitte Nutzernamen eingeben</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="155"/>
        <source>Enter your password</source>
        <translation>Bitte Passwort eingeben</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="206"/>
        <source>Username or password is incorrect</source>
        <translation>Nutzername/Passwort sind ungültig</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="209"/>
        <source>The romhacking.com servers appear to be down.</source>
        <translation>Die romhacking.com-Server sind nicht erreichbar.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-login-dialog.cpp" line="212"/>
        <source>An unknown error occurred.</source>
        <translation>Ein unbekannter Fehler ist aufgetreten.</translation>
    </message>
</context>
<context>
    <name>RhdcRatingDialog</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="14"/>
        <source>Rate this Hack</source>
        <translation>Hack bewerten</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="25"/>
        <source>How would you rate this hack?</source>
        <translation>Wie würdest du diesen Hack bewerten?</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="51"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Quality. &lt;/span&gt;How enjoyable, impressive, and/or polished was this hack?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Qualität. &lt;/span&gt;Wie spaßig, beeindruchend, und/oder qualitativ war dieser Hack?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-rating-dialog.ui" line="162"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Difficulty. &lt;/span&gt;In general, how would you rate this hack&apos;s difficulty level?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Schwierigkeit. &lt;/span&gt;Wie würdest du die Schwierigkeit in diesem Hack bewerten?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="10"/>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="24"/>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="38"/>
        <source>I can&apos;t decide or have no opinion</source>
        <extracomment>0</extracomment>
        <translation>Ich bin unentschlossen/habe keine Meinung</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="11"/>
        <source>Zero Effort</source>
        <extracomment>1</extracomment>
        <translation>Keine Mühe</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="12"/>
        <source>Poor Quality</source>
        <extracomment>2</extracomment>
        <translation>Schlechte Qualität</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="13"/>
        <source>A Little Rough</source>
        <extracomment>3</extracomment>
        <translation>Ein wenig grob</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="14"/>
        <source>Unremarkable</source>
        <extracomment>4</extracomment>
        <translation>Nichts besonderes</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="15"/>
        <source>Decent</source>
        <extracomment>5</extracomment>
        <translation>Gut</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="16"/>
        <source>Pretty Good</source>
        <extracomment>6</extracomment>
        <translation>Ziemlich gut</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="17"/>
        <source>Very Good</source>
        <extracomment>7</extracomment>
        <translation>Sehr gut</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="18"/>
        <source>Excellent</source>
        <extracomment>8</extracomment>
        <translation>Exzellent</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="19"/>
        <source>Exceptional</source>
        <extracomment>9</extracomment>
        <translation>Außerordentlich</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="20"/>
        <source>Legendary</source>
        <extracomment>10</extracomment>
        <translation>Legendär</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="25"/>
        <source>No Challenge</source>
        <extracomment>1</extracomment>
        <translation>Keine Schwierigkeit</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="26"/>
        <source>Very Easy</source>
        <extracomment>2</extracomment>
        <translation>Sehr einfach</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="27"/>
        <source>Casual</source>
        <extracomment>3</extracomment>
        <translation>Casual</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="28"/>
        <source>Classic SM64</source>
        <extracomment>4</extracomment>
        <translation>Wie SM64</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="29"/>
        <source>Moderate</source>
        <extracomment>5</extracomment>
        <translation>Moderat</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="30"/>
        <source>Challenging</source>
        <extracomment>6</extracomment>
        <translation>Herausfordernd</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="31"/>
        <source>Difficult</source>
        <extracomment>7</extracomment>
        <translation>Schwer</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="32"/>
        <source>Very Difficult</source>
        <extracomment>8</extracomment>
        <translation>Sehr schwer</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="33"/>
        <source>Extremely Difficult</source>
        <extracomment>9</extracomment>
        <translation>Extrem schwer</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="34"/>
        <source>Borderline Kaizo</source>
        <extracomment>10</extracomment>
        <translation>Fast schon Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="39"/>
        <source>Beginner/Introductory Kaizo</source>
        <extracomment>1</extracomment>
        <translation>Einstiegs-/Beginner-Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="40"/>
        <source>Easy Kaizo</source>
        <extracomment>2</extracomment>
        <translation>Einfacher Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="41"/>
        <source>Standard Kaizo</source>
        <extracomment>3</extracomment>
        <translation>Normaler Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="42"/>
        <source>Moderate Kaizo</source>
        <extracomment>4</extracomment>
        <translation>Moderater Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="43"/>
        <source>Challenging Kaizo</source>
        <extracomment>5</extracomment>
        <translation>Herausfordernder Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="44"/>
        <source>Difficult Kaizo</source>
        <extracomment>6</extracomment>
        <translation>Schwerer Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="45"/>
        <source>Very Difficult Kaizo</source>
        <extracomment>7</extracomment>
        <translation>Sehr schwerer Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="46"/>
        <source>Extremely Difficult Kaizo</source>
        <extracomment>8</extracomment>
        <translation>Extrem schwerer Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="47"/>
        <source>Hardest humanly possible Kaizo</source>
        <extracomment>9</extracomment>
        <translation>Gerade noch von Menschen machbarer Kaizo</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="48"/>
        <source>TAS/Unbeatable</source>
        <extracomment>10</extracomment>
        <translation>TAS/Nicht machbar</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="59"/>
        <source>Submit</source>
        <translation>Absenden</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/rhdc-rating-dialog.cpp" line="85"/>
        <source>Not Rated</source>
        <translation>Nicht bewertet</translation>
    </message>
</context>
<context>
    <name>RhdcSaveEditor</name>
    <message>
        <location filename="../src/rhdc/ui/rhdc-save-editor.cpp" line="24"/>
        <source>Save Editor</source>
        <translation>Speicherstand-Editor</translation>
    </message>
</context>
<context>
    <name>RhdcSaveSyncDialog</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="14"/>
        <source>Synchronize Save Files</source>
        <translation>Speicherdaten synchronisieren</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="32"/>
        <source>Save file synchronization is enabled for this hack; however, your save file progress is not currently in sync. Please select which save file you would like to use for this hack.</source>
        <translation>Speicherdaten-Synchronisierung ist für diesen Hack aktiviert, jedoch sind mehrere Speicherdaten von anderen Hack-Versionen vorhanden. Bitte wähle die Speicherdate aus, die du für den Hack verwenden willst.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="67"/>
        <source>Version</source>
        <extracomment>Which version of the hack the save file is for</extracomment>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="72"/>
        <source>Last Saved</source>
        <extracomment>When the save file was last modified</extracomment>
        <translation>Zuletzt gespeichert am</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-save-sync-dialog.ui" line="77"/>
        <source>Stars</source>
        <extracomment>Number of stars collected on the save file</extracomment>
        <translation>Sterne</translation>
    </message>
</context>
<context>
    <name>RhdcSync</name>
    <message>
        <location filename="../src/rhdc/core/sync.cpp" line="235"/>
        <source>Failed to connect to romhacking.com</source>
        <translation>Fehler beim Verbinden mit romhacking.com</translation>
    </message>
</context>
<context>
    <name>RhdcViewBubble</name>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-view-bubble.ui" line="171"/>
        <source>Click here to go to the romhacking.com view.</source>
        <translation>Dieser Knopf bringt dich zur romhacking.com-Ansicht.</translation>
    </message>
    <message>
        <location filename="../src/rhdc/ui/designer/rhdc-view-bubble.ui" line="214"/>
        <source>Okay</source>
        <translation>Okay</translation>
    </message>
</context>
<context>
    <name>RomListModel</name>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="243"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="244"/>
        <source>Internal Name</source>
        <translation>Interner Name</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="245"/>
        <source>File Path</source>
        <translation>Dateipfad</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="246"/>
        <source>Last Played</source>
        <translation>Zuletzt gespielt</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-model.cpp" line="247"/>
        <source>Play Time</source>
        <translation>Spielzeit</translation>
    </message>
</context>
<context>
    <name>RomListView</name>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="236"/>
        <source>Delete Save File</source>
        <translation>Speicherstand löschen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="237"/>
        <source>[SM64] Edit Save File</source>
        <translation>[SM64] Speicherstand bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="240"/>
        <source>Import Project64 Save File</source>
        <translation>Project64-Speicherstand importieren</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="241"/>
        <source>Show Save File</source>
        <translation>Speicherstand anzeigen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="246"/>
        <source>Download</source>
        <translation>Herunterladen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="250"/>
        <source>Add to...</source>
        <translation>Hinzufügen zu...</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="257"/>
        <source>New Group</source>
        <translation>Neue Gruppe</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="261"/>
        <source>Remove from %1</source>
        <translation>Von %1 entfernen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="267"/>
        <source>Open Containing Folder</source>
        <translation>Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="268"/>
        <source>Delete ROM</source>
        <translation>ROM löschen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="270"/>
        <location filename="../src/ui/rom-list-view.cpp" line="373"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="272"/>
        <source>Revert ROM Name</source>
        <translation>ROM-Namen entfernen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="277"/>
        <source>Rate Hack</source>
        <translation>Hack bewerten</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="280"/>
        <source>Mark as Not Completed</source>
        <translation>Als &quot;Noch nicht fertig&quot; markieren</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="282"/>
        <source>Mark as Completed</source>
        <translation>Als &quot;Fertig&quot; markieren</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="305"/>
        <location filename="../src/ui/rom-list-view.cpp" line="364"/>
        <source>Confirm Delete</source>
        <translation>Löschen bestätigen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="305"/>
        <source>Are you sure you want to delete your save file?</source>
        <translation>Möchtest du deinen Speicherstand wirklich löschen?</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="325"/>
        <source>Create Group</source>
        <translation>Gruppe erstellen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="325"/>
        <source>Enter a name for your new group</source>
        <translation>Bitte einen neuen Namen für diese Gruppe eingeben</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="331"/>
        <source>Group Exists</source>
        <translation>Gruppe existiert</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="331"/>
        <source>A group with this name already exists</source>
        <translation>Eine Gruppe mit diesem Namen existiert bereits</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="364"/>
        <source>This will completely remove the ROM file from your computer. Are you sure you want to delete this ROM?</source>
        <translation>Dies wird die ROM-Datei auf deinem Computer komplett entfernen. Willst du dies wirklich tun?</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-list-view.cpp" line="373"/>
        <source>Enter a new name for your rom</source>
        <translation>Bitte einen neuen Namen für diese ROM eingeben</translation>
    </message>
</context>
<context>
    <name>RomSettingsWidget</name>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="29"/>
        <source>Input Mode</source>
        <translation>Eingabe-Modus</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="76"/>
        <source>SD Card</source>
        <translation>SD-Karte</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="156"/>
        <source>Unlocks the CPU to run at full speed, removing CPU-based lag you would encounter on console. This is almost always safe, but can rarely cause issues on certain ROMs.</source>
        <translation>Entfernt die Geschwindigkeitseinschränkung des CPUs, wodurch CPU-basierender Lag entfernt wird. Fast immer sicher zu benutzen, könnte aber Fehler in bestimmten ROMs verursachen.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="159"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="171"/>
        <source>Overclock CPU (Recommended)</source>
        <translation>CPU übertakten (Empfohlen)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="175"/>
        <source>Alters the vertical interrupt timing. In most cases this either gives a slight performance boost or has no effect, but in some cases it can cause the game to run at the wrong framerate.</source>
        <translation>Modifiziert das Vertical Interrupt-Timing. In den meisten Fällen resultiert es in einem leichten Geschwindigkeitsboost oder hat keinen Effekt, aber in manchen Fällen könnte das Spiel deswegen mit der falschen Bildwiederholungsrate laufen.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="178"/>
        <source>Overclock VI</source>
        <translation>VI übertakten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="191"/>
        <source>Overrides save type autodetection, and forces the use of 16kB EEPROM for save data. This breaks roms that use 4kB EEPROM, so only enable this for romhacks that require it.</source>
        <translation>Diese Option überschreibt die automatische Speichertyperkennung, sodass immer ein 16kB-EEPROM benutzt wird. ROMs, die einen 4kB-EEPROM benutzen, werden nicht funktionieren, aktiviere diese Option also nur, wenn die ROM sie benötigt.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="194"/>
        <source>Force 16kB EEPROM</source>
        <translation>16kB-EEPROM erzwingen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="207"/>
        <source>Use the interpreter core instead of the dynamic recompiler. Slower, but can be useful for debugging.</source>
        <translation>Der Interpreter-Core wird anstelle von der dynamischen Rekompilierung benutzt. Er ist langsamer, kann aber zum Debugging hilfreich sein.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="210"/>
        <source>Use Interpreter Core</source>
        <translation>Interpreter-Core benutzen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="223"/>
        <source>Enable widescreen mode by stretching the video. Some ROMs have a widescreen mode that will allow the video to no longer be stretched when it is enabled.</source>
        <translation>Der Breitbild-Modus wird aktiviert, indem das Originalbild verzerrt wird. Manche ROMs haben einen nativen Breitbild-Modus, wodurch das Bild nicht mehr verzerrt wird.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="226"/>
        <source>Widescreen (Stretched)</source>
        <translation>Breitbild-Modus (Verzerrt)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="239"/>
        <source>Enable widescreen mode by forcing the game to use a widescreen viewport. You will experience visual artifacts on the sides of the screen unless you are playing a romhack specifically designed for this widescreen mode.</source>
        <translation>Der Breitbild-Modus wird aktiviert, indem das Spiel dazu gezwungen wird, einen Breitbild-Viewport zu benutzen. Du wirst visuelle Fehler an den Seiten des Bildes sehen, wenn du nicht einen ROM-Hack spielst der mit dieser Funktion entworfen wurde.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="242"/>
        <source>Widescreen (Viewport Hack)</source>
        <translation>Breitbild-Modus (Viewport-Hack)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="255"/>
        <source>Upscale textures drawn using the TEX_RECT command. This can cause visual artifacts in some games.</source>
        <translation>Texturen hochskalieren, die mit dem TEX_RECT-Befehl angezeigt werden. Kann in manchen Spielen zu Artefakten führen.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="258"/>
        <source>Upscale TEXRECTs</source>
        <translation>TEXRECTs hochskalieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="271"/>
        <source>Removes the black borders on the left and right sides of the video. Since these pixels are never rendered on real hardware, results will vary depending on the game.</source>
        <translation>Entfernt die schwarzen Ränder am linken und rechten Rand des Bildes. Da diese Pixel auf realer Hardware nicht angezeigt werden, können die Ergebnisse je nach Spiel variieren.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="274"/>
        <source>Remove Black Borders</source>
        <translation>Schwarze Ränder entfernen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="287"/>
        <source>Emulate the native framebuffer. This is required for some visual effects to work, but may cause lag on lower end GPUs</source>
        <translation>Emuliert den nativen Framebuffer. Wird für manche visuellen Effekte gebraucht, kann aber auf langsameren GPUs Lag verursachen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="290"/>
        <source>Emulate Framebuffer</source>
        <translation>Framebuffer emulieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="306"/>
        <source>Greatly increases accuracy by rendering decals correctly, but may cause a loss in performance</source>
        <translation>Erhöht Genauigkeit erheblich, indem Decals korrekt gerendert werden. Kann einen Performanceverlust verursachen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="309"/>
        <source>Emulate N64 Depth Compare</source>
        <translation>N64 Depth Compare emulieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="373"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning:&lt;/span&gt; The &lt;span style=&quot; font-style:italic;&quot;&gt;Emulate N64 Depth Compare&lt;/span&gt; option does not work on MacOS. Some things may not render correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warnung:&lt;/span&gt; Die &lt;span style=&quot; font-style:italic;&quot;&gt;N64 Depth Compare emulieren&lt;/span&gt;-Option funktioniert auf MacOS nicht. Manche Dinge könnten nicht richtig angezeigt werden.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="398"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning:&lt;/span&gt; This ROM supports mouse input, but you do not have a hotkey assigned to &lt;span style=&quot; font-style:italic;&quot;&gt;Grab Mouse&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>&quot;Grab Mouse&quot; = &quot;Maus einfangen&quot;</translatorcomment>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warnung:&lt;/span&gt; Diese ROM unterstützt Mauseingaben, aber du hast noch keine Tastenbelegung für &lt;span style=&quot; font-style:italic;&quot;&gt;Maus einfangen&lt;/span&gt; zugewiesen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="557"/>
        <source>gln64 (Obsolete)</source>
        <translation>gln64 (Obsolet)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="475"/>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="592"/>
        <source>Graphics Plugin</source>
        <translation>Grafikplugin</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="20"/>
        <source>Sync save files between hack versions</source>
        <translation>Speicherdaten mit anderen Hack-Versionen synchronisieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="325"/>
        <source>Allows roms with custom RSP microcode to be played, but causes visible seams in models due to plugin inaccuracies.</source>
        <translation>Macht das Spielen von ROMs mit eigenem RSP-Mikrocode möglich, aber lässt aufgrund von Plugin-Ungenauigkeiten Abstände zwischen Modellen entstehen.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="348"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Not enabling CPU overclocking is likely to result in a laggy experience. This option should only be used for speedruns or testing where you want to approximate console CPU lag.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warnung: &lt;/span&gt;CPU-Übertaktung zu deaktivieren wird wahrscheinlich zu schlechter Performance führen. Diese Option sollte nur in Speedruns oder wenn du ungefähr konsolengenauen CPU-Lag brauchst aktiviert werden.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="484"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="539"/>
        <source>ParaLLEl (Recommended - very accurate to console)</source>
        <translation>ParaLLEI (Empfohlen - Sehr konsolengenau)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="497"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="540"/>
        <source>GLideN64 (Recommended for lower end computers)</source>
        <translation>GLideN64 (Empfohlen für ältere Computer)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="507"/>
        <location filename="../src/ui/rom-settings-widget.cpp" line="541"/>
        <source>OGRE (Needed by some older romhacks)</source>
        <translation>OGRE (Von älteren ROM-Hacks benötigt)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-settings-widget.ui" line="601"/>
        <source>ParaLLEl (Recommended)</source>
        <translation>ParaLLEI (Empfohlen)</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="171"/>
        <source>Overclock CPU</source>
        <translation>CPU übertakten</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="331"/>
        <source>Show Fewer Plugins</source>
        <translation>Weniger Plugins anzeigen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="331"/>
        <source>Show More Plugins</source>
        <translation>Mehr Plugins anzeigen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="342"/>
        <source>This ROM supports mouse input. After launching the emulator, your mouse will be captured by the emulator. Press %1 if you need to free the mouse cursor.</source>
        <translation>Diese ROM unterstützt Mauseingaben. Sobald der Emulator gestartet wird, wird deine Maus eingefangen. Drücke %1 um die Maus freizugeben.</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="521"/>
        <source>(recommended by hack author)</source>
        <translation>(vom Hack-Autor empfohlen)</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-settings-widget.cpp" line="613"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
</context>
<context>
    <name>RomSourceDialog</name>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="14"/>
        <source>Manage ROM Sources</source>
        <translation>ROM-Quellen verwalten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="31"/>
        <source>ROM Search Folders</source>
        <translation>ROM-Ordner</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="68"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="82"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="77"/>
        <source>New Source</source>
        <translation>Quelle hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="103"/>
        <source>ROM Source</source>
        <translation>ROM-Quelle</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="114"/>
        <source>Browse for a folder</source>
        <translation>Bitte einen Ordner auswählen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="121"/>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="431"/>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="502"/>
        <source>Browse</source>
        <translation>Durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="134"/>
        <source>Also scan all subfolders</source>
        <translation>Auch Unterordner durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="137"/>
        <source>Recursive</source>
        <translation>Rekursiv</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="168"/>
        <source>Ignore directories that begin with a period character</source>
        <translation>Versteckte Ordner (die mit einem Punkt beginnen) ignorieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="171"/>
        <source>Ignore hidden directories</source>
        <translation>Versteckte Ordner ignorieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="183"/>
        <source>Max Depth</source>
        <translation>Maximale Tiefe</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="236"/>
        <source>Follow symbolic links to folders and ROMs</source>
        <translation>Symbolische Links zu Ordnern und ROMs folgen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="239"/>
        <source>Follow Symlinks</source>
        <translation>Symlinks folgen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="256"/>
        <source>Automatically add to groups</source>
        <translation>Automatisch zu Gruppen hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="289"/>
        <source>Manage Groups</source>
        <translation>Gruppen verwalten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="317"/>
        <source>Individual ROMs</source>
        <translation>Individuelle ROMs</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="348"/>
        <source>Forget Selected ROM</source>
        <translation>Ausgewählte ROM vergessen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="359"/>
        <source>Add New ROM(s)</source>
        <translation>ROM(s) hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="388"/>
        <source>BPS Patches</source>
        <translation>BPS-Patches</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="417"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="298"/>
        <source>Patch File</source>
        <translation>Patch-Datei</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="444"/>
        <source>Base ROM</source>
        <translation>Basis-ROM</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="450"/>
        <source>Automatic (Search Existing ROMs)</source>
        <translation>Automatisch (Hinzugefügte ROMs durchsuchen)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="463"/>
        <source>Provided ROM:</source>
        <translation>Diese ROM:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/rom-source-dialog.ui" line="532"/>
        <source>Apply Patch</source>
        <translation>Patch anwenden</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="275"/>
        <source>Select one or more ROMs</source>
        <translation>Bitte eine oder mehrere ROMs auswählen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="297"/>
        <source>Select a .bps patch file</source>
        <translation>Bitte eine .BPS-Patchdatei auswählen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="308"/>
        <source>Select a ROM</source>
        <translation>Bitte eine ROM auswählen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="337"/>
        <source>Patch Applied</source>
        <translation>Patch angewendet</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="337"/>
        <source>Saved patched rom to %1</source>
        <translation>Modifizierte ROM gespeichert als %1</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="352"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="355"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="359"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="361"/>
        <location filename="../src/ui/rom-source-dialog.cpp" line="365"/>
        <source>Patch Failed</source>
        <translation>Patch fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="352"/>
        <source>Failed to patch ROM. The .bps patch appears to be invalid.</source>
        <translation>Fehler beim Patchen der ROM: Der .bps-Patch scheint ungültig zu sein.</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="355"/>
        <source>Failed to patch ROM. The base ROM does not match what the patch expects.</source>
        <translation>Fehler beim Patchen der ROM: Die Basis-ROM hat nicht die vom Patch erwartete Prüfsumme.</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="359"/>
        <source>Failed to patch ROM. The base rom required to patch is not known to Parallel Launcher.</source>
        <translation>Fehler beim Patchen der ROM: Die Basis-ROM ist Parallel-Launcher nicht bekannt.</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="361"/>
        <source>Failed to patch ROM. The base ROM is missing or invalid.</source>
        <translation>Fehler beim Patchen der ROM: Die Basis-ROM scheint ungültig oder nicht vorhanden zu sein.</translation>
    </message>
    <message>
        <location filename="../src/ui/rom-source-dialog.cpp" line="365"/>
        <source>Failed to patch ROM. An error occurred while writing the patched ROM to disk.</source>
        <translation>Fehler beim Patchen der ROM: Ein Fehler beim Speichern der ROM auf die Festplatte ist aufgetreten.</translation>
    </message>
</context>
<context>
    <name>SaveFileEditorDialog</name>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="14"/>
        <source>Edit SM64 Save File</source>
        <translation>SM64-Speicherdatei bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="29"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;This is a save file editor for Super Mario 64 and SM64 romhacks. Attempting to use this save file editor with other ROMs will corrupt your save file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warnung: &lt;/span&gt;Dieser Speicherdaten-Editor ist nur für Super Mario 64 und Romhacks. Wenn du ihn mit anderen ROMs benutzt, werden deine Speicherdaten beschädigt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="41"/>
        <source>Save Slot:</source>
        <translation>Speicherstand:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="55"/>
        <source>Slot A (Empty)</source>
        <translation>Speicherstand A (Leer)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="60"/>
        <source>Slot B (Empty)</source>
        <translation>Speicherstand B (Leer)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="65"/>
        <source>Slot C (Empty)</source>
        <translation>Speicherstand C (Leer)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="70"/>
        <source>Slot D (Empty)</source>
        <translation>Speicherstand D (Leer)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="95"/>
        <source>Delete Save Slot</source>
        <translation>Speicherstand löschen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-file-editor-dialog.ui" line="106"/>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="62"/>
        <source>Edit Save Slot</source>
        <translation>Speicherstand bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="15"/>
        <source>Slot %1 (Empty)</source>
        <translation>Speicherstand %1 (Leer)</translation>
    </message>
    <message>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="18"/>
        <source>Slot %1 (%2 Stars)</source>
        <translation>Speicherstand %1 (%2 Sterne)</translation>
    </message>
    <message>
        <location filename="../src/ui/save-file-editor-dialog.cpp" line="66"/>
        <source>Create Save Slot</source>
        <translation>Speicherstand erstellen</translation>
    </message>
</context>
<context>
    <name>SaveSlotEditorDialog</name>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="14"/>
        <source>Edit Save Slot (SM64)</source>
        <translation>Speicherplatz bearbeiten (SM64)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="20"/>
        <source>Show flags and stars unused in the vanilla game</source>
        <translation>Auch Flaggen und Sterne anzeigen, die im unmodifizertem Spiel unbenutzt sind</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="32"/>
        <source>Flags</source>
        <translation>Flaggen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="74"/>
        <source>Caps Unlocked</source>
        <translation>Freigeschaltete Kappen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="80"/>
        <source>Wing Cap</source>
        <translation>Federkappe</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="93"/>
        <source>Metal Cap</source>
        <translation>Titankappe</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="106"/>
        <source>Vanish Cap</source>
        <translation>Tarnkappe</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="122"/>
        <source>Keys Collected</source>
        <translation>Eingesammelte Schlüssel</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="128"/>
        <source>Basement Key</source>
        <translation>Kellerschlüssel</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="141"/>
        <source>Upstairs Key</source>
        <translation>Dachbodenschlüssel</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="157"/>
        <source>Unlocked Doors</source>
        <translation>Aufgeschlossene Türen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="163"/>
        <source>Basement Key Door</source>
        <translation>Kellertür</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="176"/>
        <source>Upstairs Key Door</source>
        <translation>Dachbodentür</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="189"/>
        <source>Peach&apos;s Secret Slide Door</source>
        <translatorcomment>door to &quot;Toadstools Rutschbahn&quot;</translatorcomment>
        <translation>Tür zu Toadstools Rutschbahn</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="202"/>
        <source>Whomp&apos;s Fortress Door</source>
        <translatorcomment>door to &quot;Wummps Wuchtwall&quot;</translatorcomment>
        <translation>Tür zu Wummps Wuchtwall</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="215"/>
        <source>Cool Cool Mountain Door</source>
        <translatorcomment>door to &quot;Bibberberg Bob&quot;</translatorcomment>
        <translation>Tür zu Bibberberg Bob</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="228"/>
        <source>Jolly Rodger Bay Door</source>
        <translatorcomment>door to &quot;Piratenbucht Panik&quot;</translatorcomment>
        <translation>Tür zu Piratenbucht Panik</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="241"/>
        <source>Dark World Door</source>
        <translation>Tür zu Bowsers Schattenwelt</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="254"/>
        <source>Fire Sea Door</source>
        <translation>Tür zu Bowsers Lavasee</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="267"/>
        <source>50 Star Door</source>
        <translation>50-Sterne-Tür</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="283"/>
        <source>Miscellaneous</source>
        <translation>Sonstiges</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="289"/>
        <source>Moat Drained</source>
        <translation>Wasser im Graben entfernt</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="302"/>
        <source>Bowser&apos;s Sub Gone</source>
        <translation>Bowsers U-Boot entfernt</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="318"/>
        <source>Unused</source>
        <translation>Unbenutzt</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="366"/>
        <source>Lost/Stolen Cap</source>
        <translation>Verlorene/Gestohlene Kappe</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="374"/>
        <source>Level</source>
        <translation>Level</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="395"/>
        <source>Area</source>
        <translation>Umgebung</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="427"/>
        <source>Cap Stolen By:</source>
        <translation>Kappe gestohlen von:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="434"/>
        <source>The flag indicates that Mario&apos;s cap is on the ground, however, when loading a save file, a cap that is on the ground is either given back to Mario or moved to the appropriate NPC for the area, so it will never actually be on the ground.</source>
        <translation>Diese Flagge gibt an dass Marios Kappe auf dem Boden liegt, jedoch wird beim Laden einer Speicherdatei eine auf dem Boden liegende Kappe entweder zu Mario zurückgegeben oder zum umgebungs-speziefischen NPC bewegt. Somit wird die Kappe nie wirklich auf dem Boden liegen.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="437"/>
        <source>Automatic*</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="450"/>
        <source>Klepto (Bird)</source>
        <translation>Klepto der Kondor</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="463"/>
        <source>Ukiki (Monkey)</source>
        <translation>Schim-Peng (Schimpanse)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="476"/>
        <source>Mr. Blizzard (Snowman)</source>
        <translation>Schneemann</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/save-slot-editor-dialog.ui" line="499"/>
        <source>Stars and Coins</source>
        <translation>Sterne und Münzen</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="44"/>
        <source>Big Boo&apos;s Haunt</source>
        <translation>Big Boos Burg</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="45"/>
        <source>Cool Cool Mountain</source>
        <translation>Bibberberg Bob</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="46"/>
        <source>Castle Interior</source>
        <translation>Schloss (innen)</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="47"/>
        <source>Hazy Maze Cave</source>
        <translation>Grüne Giftgrotte</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="48"/>
        <source>Shifting Sand Land</source>
        <translation>Wobiwaba Wüste</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="49"/>
        <source>Bob-Omb Battlefield</source>
        <translation>Bob-ombs Bombenberg</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="50"/>
        <source>Snowman&apos;s Land</source>
        <translation>Frostbeulen Frust</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="51"/>
        <source>Wet Dry World</source>
        <translation>Atlantis Aquaria</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="52"/>
        <source>Jolly Rodger Bay</source>
        <translation>Piratenbucht Panik</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="53"/>
        <source>Tiny-Huge Island</source>
        <translation>Gulliver Gumba</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="54"/>
        <source>Tick Tock Clock</source>
        <translation>Ticktack-Trauma</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="55"/>
        <source>Rainbow Ride</source>
        <translation>Regenbogen Raserei</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="56"/>
        <source>Castle Grounds</source>
        <translation>Schloss-Umgebung</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="57"/>
        <source>Bowser in the Dark World</source>
        <translation>Bowsers Schattenwelt</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="58"/>
        <source>Vanish Cap Under the Moat</source>
        <translation>Blauer Schalterpalast</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="59"/>
        <source>Bowser in the Fire Sea</source>
        <translation>Bowsers Lavasee</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="60"/>
        <source>Secret Aquarium</source>
        <translation>Verstecktes Aquarium</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="61"/>
        <source>Bowser in the Sky</source>
        <translation>Bowsers Luftschloss</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="62"/>
        <source>Lethal Lava Land</source>
        <translation>Lava Lagune</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="63"/>
        <source>Dire Dire Docks</source>
        <translation>Wilde Wasserwerft</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="64"/>
        <source>Whomp&apos;s Fortress</source>
        <translation>Wummps Wuchtwall</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="65"/>
        <source>End Screen</source>
        <translation>Endbildschirm</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="66"/>
        <source>Castle Courtyard</source>
        <translation>Schloss-Innenhof</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="67"/>
        <source>Peach&apos;s Secret Slide</source>
        <translation>Toadstools Rutschbahn</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="68"/>
        <source>Cavern of the Metal Cap</source>
        <translation>Hinter dem Wasserfall</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="69"/>
        <source>Tower of the Wing Cap</source>
        <translation>Roter Schalterpalast</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="70"/>
        <source>Bowser 1</source>
        <translation>Bowser 1</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="71"/>
        <source>Winged Mario over the Rainbow</source>
        <translation>Regenbogen Feuerwerk</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="73"/>
        <source>Bowser 2</source>
        <translation>Bowser 2</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="74"/>
        <source>Bowser 3</source>
        <translation>Bowser 3</translation>
    </message>
    <message>
        <location filename="../src/core/sm64.cpp" line="76"/>
        <source>Tall, Tall Mountain</source>
        <translation>Fliegenpilz Fiasko</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="11"/>
        <source>8 Red Coins</source>
        <translation>8 rote Münzen</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="12"/>
        <source>100 Coins</source>
        <translation>100 Münzen</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="17"/>
        <source>Toad 1</source>
        <translation>Toad 1</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="18"/>
        <source>Toad 2</source>
        <translation>Toad 2</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="19"/>
        <source>Toad 3</source>
        <translation>Toad 3</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="20"/>
        <source>MIPS 1</source>
        <translation>MIPS 1</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="21"/>
        <source>MIPS 2</source>
        <translation>MIPS 2</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="27"/>
        <source>Big Bob-Omb on the Summit</source>
        <translation>Besiege König Bob-omb!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="28"/>
        <source>Footface with Koopa the Quick</source>
        <translation>Besiege den schnellen Koopa!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="29"/>
        <source>Shoot to the Island in the Sky</source>
        <translation>Fliege zur schwebenden Insel!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="30"/>
        <source>Find the 8 Red Coins</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="31"/>
        <source>Mario Wings to the Sky</source>
        <translation>Durchfliege alle Münzenringe!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="32"/>
        <source>Behind Chain Chomp&apos;s Gate</source>
        <translation>Befreie den Kettenhund!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="37"/>
        <source>Chip Off Whomp&apos;s Block</source>
        <translation>Besiege König Wummp!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="38"/>
        <source>To the Top of the Fortress</source>
        <translation>Steige auf die Bergfestung!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="39"/>
        <source>Shoot into the Wild Blue</source>
        <translation>Flieg ins Blaue!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="40"/>
        <source>Red Coins on the Floating Isle</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="41"/>
        <source>Fall onto the Caged Island</source>
        <translation>Wecke die Eule!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="42"/>
        <source>Blast Away the Wall</source>
        <translation>Zerstöre die Felswand!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="47"/>
        <source>Plunder in the Sunken Ship</source>
        <translation>Tauche in das Schiffswrack!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="48"/>
        <source>Can the Eel Come Out to Play?</source>
        <translation>Locke den Aal Kano heraus!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="49"/>
        <source>Treasure of the Ocean Cave</source>
        <translation>Tauche zur Schatzhöhle!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="50"/>
        <source>Red Coins on the Ship Afloat</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="51"/>
        <source>Blast to the Stone Pillar</source>
        <translation>Fliege zu den Steinsäulen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="52"/>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="110"/>
        <source>Through the Jet Stream</source>
        <translation>Laufe durch den Strudel!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="57"/>
        <source>Slip Slidin&apos; Away</source>
        <translation>Rutsche auf der Eisbahn!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="58"/>
        <source>Li&apos;l Penguin Lost</source>
        <translation>Finde das Pinguinbaby!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="59"/>
        <source>Big Penguin Race</source>
        <translation>Besiege den schnellen Pinguin!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="60"/>
        <source>Frosty Slide for 8 Red Coins</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="61"/>
        <source>Snowman&apos;s Lost his Head</source>
        <translation>Baue einen Schneemann!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="62"/>
        <source>Wall Kicks Will Work</source>
        <translation>Denke an den Wandsprung!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="67"/>
        <source>Go on a Ghost Hunt</source>
        <translation>Gehe auf Geisterjagd!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="68"/>
        <source>Ride Big Boo&apos;s Merry-Go-Round</source>
        <translation>Besuche Big Boos Karussell!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="69"/>
        <source>Secret of the Haunted Books</source>
        <translation>Löse das Bücherei-Rätsel!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="70"/>
        <source>Seek the 8 Red Coins</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="71"/>
        <source>Big Boo&apos;s Balcony</source>
        <translation>Klettere auf Big Boos Balkon!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="72"/>
        <source>Eye to Eye in the Secret Room</source>
        <translation>Finde das Auge im Geheimraum!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="77"/>
        <source>Swimming Beast in the Cavern</source>
        <translation>Klettere auf Nessies Rücken!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="78"/>
        <source>Elevate for 8 Red Coins</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="79"/>
        <source>Metal-Head Mario Can Move!</source>
        <translation>Laufe auf dem Grund des Sees!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="80"/>
        <source>Navigating the Toxic Maze</source>
        <translation>Durchsuche das Nebellabyrinth!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="81"/>
        <source>A-maze-ing Emergency Exit</source>
        <translation>Suche im Nebel den Notausgang!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="82"/>
        <source>Watch for Rolling Rocks</source>
        <translation>Achte auf rollende Felsen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="87"/>
        <source>Boil the Big Bully</source>
        <translation>Werfe Big Bully in die Lava!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="88"/>
        <source>Bully the Bullies</source>
        <translation>Werfe die Bullies in die Lava!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="89"/>
        <source>8-Coin Puzzle with 15 Pieces</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="90"/>
        <source>Red-Hot Log Rolling</source>
        <translation>Tanze auf dem Baumstamm!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="91"/>
        <source>Hot-Foot-it into the Volcano</source>
        <translation>Erkunde den Vulkan!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="92"/>
        <source>Elevator Tour in the Volcano</source>
        <translation>Gewinne im Vulkan an Höhe!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="97"/>
        <source>In the Talons of the Big Bird</source>
        <translation>Folge dem Riesengeier!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="98"/>
        <source>Shining Atop the Pyramid</source>
        <translation>Bringe Licht in die Pyramide!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="99"/>
        <source>Inside the Ancient Pyramid</source>
        <translation>Begebe dich in die Pyramide!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="100"/>
        <source>Stand Tall on the Four Pillars</source>
        <translation>Lande auf allen vier Säulen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="101"/>
        <source>Free Flying for 8 Red Coins</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="102"/>
        <source>Pyramid Puzzle</source>
        <translation>Löse das Pyramidenpuzzle!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="107"/>
        <source>Board Bowser&apos;s Sub</source>
        <translation>Entere Bowsers U-Boot!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="108"/>
        <source>Chests in the Current</source>
        <translation>Öffne die Truhen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="109"/>
        <source>Pole-Jumping for Red Coins</source>
        <translation>Springe zu den roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="111"/>
        <source>The Manta Ray&apos;s Reward</source>
        <translation>Folge dem Fisch!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="112"/>
        <source>Collect the Caps...</source>
        <translation>Vereine die Mützen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="117"/>
        <source>Snowman&apos;s Big Head</source>
        <translation>Erklimme den Kopf!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="118"/>
        <source>Chill with the Bully</source>
        <translation>Spiele mit Bad Bully!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="119"/>
        <source>In the Deep Freeze</source>
        <translation>Wage dich ins Eis!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="120"/>
        <source>Whirl from the Freezing Pond</source>
        <translation>Gehe zum Eissee!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="121"/>
        <source>Shell Shreddin&apos; for Red Coins</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="122"/>
        <source>Into the Igloo</source>
        <translation>Schau im Iglu nach!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="127"/>
        <source>Shocking Arrow Lifts!</source>
        <translation>Folge den blauen Pfeilen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="128"/>
        <source>Top o&apos; the Town</source>
        <translation>Suche nach der Spitze der Stadt!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="129"/>
        <source>Secrets in the Shallows &amp; Sky</source>
        <translation>Erforsche die Höhen und Tiefen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="130"/>
        <source>Express Evelator--Hurry Up!</source>
        <translation>Beeile dich - Expressaufzug!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="131"/>
        <source>Go to Town for Red Coins</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="132"/>
        <source>Quick Race through Downtown!</source>
        <translation>Beachte das Zeitlimits!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="137"/>
        <source>Scale the Mountain</source>
        <translation>Erklimme den Berg!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="138"/>
        <source>Mystery of the Monkey&apos;s Cage</source>
        <translation>Erkunde den Affenkäfig!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="139"/>
        <source>Scary &apos;Shrooms, Red Coins</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="140"/>
        <source>Mysterious Mountainside</source>
        <translation>Erforsche die Steilwand!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="141"/>
        <source>Breathtaking View from Bridge</source>
        <translation>Halte Ausschau auf der Brücke!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="142"/>
        <source>Blast to the Lonely Mushroom</source>
        <translation>Suche den einsamen Pilz!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="147"/>
        <source>Pluck the Piranha Flower</source>
        <translation>Pflücke die Schnapp-Piranha!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="148"/>
        <source>The Tip Top of the Huge Island</source>
        <translation>Begib dich zur Spitze der Insel!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="149"/>
        <source>Rematch with Koopa the Quick</source>
        <translation>Trete noch einmal gegen Koopa an!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="150"/>
        <source>Five Itty Bitty Secrets</source>
        <translation>Löse die 5 Itty Bitty-Rätsel!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="151"/>
        <source>Wiggler&apos;s Red Coins</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="152"/>
        <source>Make Wiggler Squirm</source>
        <translation>Drehe Wiggler im Kreis!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="157"/>
        <source>Roll into the Cage</source>
        <translation>Rolle den Käfig!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="158"/>
        <source>The Pit and the Pendulums</source>
        <translation>Achte auf das Pendel!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="159"/>
        <source>Get a Hand</source>
        <translation>Lass dir den Weg zeigen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="160"/>
        <source>Stomp on the Thwomp</source>
        <translation>Stampfe mächtig auf!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="161"/>
        <source>Timed Jumps on Moving Bars</source>
        <translation>Springe auf den Schwebebalken!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="162"/>
        <source>Stop Time for Red Coins</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="167"/>
        <source>Cruiser Crossing the Rainbow</source>
        <translation>Reise auf den Regenbögen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="168"/>
        <source>The Big House in the Sky</source>
        <translation>Suche das Haus am Himmel!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="169"/>
        <source>Coins Amassed in a Maze</source>
        <translation>Finde die 8 roten Münzen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="170"/>
        <source>Swingin&apos; in the Breeze</source>
        <translation>Lass dich von der Brise tragen!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="171"/>
        <source>Tricky Triangles!</source>
        <translation>Überwinde die Macht der Dreiecke!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="172"/>
        <source>Somewhere Over the Rainbow</source>
        <translation>Schau dir den Regenbogen an!</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="227"/>
        <source>Act 1 Star</source>
        <translation>Akt 1-Stern</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="228"/>
        <source>Act 2 Star</source>
        <translation>Akt 2-Stern</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="229"/>
        <source>Act 3 Star</source>
        <translation>Akt 3-Stern</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="230"/>
        <source>Act 4 Star</source>
        <translation>Akt 4-Stern</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="231"/>
        <source>Act 5 Star</source>
        <translation>Akt 5-Stern</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="232"/>
        <source>Act 6 Star</source>
        <translation>Akt 6-Stern</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="233"/>
        <source>100 Coin Star</source>
        <translation>100-Münzen-Stern</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="234"/>
        <source>Cannon Status</source>
        <translation>Kanonen-Status</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="235"/>
        <source>Coin High Score</source>
        <translation>Münzen-Highscores</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="264"/>
        <source>Peach&apos;s Castle</source>
        <translation>Peachs Schloss</translation>
    </message>
    <message>
        <location filename="../src/ui/save-slot-editor-dialog.cpp" line="317"/>
        <source>-- None --</source>
        <translation>-- Nichts --</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="44"/>
        <source>User Interface</source>
        <translation>Benutzeroberfläche</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="49"/>
        <source>Emulation</source>
        <translation>Emulation</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="54"/>
        <source>BPS Patches</source>
        <translation>BPS-Patches</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="59"/>
        <source>GFX Plugins</source>
        <translation>Grafikplugins</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="64"/>
        <source>Updaters</source>
        <translation>Updatemanager</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="69"/>
        <source>Romhacking.com</source>
        <translation>Romhacking.com</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="118"/>
        <source>Theme (Requires Restart)</source>
        <translation>Thema (Benötigt Neustart)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="134"/>
        <source>Dark Mode (Requires Restart)</source>
        <translation>Dunkler Modus (Benötigt Neustart)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="143"/>
        <source>Language (Requires Restart)</source>
        <translation>Sprache (Benötigt Neustart)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="158"/>
        <source>Hide launcher while playing ROM</source>
        <translation>Launcher verstecken, wenn eine ROM gespielt wird</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="165"/>
        <source>Check for updates on startup</source>
        <translation>Beim Start auf Updates prüfen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="175"/>
        <source>Discord &quot;Now Playing&quot; Integration</source>
        <translation>Discord-Integration</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="182"/>
        <source>Show additional rom settings meant for advanced users</source>
        <translation>Extra-ROM-Einstellungen anzeigen (nur für fortgeschrittene Nutzer)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="185"/>
        <source>Show advanced ROM options</source>
        <translation>Extra-ROM-Optionen anzeigen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="192"/>
        <source>Visible Columns</source>
        <translation>Sichtbare Spalten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="198"/>
        <source>The full path to the file</source>
        <translation>Der vollständige Pfad zur Datei</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="201"/>
        <source>File Path</source>
        <translation>Dateipfad</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="211"/>
        <source>The filename without the extension</source>
        <translation>Der Dateiname ohne Erweiterung</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="214"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="227"/>
        <source>The name stored in the ROM itself</source>
        <translation>Der Name, der in der ROM angegeben ist</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="230"/>
        <source>Internal Name</source>
        <translation>Interner Name</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="237"/>
        <source>The date you last played the ROM</source>
        <translation>Das Datum, an dem du diese ROM das letzte Mal gespielt hast</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="240"/>
        <source>Last Played</source>
        <translation>Zuletzt gespielt</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="250"/>
        <source>The total time spent playing the ROM</source>
        <translation>Die Gesamtzeit, die du diese ROM gespielt hast</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="253"/>
        <source>Total Play Time</source>
        <translation>Gesamtspielzeit</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="433"/>
        <source>Window Scale</source>
        <translation>Fenstergröße</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="404"/>
        <source>Default ParallelN64 Plugin</source>
        <translation>Standard ParallelN64-Plugin</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="443"/>
        <source>Default Mupen Plugin</source>
        <translation>Standard Mupen-Plugin</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="380"/>
        <source>Default Emulator Core</source>
        <translation>Standard Emulations-Core</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="481"/>
        <source>Fullscreen</source>
        <translation>Vollbild</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="495"/>
        <source>Pause when emulator loses focus</source>
        <translation>Emulation pausieren wenn der Fokus verloren wird</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="502"/>
        <source>Do not show RetroArch notifications such as &quot;Saved state to slot #0&quot; or &quot;Device disconnected from port #1&quot;</source>
        <translation>Wenn diese Einstellung aktiviert ist, werden RetroArch-Benachrichtigungen wie &quot;Spielstand in Speicherplatz #0&quot; oder &quot;Gerät getrennt von Port #1&quot; nicht angezeigt</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="505"/>
        <source>Hide notifications</source>
        <translation>Benachrichtigungen verstecken</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="537"/>
        <source>Reset RetroArch Config</source>
        <translation>RetroArch-Konfiguration zurücksetzen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="589"/>
        <source>When loading a BPS patch, save the patched ROM...</source>
        <translation>Wenn ein BPS-Patch geladen wird, speichere die ROM...</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="596"/>
        <source>To the same folder as the patch</source>
        <translation>Im selben Ordner wie der Patch</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="609"/>
        <source>To this folder:</source>
        <translation>In diesem Ordner:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="651"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1350"/>
        <source>Browse</source>
        <translation>Durchsuchen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="686"/>
        <source>Global Settings</source>
        <translation>Globale Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="710"/>
        <source>Upscaling</source>
        <translation>Hochskalierung</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="726"/>
        <source>Native (x1 - 320x240)</source>
        <translation>Nativ (x1 - 320x240)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="764"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="893"/>
        <source>Anti-Aliasing</source>
        <translation>Kantenglättung</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="799"/>
        <source>Filtering</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="810"/>
        <source>None</source>
        <translation>Keine</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="815"/>
        <source>Anti-Alias</source>
        <translation>Kantenglättung</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="820"/>
        <source>Anti-Alias + Dedither</source>
        <translation>Kantenglättung + Dedithering</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="825"/>
        <source>Anti-Alias + Blur</source>
        <translation>Kantenglättung + Weichzeichner</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="830"/>
        <source>Filtered</source>
        <translation>Geflitert</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="992"/>
        <source>Removes the black borders on the left and right sides of the video. Since these pixels are never rendered on real hardware, results will vary depending on the game.</source>
        <translation>Entfernt die schwarzen Ränder am linken und rechten Rand des Bildes. Da diese Pixel auf realer Hardware nicht angezeigt werden, können die Ergebnisse je nach Spiel variieren.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="995"/>
        <source>Remove Black Borders</source>
        <translation>Schwarze Ränder entfernen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="903"/>
        <source>Use N64 3-Point Filtering</source>
        <translation>N64 3-Punkt-Filtering benutzen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="74"/>
        <source>Developer</source>
        <translation>Entwickler</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="79"/>
        <source>System Clock</source>
        <translation>Systemzeit</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="453"/>
        <source>Audio Driver</source>
        <translation>Audiotreiber</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="488"/>
        <source>Vsync</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="522"/>
        <source>Emulate SummerCart64 SD card interface</source>
        <translation>SummerCart64 SD-Karten-Schnittstelle emulieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="927"/>
        <source>Default ROM Settings</source>
        <translation>Standard-ROM-Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="951"/>
        <source>Upscale textures drawn using the TEX_RECT command. This can cause visual artifacts in some games.</source>
        <translation>Texturen hochskalieren, die mit dem TEX_RECT-Befehl angezeigt werden. Kann in manchen Spielen zu Artefakten führen.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="954"/>
        <source>Upscale TEXRECTs</source>
        <translation>TEXRECTs hochskalieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="974"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1096"/>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1140"/>
        <source>Apply to all existing ROMs</source>
        <translation>Auf alle gepeicherten ROMs anwenden</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1070"/>
        <source>Emulate the native framebuffer. This is required for some visual effects to work, but may cause lag on lower end GPUs</source>
        <translation>Emuliert den nativen Framebuffer. Wird für manche visuellen Effekte gebraucht, kann aber auf langsameren GPUs Lag verursachen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1073"/>
        <source>Emulate Framebuffer</source>
        <translation>Framebuffer emulieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1114"/>
        <source>Greatly increases accuracy by rendering decals correctly, but may cause a loss in performance</source>
        <translation>Erhöht Genauigkeit erheblich, indem Decals korrekt gerendert werden. Kann einen Performanceverlust verursachen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1117"/>
        <source>Emulate N64 Depth Compare</source>
        <translation>N64 Depth Compare emulieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1177"/>
        <source>Enable RetroArch Automatic Updates</source>
        <translation>Automatische RetroArch-Updates aktivieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1187"/>
        <source>Enable Mupen64Plus-Next Automatic Updates</source>
        <translation>Automatische Mupen64Plus-Next-Updates aktivieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1215"/>
        <source>Use development branch</source>
        <translation>Entwicklungsversionen benutzen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1229"/>
        <source>Update Interval</source>
        <translation>Update-Interval</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1249"/>
        <source>Every Launch</source>
        <translation>Bei jedem Start</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1254"/>
        <source>Daily</source>
        <translation>Täglich</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1259"/>
        <source>Weekly</source>
        <translation>Wöchentlich</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1264"/>
        <source>Monthly</source>
        <translation>Monatlich</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1289"/>
        <source>Check for Updates</source>
        <translation>Jetzt nach Updates suchen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1328"/>
        <source>Download Directory</source>
        <translation>Download-Ordner</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1340"/>
        <source>Sets the directory that hacks downloaded from romhacking.com will be saved to. If you change this setting later, your roms will automatically be moved to the new directory.</source>
        <translation>Kontrolliert den Ordner, in dem Hacks von romhacking.com automatisch gespeichert werden. Wenn di diese Einstellung später änderst, werden deine ROMs automatisch in den neuen Ordner verschoben.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1379"/>
        <source>Enable Star Display</source>
        <translation>Star Display einschalten</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1408"/>
        <source>If checked, Parallel Launcher will still show a star display with a default layout if the hack author did not provide one.</source>
        <translation>Wenn ein Hack kein Sternen-Layout hat, wird Parallel Launcher das Standard-Layout von SM64 anzeigen.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1411"/>
        <source>Show star display for hacks without a
star layout</source>
        <translation>Sternen-Anzeige für Hacks ohne ein
Sternen-Layout anzeigen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1425"/>
        <source>If checked, Parallel Launcher will submit your highest star count across all save slots to romhacking.com; otherwise, it will only submit your star count in slot A.</source>
        <translation>Wenn diese Einstellung aktiviert ist, wird Parallel Launcher die höchste Sternenanzahl aller Speicherstände an romhacking.com senden; wenn deaktiviert wird nur Speicherstand A gesendet.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1428"/>
        <source>Check all save slots</source>
        <translation>Alle Spielstände überprüfen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1435"/>
        <source>If checked, Parallel Launcher will use the GLideN64 plugin instead of the ParaLLEl plugin when it guesses that ParrLLEl is probably the best plugin to use. Only check this if your computer has an old GPU with poor Vulkan support. If you still experience lag even with GlideN64, you may need to disable &apos;Emulate Framebuffer&apos; and/or &apos;Emulate N64 Depth Compare&apos; in the GFX Plugins section of your Parallel Launcher settings.</source>
        <translation>Wenn diese Einstellung aktiviert ist, wird Parallel Launcher bei der automatischen Erkennung des Grafikplugins das GLideN64-Plugin anstelle des ParaLLEl-Plugins verwenden. Bitte aktiviere diese Einstellung nur, wenn du eine ältere GPU mit schlechtem Vulkan-Support benutzt. Wenn du immer noch schlechte Performance mit GLideN64 bekommst, versuche die &quot;Framebuffer emulieren&quot;- und/oder &quot;N64 Depth Compare emulieren&quot;-Einstellung in den Grafikplugin-Einstellungen zu deaktivieren.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1438"/>
        <source>Prefer HLE plugins</source>
        <translation>HLE-Plugins bevorzugen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1445"/>
        <source>If checked, Parallel Launcher will always default to using 4:3 resolution, even if the recommended settings indicate that widescreen is supported</source>
        <translation>Wenn diese Einstellung aktiviert ist, wird Parallel Launcher immer das 4:3-Bildformat verwenden, auch wenn Breitbild laut den empfohlenen Einstellungen unterstützt wird</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1448"/>
        <source>Ignore widescreen hint</source>
        <translation>Breitbild-Empfehlung ignorieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1472"/>
        <source>Emulate IS Viewer hardware and display any messages sent from the ROM in a separate window</source>
        <translation>IS-Viewer-Hardware emulieren und von ROM gesendete Nachrichten in einem separaten Fenster anzeigen</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1475"/>
        <source>Enable IS Viewer (ParallelN64 only)</source>
        <translation>IS-Viewer einschalten (ausschließlich ParallelN64)</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1503"/>
        <source>History Size</source>
        <translation>Nachrichtenpuffer-Größe</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1572"/>
        <source>The following options only apply to the ParallelN64 emulator core. The mupen64plus core will always sync to the system clock.</source>
        <translation>Die folgenden Optionen wirken sich nur auf den ParallelN64 Emulator-Core aus. mupen64plus wird immer die Systemzeit verwenden.</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1588"/>
        <source>N64 Real Time Clock</source>
        <translation>N64-Echtzeituhr</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1595"/>
        <source>Sync to system clock</source>
        <translation>Mit Systemzeit synchronisieren</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1608"/>
        <source>Use this time:</source>
        <translation>Diesen Zeitstempel benutzen:</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/settings-dialog.ui" line="1648"/>
        <source>Rollback clock when loading a savestate</source>
        <translation>Uhr zurückdrehen, wenn ein SaveState geladen wird</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="80"/>
        <source>Automatic (%1)</source>
        <translation>Automatisch (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="328"/>
        <source>Install Discord Plugin?</source>
        <translation>Discord-Plugin installieren?</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="328"/>
        <source>You have enabled Discord integration, but the Discord plugin is not currently installed. Would you like to install it now?</source>
        <translation>Discord-Integration wurde aktiviert, aber das Discord-Plugin ist momentan nicht installiert. Möchtest du es jetzt installieren?</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="338"/>
        <source>Download Failed</source>
        <translation>Download fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="338"/>
        <source>Failed to download Discord plugin</source>
        <translation>Beim Herunterladen des Discord-Plugins ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="371"/>
        <source>Auto</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="375"/>
        <location filename="../src/ui/settings-dialog.cpp" line="382"/>
        <source>Select a Folder</source>
        <translation>Bitte Ordner auswählen</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="391"/>
        <source>Confirm Reset</source>
        <translation>Zurücksetzen bestätigen</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="392"/>
        <source>This will reset your RetroArch config file, undoing any changes you have made within RetroArch. Your Parallel Launcher settings will not be affected. Do you want to continue?</source>
        <translation>Dies wird deine RetroArch-Konfigurationsdatei zurücksetzen, was alle Änderungen in RetroArch verwirft. Die Einstellungen in Parallel Launcher werden nicht beeinflusst. Möchtest du fortfahren?</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="395"/>
        <source>Config Reset</source>
        <translation>Konfiguration zurückgesetzt</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="395"/>
        <source>Your RetroArch config has been reset.</source>
        <translation>Deine RetroArch-Konfiguration wurde zurückgesetzt.</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="397"/>
        <source>Oops</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="397"/>
        <source>An unknown error occurred. Your RetroArch config has not been reset.</source>
        <translation>Ein unbekannter Fehler ist aufgetreten. Deine RetroArch-Konfiguration wurde nicht zurückgesetzt.</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="416"/>
        <location filename="../src/ui/settings-dialog.cpp" line="422"/>
        <location filename="../src/ui/settings-dialog.cpp" line="428"/>
        <location filename="../src/ui/settings-dialog.cpp" line="434"/>
        <source>Confirm Apply All</source>
        <translation>&quot;Auf alle anwenden&quot; bestätigen</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="416"/>
        <location filename="../src/ui/settings-dialog.cpp" line="422"/>
        <location filename="../src/ui/settings-dialog.cpp" line="428"/>
        <location filename="../src/ui/settings-dialog.cpp" line="434"/>
        <source>Apply this setting to all current roms?</source>
        <translation>Wirklich diese Einstellungen auf alle registrierten ROMs anwenden?</translation>
    </message>
    <message>
        <location filename="../src/ui/settings-dialog.cpp" line="446"/>
        <source>Unlimited</source>
        <translation>Unbegrenzt</translation>
    </message>
</context>
<context>
    <name>SingleplayerControllerSelectDialog</name>
    <message>
        <location filename="../src/ui/designer/singleplayer-controller-select-dialog.ui" line="20"/>
        <source>Controller Select</source>
        <translation>Controller-Auswahl</translation>
    </message>
    <message>
        <location filename="../src/ui/designer/singleplayer-controller-select-dialog.ui" line="32"/>
        <source>Multiple controllers are connected. Please press any button on the controller you wish to use, or press Esc on the keyboard to use keyboard controls.</source>
        <translation>Mehrere Controller sind angeschlossen. Bitte drücke einen Knopf auf dem Controller, den du verwenden möchtest oder drücke Esc auf der Tastatur, um die Tastatur-Steuerung zu verwenden.</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="14"/>
        <source>Update Available</source>
        <translation>Update verfügbar</translation>
    </message>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="26"/>
        <source>A new update is available for Parallel Launcher. Would you like to update now?</source>
        <translation>Ein Update für Parallel Launcher ist verfügbar. Möchtest du es jetzt installieren?</translation>
    </message>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="62"/>
        <source>Changelog:</source>
        <translation>Änderungen:</translation>
    </message>
    <message>
        <location filename="../src/updaters/update-dialog.ui" line="79"/>
        <source>Don&apos;t remind me again</source>
        <translation>Nicht wieder erinnern</translation>
    </message>
</context>
<context>
    <name>WindowsRetroArchUpdater</name>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="111"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="33"/>
        <source>Checking for RetroArch Updates</source>
        <translation>RetroArch-Updates werden gesucht</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="210"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="120"/>
        <source>Download Failed</source>
        <translation>Download fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="211"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="121"/>
        <source>Failed to download RetroArch.</source>
        <translation>Fehler beim Herunterladen von RetroArch.</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="218"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="128"/>
        <source>Installing RetroArch</source>
        <translation>RetroArch wird installiert</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="238"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="144"/>
        <source>Installation Error</source>
        <translation>Installation felhgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="239"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="145"/>
        <source>An error occurred attempting to uncompress the portable RetroArch bundle</source>
        <translation>Ein Fehler beim Dekomprimieren der portabelen RetroArch-Version ist aufgetreten</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="280"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="174"/>
        <source>Install Update?</source>
        <translation>Update installieren?</translation>
    </message>
    <message>
        <location filename="../src/updaters/retroarch-updater.cpp" line="281"/>
        <location filename="../src/updaters/retroarch-updater.mac.cpp" line="175"/>
        <source>An update for RetroArch is available. Would you like to install it now?</source>
        <translation>Ein Update für RetroArch ist verfügbar. Möchtest du es jetzt installieren?</translation>
    </message>
</context>
<context>
    <name>WindowsUpdater</name>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="45"/>
        <source>Unexpected Error</source>
        <translation>Unerwarteter Fehler</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="46"/>
        <source>Failed to launch installer.</source>
        <translation>Beim Starten des Installationsprogramms ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="57"/>
        <source>Download Failed</source>
        <translation>Download fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../src/updaters/self-updater.cpp" line="58"/>
        <source>Failed to download the latest installer. Try again later.</source>
        <translation>Fehler beim Herunterladen des neusten Installationsprogramms. Versuche es später erneut.</translation>
    </message>
</context>
</TS>
