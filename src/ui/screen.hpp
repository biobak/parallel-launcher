#ifndef SRC_UI_SCREEN_HPP_
#define SRC_UI_SCREEN_HPP_

namespace Screen {

	void getResolution( int &width, int &height );
	void getDesktopSize( int &width, int &height );

}



#endif /* SRC_UI_SCREEN_HPP_ */
