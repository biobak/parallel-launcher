#ifndef SRC_POLYFILL_DEBUG_HPP_
#define SRC_POLYFILL_DEBUG_HPP_

#include <cstdlib>
#include "src/types.hpp"

namespace Debug {

	class Backtrace {

		private:
		void *m_trace[32];
		int m_frames;

		public:
		Backtrace() noexcept;
		~Backtrace() noexcept {}

		void log( const char *header = nullptr ) const noexcept;

	};

	extern void logBacktrace( const char *header = nullptr ) noexcept;

#ifdef _WIN32
	extern void logBacktraceFromContext( void *context, const char *header = nullptr ) noexcept;
#endif
}

#define PL_STRINGIFY_2( x ) #x
#define PL_STRINGIFY( x ) PL_STRINGIFY_2( x )

#define pl_assert( requirement ) \
	if( !(requirement) ) { \
		Debug::logBacktrace( "Assertion Failed: " #requirement "\n" __FILE__ ":" PL_STRINGIFY(__LINE__) ); \
		std::abort(); \
	}

#endif /* SRC_POLYFILL_DEBUG_HPP_ */
