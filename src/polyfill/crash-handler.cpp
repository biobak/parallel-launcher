#include "src/polyfill/crash-handler.hpp"

#include "src/core/traceable-exception.hpp"
#include "src/polyfill/debug.hpp"
#include <exception>
#include <typeinfo>
#include <cstdlib>

static void onTerminate() {
	const std::exception_ptr ep = std::current_exception();
	if( !ep ) return;

	try {
		std::rethrow_exception( ep );
	} catch( const std::exception &exception ) {
		const string header = "Unhandled Exception: "s + typeid( exception ).name();
		// Do not log exception.what() in case it contains personal information such as a username in a file path
		const TraceableException *traceableException = dynamic_cast<const TraceableException*>( &exception );
		if( traceableException != nullptr ) {
			traceableException->backtrace().log( header.c_str() );
		} else {
			Debug::logBacktrace( header.c_str() );
		}
	} catch( ... ) {
		Debug::logBacktrace( "Unhandled Exception" );
	}

	std::abort();
}

#ifdef _WIN32

#include <windows.h>
#include "src/core/numeric-string.hpp"
#include "src/types.hpp"

static LONG WINAPI onWindowsException( LPEXCEPTION_POINTERS seh ) {
	const std::exception_ptr ep = std::current_exception();
	if( ep ) {
		try {
			std::rethrow_exception( ep );
		} catch( const std::exception &exception ) {
			const string header = "Unhandled Exception: "s + typeid( exception ).name();
			const TraceableException *traceableException = dynamic_cast<const TraceableException*>( &exception );
			if( traceableException != nullptr ) {
				traceableException->backtrace().log( header.c_str() );
			} else {
				Debug::logBacktraceFromContext( (void*)seh->ContextRecord, header.c_str() );
			}
		} catch( ... ) {
			Debug::logBacktraceFromContext( (void*)seh->ContextRecord, "Unhandled Exception" );
		}
	} else {
		PEXCEPTION_RECORD exception = seh->ExceptionRecord;
		if( exception == nullptr ) {
			Debug::logBacktraceFromContext( (void*)seh->ContextRecord, "Structured Exception Event" );
		} else {
			while( exception->ExceptionRecord != nullptr ) {
				exception = exception->ExceptionRecord;
			}

			const string message = "Structured Exception Event (Code "s + Number::toString( (uint)exception->ExceptionCode ) + ')';
			Debug::logBacktraceFromContext( (void*)seh->ContextRecord, message.c_str() );
		}
	}

	return EXCEPTION_CONTINUE_SEARCH;
}


void registerCrashHandlers() {
	SetUnhandledExceptionFilter( onWindowsException );
	std::set_terminate( onTerminate );
}

#else

#include <csignal>

extern "C" void onSegfault( int ) {
	Debug::logBacktrace( "Segmentation Fault" );
	std::abort();
}

void registerCrashHandlers() {
	std::signal( SIGSEGV, onSegfault );
	std::set_terminate( onTerminate );
}

#endif
