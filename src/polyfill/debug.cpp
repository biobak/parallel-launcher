#include "src/polyfill/debug.hpp"

#include "src/polyfill/base-directory.hpp"

#ifdef _WIN32
#include <windows.h>
#include <dbghelp.h>
#include <cstdio>

static void logBacktraceImpl( void *const *trace, int frames, const char *header ) {
	if( frames <= 1 ) return;

	std::FILE *crashlog = nullptr;
	try {
		crashlog = std::fopen( (BaseDir::data() / "crashlog.txt").u8string().c_str(), "w" );
	} catch( ... ) {}

	if( crashlog == nullptr ) return;

	HANDLE proc = GetCurrentProcess();
	SymInitialize( proc, nullptr, TRUE );

	unsigned char symbolBuffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME];
	SYMBOL_INFO *symbol = (SYMBOL_INFO*)(void*)symbolBuffer;

	symbol->MaxNameLen = MAX_SYM_NAME - 1;
	symbol->SizeOfStruct = sizeof( SYMBOL_INFO );

	if( header != nullptr ) std::fprintf( crashlog, "%s\n", header );
	for( int i = 1; i < frames; i++ ) {
		SymFromAddr( proc, (DWORD64)trace[i], 0, symbol );
		std::fprintf( crashlog, "%s (0x%0llX)\n", symbol->Name, symbol->Address );
	}

	std::fflush( crashlog );
	std::fclose( crashlog );
}

void Debug::logBacktraceFromContext( void *context, const char *header ) noexcept {
	std::FILE *crashlog = nullptr;
	try {
		crashlog = std::fopen( (BaseDir::data() / "crashlog.txt").u8string().c_str(), "w" );
	} catch( ... ) {}

	if( crashlog == nullptr ) return;

	HANDLE proc = GetCurrentProcess();
	HANDLE thread = GetCurrentThread(); // Does the exception handler always run on the crashing thread?
	SymInitialize( proc, nullptr, TRUE );

	unsigned char symbolBuffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME];
	SYMBOL_INFO *symbol = (SYMBOL_INFO*)(void*)symbolBuffer;

	symbol->MaxNameLen = MAX_SYM_NAME - 1;
	symbol->SizeOfStruct = sizeof( SYMBOL_INFO );

	PCONTEXT cpuContext = (PCONTEXT)context;

	STACKFRAME64 frame;

	frame.AddrPC.Mode = AddrModeFlat;
	frame.AddrFrame.Mode = AddrModeFlat;
	frame.AddrStack.Mode = AddrModeFlat;

#ifdef _WIN64
	constexpr DWORD cpuArch = IMAGE_FILE_MACHINE_AMD64;
	frame.AddrPC.Offset = cpuContext->Rip;
	frame.AddrFrame.Offset = cpuContext->Rbp;
	frame.AddrStack.Offset = cpuContext->Rsp;
#else
	constexpr DWORD cpuArch = IMAGE_FILE_MACHINE_I386;
	frame.AddrPC.Offset = cpuContext->Eip;
	frame.AddrFrame.Offset = cpuContext->Ebp;
	frame.AddrStack.Offset = cpuContext->Esp;
#endif

	if( header != nullptr ) std::fprintf( crashlog, "%s\n", header );
	while( StackWalk64(
		cpuArch,
		proc,
		thread,
		&frame,
		cpuContext,
		nullptr,
		SymFunctionTableAccess64,
		SymGetModuleBase64,
		nullptr
	)) {
		const DWORD64 pc = frame.AddrPC.Offset;
		if( SymFromAddr( proc, pc, nullptr, symbol ) ) {
			std::fprintf( crashlog, "%s (0x%0llX)\n", symbol->Name, pc );
		} else {
			std::fprintf( crashlog, "??? (0x%0llX)\n", pc );
		}
	}

	std::fflush( crashlog );
	std::fclose( crashlog );
}

void Debug::logBacktrace( const char *header ) noexcept {
	void *trace[32];
	ULONG hash;
	const ushort frames = CaptureStackBackTrace( 0, 32, trace, &hash );
	logBacktraceImpl( trace, frames, header );
}

Debug::Backtrace::Backtrace() noexcept {
	ULONG hash;
	m_frames = (int)CaptureStackBackTrace( 0, 32, m_trace, &hash );
}

#else

#include <execinfo.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>

static void logBacktraceImpl( void *const *trace, int frames, const char *header ) {
	if( frames <= 1 ) return;

	backtrace_symbols_fd( &trace[1], frames - 1, 2 );

	int fd = -1;
	try {
		fd = open( (BaseDir::data() / "crashlog.txt").u8string().c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644 );
	} catch( ... ) {}

	if( fd >= 0 ) {
		if( header != nullptr ) {
			const char newline = '\n';
			(void)!write( fd, header, std::strlen( header ) );
			(void)!write( fd, &newline, 1 );
		}
		backtrace_symbols_fd( &trace[1], frames - 1, fd );
		fsync( fd );
		close( fd );
	}
}

void Debug::logBacktrace( const char *header ) noexcept {
	void *trace[32];
	const int frames = backtrace( trace, 32 );
	logBacktraceImpl( &trace[1], frames - 1, header );
}

Debug::Backtrace::Backtrace() noexcept {
	m_frames = backtrace( m_trace, 32 );
}

#endif

void Debug::Backtrace::log( const char *header ) const noexcept {
	logBacktraceImpl( m_trace, m_frames, header );
}
