#include "src/rhdc/ui/rhdc-rating-dialog.hpp"
#include "ui_rhdc-rating-dialog.h"

#include <QPushButton>
#include "src/core/numeric-string.hpp"
#include "src/ui/icons.hpp"
#include "src/ui/util.hpp"

static const char *s_qualityLabels[11] = {
	/*:  0 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "I can't decide or have no opinion" ),
	/*:  1 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Zero Effort" ),
	/*:  2 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Poor Quality" ),
	/*:  3 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "A Little Rough" ),
	/*:  4 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Unremarkable" ),
	/*:  5 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Decent" ),
	/*:  6 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Pretty Good" ),
	/*:  7 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Very Good" ),
	/*:  8 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Excellent" ),
	/*:  9 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Exceptional" ),
	/*: 10 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Legendary" )
};

static const char *s_difficultyLabels[11] = {
	/*:  0 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "I can't decide or have no opinion" ),
	/*:  1 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "No Challenge" ),
	/*:  2 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Very Easy" ),
	/*:  3 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Casual" ),
	/*:  4 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Classic SM64" ),
	/*:  5 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Moderate" ),
	/*:  6 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Challenging" ),
	/*:  7 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Difficult" ),
	/*:  8 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Very Difficult" ),
	/*:  9 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Extremely Difficult" ),
	/*: 10 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Borderline Kaizo" )
};

static const char *s_kaizoLabels[11] = {
	/*:  0 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "I can't decide or have no opinion" ),
	/*:  1 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Beginner/Introductory Kaizo" ),
	/*:  2 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Easy Kaizo" ),
	/*:  3 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Standard Kaizo" ),
	/*:  4 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Moderate Kaizo" ),
	/*:  5 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Challenging Kaizo" ),
	/*:  6 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Difficult Kaizo" ),
	/*:  7 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Very Difficult Kaizo" ),
	/*:  8 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Extremely Difficult Kaizo" ),
	/*:  9 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "Hardest humanly possible Kaizo" ),
	/*: 10 */ QT_TRANSLATE_NOOP( "RhdcRatingDialog", "TAS/Unbeatable" )
};

RhdcRatingDialog::RhdcRatingDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::RhdcRatingDialog ),
	m_kaizo( false )
{
	m_ui->setupUi( this );
	setWindowIcon( Icon::appIcon() );
	setModal( true );
	m_ui->buttonTray->button( QDialogButtonBox::Ok )->setText( tr( "Submit" ) );
	UiUtil::fixFontSizeOnMac( m_ui->label );
	UiUtil::fixDialogButtonsOnWindows( this );
}

RhdcRatingDialog::~RhdcRatingDialog() {
	delete m_ui;
}

void RhdcRatingDialog::setRatings( ubyte quality, ubyte difficulty, bool isKaizo ) {
	m_kaizo = isKaizo;
	m_ui->qualitySlider->setValue( (int)quality );
	m_ui->difficultySlider->setValue( (int)difficulty );
	qualityChanged( (int)quality );
	difficultyChanged( (int)difficulty );
}

ubyte RhdcRatingDialog::getQuality() const {
	return (ubyte)m_ui->qualitySlider->value();
}

ubyte RhdcRatingDialog::getDifficulty() const {
	return (ubyte)m_ui->difficultySlider->value();
}

static QString makeScoreLabel( int score ) {
	if( score == 0 ) return RhdcRatingDialog::tr( "Not Rated" );
	return QString( (Number::toString( score ) + "/10").c_str() );
}

void RhdcRatingDialog::qualityChanged( int rating ) {
	m_ui->qualityScoreLabel->setText( makeScoreLabel( rating ) );
	m_ui->qualityInfoLabel->setText( tr( s_qualityLabels[rating] ) );
}

void RhdcRatingDialog::difficultyChanged( int rating ) {
	m_ui->difficultyScoreLabel->setText( makeScoreLabel( rating ) );
	m_ui->difficultyInfoLabel->setText( m_kaizo ? tr( s_kaizoLabels[rating] ) : tr( s_difficultyLabels[rating] ) );
}
